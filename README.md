
# Car Rental

Welcome to the Car Rental booking application, a comprehensive solution designed to streamline the process of renting cars. Developed by a dedicated team of eight, this app offers an intuitive interface for users to book cars according to their preferences and needs. 

## Project Overview

Car Rental aims to simplify the car rental process, making it accessible and user-friendly for customers worldwide. Our application leverages cutting-edge technology, including a seamless integration with OpenAI for personalized recommendations, ensuring a tailor-made experience for every user.

## Features

- **User-Friendly Interface**: Designed with a focus on user experience, ensuring easy navigation and interaction.
- **Advanced Booking System**: An intuitive booking system that allows users to select, reserve, and rent cars with just a few clicks.
- **Personalized Recommendations**: Utilizing OpenAI technology for AI-driven suggestions tailored to the user's preferences and past behavior.
- **Real-Time Availability Check**: Instantly check the availability of cars, ensuring users can book what they see without any hassles.
- **Secure Payment Gateway**: A robust and secure payment system for safe transaction processing.

## Components

- **Infrastructura**: For a deep dive into our infrastructure's architecture, technologies, and workflows, 
  - [Read more about Infrastructura](_infra/README.md)
- **Backend**: The backbone of our application, handling business logic, database management, and server interactions.
  - [Read more about the backend](backend/README.md)
- **Frontend**: The user-facing part of our application, crafted to provide an engaging and intuitive user experience.
  - [Read more about the frontend](frontend/README.md)

## Team

Our project is brought to life by a team of passionate developers, each bringing their unique skills and dedication to the table:


- **Marcos Lopez** - Scrum Master, Testing
- **Amy Alvarado** - UX/UI Designer
- **Facundo Elorz**, **Mateo Romano**, **Amy Alvarado** - Frontend Developers
- **Maria F. Ardila**, **Melissa Miranda** - Backend Developers
- **Luis Curetti** - Database Specialist
- **Maximo Timochenko** - DevOps/SRE Engineer, Infraestructura

## Getting Started

To get started with the Car Rental application, please refer to the specific README files within each component directory for setup and deployment instructions.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE.md) file for details.
