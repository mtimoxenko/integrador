import { Route, Routes } from 'react-router-dom'
import Header from './Componentes/Header'
import Home from './Pages/Home'
import Categorias from './Pages/Categorias'
import Detalle from './Pages/Detalle'
import User from './Pages/User'
import Administracion from './Pages/Administracion/Administracion'
import AgregarVehiculos from './Pages/Administracion/AgregarVehiculos'
import ListaDeVehiculos from './Pages/Administracion/ListaDeVehiculos'
import Footer from './Componentes/Footer'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import './App.css'
import AgregarCategoria from './Pages/Administracion/AgregarCategoria'
import AdministracionDeUsuarios from './Pages/Administracion/AdministracionDeUsuarios'
import  {Favoritos}  from './Pages/Favoritos'
import Nosotros from './Pages/Nosotros'
import Contacto from './Pages/Contacto'
import Reservas from './Pages/Reservas'
import ScrollToTop from './utils/ScrollToTop'
import ErrorBoundary from './utils/ErrorBoundary'
import ReservaConfirmada from './Pages/ReservaConfirmada'
import ListarReservas from './Pages/ListarReservas'
 

function App() {
  
  return (
    <>
      <ToastContainer />
      <Header />
      <ScrollToTop />
      
      <ErrorBoundary>
        <Routes>
          <Route path="/" element={<Home/>}/>
          <Route path="/nosotros" element={<Nosotros/>}/>
          <Route path="/contacto" element={<Contacto/>}/>
          <Route path="/categorias/:id" element={<Categorias/>} />
          <Route path="/detalle/:id" element={<Detalle/>} />
          <Route path="/administracion" element={<Administracion/>} />
          <Route path="/administracion/agregarvehiculo" element={<AgregarVehiculos/>} />
          <Route path="/administracion/gestiondevehiculos" element={<ListaDeVehiculos/>} />
          <Route path="/administracion/agregarcategoria" element={<AgregarCategoria/>} />
          <Route path="/administracion/administracionusuarios" element={<AdministracionDeUsuarios />} />
          <Route path="/user" element={<User/>} />
          <Route path="/favoritos" element={<Favoritos/>}/>
          <Route path="/reservas/:id" element={<Reservas/>} />
          <Route path="/reservaconfirmada" element={<ReservaConfirmada/>} />
          <Route path="/listadereservas" element={<ListarReservas/>}/>
        </Routes>
      </ErrorBoundary>

      <Footer />
    </>

  )
}

export default App
