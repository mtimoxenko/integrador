import React, { useEffect, useRef, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { estiloHeader , imagenLogo, section, usersIconsDiv, saludoUsuario, links, responsive, avatar, avatarh3, imagenBoton, logoHeader, usersIcons, userOptions, circulo0, panelAdminItem, elMenu, burguer} from '../Estilos/Header.module.css'
import FormularioRegistro from './Formularios/FormularioRegistro'
import FormularioIngreso from './Formularios/FormularioIngreso'
import Alert from './Alert'
import { toast } from 'react-toastify'

const Header = () => {

  const navigate = useNavigate()
  const [mostrar, setMostrar] = useState(false)
  const [verFormRegistro, setVerFormRegistro] = useState(false)
  const [verFormIngreso, setVerFormIngreso] = useState(false)
  const [superSwitch, setSuperSwitch] = useState(false)

  const [menuSwitch, setMenuSwitch] = useState(false)
  const [menuHeigth, setMenuHeigth] = useState('0px')
  const [display, setDisplay] = useState('none')

  const [userOptionsSwitch, setUserOptionsSwitch] = useState(false)
  const [optionsHeigth, setOptionsHeigth] = useState('0')
  const [optionsDisplay, setOptionsDisplay] = useState('none')

  const [tokenUsuario, setTokenUsuario] = useState('')
  const [nombreUsuario, setNombreUsuario] = useState('')
  const [apellidoUsuario, setApellidoUsuario] = useState('')
  const [rolUsuario, setRolUsuario] = useState('')

  const [avatarDisplay, setAvatarDisplay] = useState('flex')

  const [alertIngreso, setAlertIngreso] = useState(false)
  const [alertCerrarSesion, setAlertCerrarSesion] = useState(false)


  const alertIngresoSwitch = () => {
    setAlertIngreso(!alertIngreso)
  }
  const alertIngresoSwitchAceptar = () => {
    window.location.replace('/')
  }

  const alertCerrarSesionSwitch = () => {
    setAlertCerrarSesion(!alertCerrarSesion)
  }
  const alertSesionCerradaSwitch = () => {
    localStorage.clear()
    window.location.replace('/')
  }

  const avatarRef = useRef(null);
  const elMenuRef = useRef(null);

  const [vehiculoEnVista, setVehiculoEnVista] = useState(sessionStorage.getItem('vehiculoEnVista') || '');
  const [visitarVehiculo, setVisitarVehiculo] = useState(sessionStorage.getItem('visitarVehiculo') || '');

  useEffect(() => {
    const handleStorageChange = () => {
      setVehiculoEnVista(sessionStorage.getItem('vehiculoEnVista') || '');
      setVisitarVehiculo(sessionStorage.getItem('visitarVehiculo') || '');
    };

    window.addEventListener('storage', handleStorageChange);

    return () => {
      window.removeEventListener('storage', handleStorageChange);
    };
  }, []);

  const token = localStorage.getItem('token')

  useEffect(()=>{
      if(token !== 'undefined'){
        setTokenUsuario(JSON.parse(token))
        setNombreUsuario(JSON.parse(localStorage.getItem('nombreUsuario')))
        setApellidoUsuario(JSON.parse(localStorage.getItem('apellidoUsuario')))
        setRolUsuario(JSON.parse(localStorage.getItem('rolUsuario')))
      }
  },[token])


  useEffect(() => {
    if (vehiculoEnVista) {
      setVerFormIngreso(true)
      sessionStorage.clear()
      return toast.warning('Inicia sesión o regístrate para reservar el auto que te gusta!', {
        position: "top-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
        style:{
          fontWeight: '500',
          color: '#000'
        }
      });
    }
    if (visitarVehiculo){
      navigate(`detalle/${visitarVehiculo}`)
      sessionStorage.clear()
    }
  }, [vehiculoEnVista, visitarVehiculo])


  const iniciales = (str) => {
    if (str) {
      return str.charAt(0).toUpperCase();
  }}

  const inicialNombre = iniciales(nombreUsuario);
  const inicialApellido = iniciales(apellidoUsuario);
  
  const setearMenu = ()=>{
    setMostrar(!mostrar)
  }
  const navegarHome = () => {
    window.location.replace('/')
  }

  const navegarUser = () => {
    navigate('/user')
    setUserOptionsSwitch(false)
  }
  const navegarFavoritos = () => {
    navigate('/favoritos')
    setUserOptionsSwitch(false)
  }
  const navegarNosotros = () => {
    navigate('/nosotros')
    setMostrar(!mostrar)
  }
  const navegarContacto = () => {
    navigate('/contacto')
    setMostrar(!mostrar)
  }

  const navegarReservas = () => {
    navigate('/listadereservas')
    setUserOptionsSwitch(false)
  }
  
  const switchDelFormRegistro = () => {
    setVerFormRegistro(!verFormRegistro)
  }

  const switchDelFormIngreso = () => {
    setVerFormIngreso(!verFormIngreso)
  }

  const superSwitchClick = () => {
    setVerFormRegistro(!verFormRegistro)
    setVerFormIngreso(!verFormIngreso)
  }


  useEffect(() => {
    if(mostrar){
      setMenuHeigth('160px')
      setTimeout(() => {
        setDisplay('flex')
      }, 300)
    }
    else{
      setMenuHeigth('0px')
      setDisplay('none')
    }
  }, [mostrar])


  const optionUserMenu = () =>{
    setUserOptionsSwitch(!userOptionsSwitch)
  }
  const navegarAdmin = () => {
    navigate('/administracion')
    optionUserMenu()
  }

  useEffect(() => {
    if(userOptionsSwitch){
      setOptionsHeigth('1')
      setOptionsDisplay('block')
    }
    else{
      setOptionsHeigth('0')
      setTimeout(() => {
        setOptionsDisplay('none')
      }, 100)
    }
  }, [userOptionsSwitch])

  const cerrarSesion = () => {
    setUserOptionsSwitch(false)
    setAlertCerrarSesion(true)
  }


  useEffect(() => {
    function handleClickOutside(event) {
      if (avatarRef.current && !avatarRef.current.contains(event.target)) {
        setUserOptionsSwitch(false);
      }
  
      if (elMenuRef.current && !elMenuRef.current.contains(event.target)) {
        setMostrar(false);
      }
    }
  
    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, []);


  useEffect(() => {
    function handleInitialScreenSize() {
      if (window.innerWidth > 768) {
        setAvatarDisplay('none')
      } else {
        setAvatarDisplay('flex')
      }
    }
  
    handleInitialScreenSize()
  
    function handleResize() {
      if (window.innerWidth > 768) {
        setAvatarDisplay('none')
      } else {
        setAvatarDisplay('flex')
      }
    }
  
    window.addEventListener('resize', handleResize)
  
    return () => window.removeEventListener('resize', handleResize)
  }, [])


  return (
    <div className={estiloHeader}>

      <div className={imagenLogo} >
        <div className={responsive} ref={elMenuRef}>
          
          <div className={burguer}>
            <span onClick={setearMenu}>
              <i className="fa-solid fa-bars fa-xl" ></i>
            </span>            
          </div>


            <div className={elMenu}>
              <ul style={{height: `${menuHeigth}`}}>
                <div style={{display: `${display}`}}>
                <li onClick={navegarHome}><h3>HOME</h3></li>
                <li onClick={navegarNosotros}><h3>NOSOTROS</h3></li>
                <li><a href="/#categorias" style={{color: '#373866', textDecoration: 'none'}}><h3>CATEGORIAS</h3></a></li>
                <li onClick={navegarContacto}><h3>CONTACTO</h3></li>
                </div>
              </ul>                
            </div>
          

        </div>

        <img onClick={navegarHome} src="/Images/logo.png" alt="imagen logo" className={logoHeader} />
        <h2 onClick={navegarHome}>Potenciando tu viaje, simplificando tu vida</h2>

        <div className={section}>
          {
            tokenUsuario ? 
            <div className={avatar} ref={avatarRef} style={{display:'flex'}}>
                <div style={{display: `${optionsDisplay}`, opacity: `${optionsHeigth}`}} className={userOptions} >
                  <ul>
                    {
                      rolUsuario === 'SUPER_ADMINISTRADOR' ?
                      <li><h4>Admin</h4></li> :
                      <li><h4>{nombreUsuario}</h4></li>
                    }
                    <li onClick={navegarUser}><h4>Perfil</h4></li>
                    
                    <li onClick={navegarFavoritos}><h4>Favoritos</h4></li>

                    <li onClick={navegarReservas}><h4>Mis Reservas</h4></li>

                    {(rolUsuario === 'ADMINISTRADOR' || rolUsuario === 'SUPER_ADMINISTRADOR') && <li className={panelAdminItem}><h4 onClick={navegarAdmin}>Panel de Administración</h4></li>}
                    <li  onClick={cerrarSesion}><h4>Cerrar sesión</h4></li>
                  </ul>
                </div>
                <div className={saludoUsuario}>
                  {/* <h5>👋</h5> */}
                   {
                      rolUsuario === 'SUPER_ADMINISTRADOR' ?
                      <h6>ADMIN</h6>
                      :
                      <h6>{nombreUsuario.toUpperCase()}</h6>
                    }
                </div>
                <div className={avatarh3}>
                  <h3 onClick={optionUserMenu}>{inicialNombre}{inicialApellido}</h3>                  
                </div>
            </div> 
              : 
            <>
              <div className={usersIcons}>
                <div className={usersIconsDiv}>
                    <img src="/Images/icono_ingresar.png" alt="" className={imagenBoton} />
                    <button onClick={switchDelFormIngreso}>Ingresar</button>
                </div>     
                <div className={usersIconsDiv}>
                  <img src="/Images/icono_registro.png" alt="" className={imagenBoton}/>
                  <button onClick={switchDelFormRegistro}>Crear cuenta</button>
                </div>               
              </div>

              <div style={{display: `${avatarDisplay}`}} className={avatar} ref={avatarRef}>
                <div style={{display: `${optionsDisplay}`, opacity: `${optionsHeigth}`}} className={userOptions}>
                 <h4>{nombreUsuario}</h4>
                  <h4 onClick={switchDelFormRegistro}>Crear Cuenta</h4>
                  <h4 onClick={switchDelFormIngreso}>Ingresar</h4>
                </div>
                <div onClick={optionUserMenu} className={circulo0}>
                  <span><i className="fa-solid fa-user-large fa-xl"></i></span>
                </div>
                
              </div> 
            </>            
          }


        </div>

      </div> 

      <div className={links}>
        <a href="/">HOME</a>
        <a href="/nosotros">NOSOTROS</a>
        <a href="/#categorias">CATEGORIAS</a>
        <a href="/contacto">CONTACTO</a>
      </div>
     
      {
        verFormRegistro && <FormularioRegistro superSwitchClick={superSwitchClick} switchDelFormRegistro={switchDelFormRegistro}/>
      }
      {
        verFormIngreso && <FormularioIngreso vehiculoEnVista={vehiculoEnVista} alertIngresoSwitch={alertIngresoSwitch} superSwitchClick={superSwitchClick} switchDelFormIngreso={switchDelFormIngreso} />
      }

      {
        alertIngreso && <Alert aceptar={alertIngresoSwitchAceptar} container='formIngreso'/>
      }
      {
        alertCerrarSesion && <Alert aceptar={alertSesionCerradaSwitch} cancelar={alertCerrarSesionSwitch} container='cerrarSesion'/>
      }
      
    </div>
  )
}

export default Header