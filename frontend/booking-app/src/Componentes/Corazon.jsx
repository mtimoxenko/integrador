import React, { useEffect, useState } from 'react'
import { toast } from 'react-toastify'


const Corazon = ({ productId, switchFav, detalleId }) => {

  const url = import.meta.env.VITE_API_URL

  const [switchCorazon, setSwitchCorazon] = useState(false)
  const [mostrarFavorito, setMostrarFavorito] = useState(false)
  const id = JSON.parse(localStorage.getItem('userId'))
  const token = JSON.parse(localStorage.getItem('token'))


  const configuraciones = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    }
  }


  useEffect(() => {

    if (!token) {
      return
    }

    fetch(`${url}/favorito/${id}`, configuraciones)
      .then(res => {
        if (!res.ok) {
          throw new Error(res.status)
        }
        else {
          return res.json()
        }
      })
      .then((data) => {
        const esFavorito = data.some(favorito => favorito.productDto.id === productId);
        setMostrarFavorito(esFavorito);

        if(window.location.pathname.includes('/detalle')){
          let esFavorito = false;

          for (const favorito of data) {
            if (favorito.productDto.id === parseInt(detalleId)) {
              esFavorito = true;
              break;
            }
          }
          setMostrarFavorito(esFavorito);
        }        
      })
      .catch(error => console.error(error))

  }, [switchCorazon])


  const agregarFavorito = () => {

    if (!token) {
      return toast.warning('Inicia sesión o regístrate para indicar que te gusta!', {
        position: "bottom-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
        style:{
          fontWeight: '500',
          color: '#373866'
        }
      });
    }

    const payload = {
      userId: parseInt(id),
      productId: parseInt(productId)
    }

    if(window.location.pathname.includes('/detalle')){
      payload.productId = parseInt(detalleId)
    }

    const configuracionesPost = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
      body: JSON.stringify(payload)
    }


    fetch(`${url}/favorito/agregar`, configuracionesPost)
      .then(res => {
        if (!res.ok) {
          throw new Error(res.status)
        }
        return res.json()
      })
      .then((data) => {
        setSwitchCorazon(!switchCorazon)
        toast('💖 El vehículo se agregó a tus favoritos!', {
          position: "bottom-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "light",
          style:{
            fontWeight: '500',
            color: '#373866'
          }
        });
      })
      .catch(error =>{
        console.error(error)
        toast.error('No se pudo agregar a favoritos!', {
          position: "bottom-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "light",
          style:{
            fontWeight: '500',
            color: '#373866'
          }
        });
      })

  }


  const eliminarFavorito = () => {

    const payload = {
      userId: parseInt(id),
      productId: parseInt(productId)
    }

    if(window.location.pathname.includes('/detalle')){
      payload.productId = parseInt(detalleId)
    }

    const configuracionesDelete = {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
      body: JSON.stringify(payload)
    }


    fetch(`${url}/favorito`, configuracionesDelete)
      .then(res => {
        if (!res.ok) {
          throw new Error(res.status)
        }
        return res.json()
      })
      .then((data) => {
        setSwitchCorazon(!switchCorazon)
        if(window.location.pathname === '/favoritos'){
          switchFav()
        }
        toast('❌ El vehículo se eliminó de tus favoritos!', {
          position: "bottom-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "light",
          style:{
            fontWeight: '500',
            color: '#373866'
          }
        });
      })
      .catch(error =>{
        console.error(error)
        toast.error('No se pudo eliminar el Vehículo!', {
          position: "bottom-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "light",
          style:{
            fontWeight: '500',
            color: '#373866'
          }
        });
      })
  }


  return (
    <>
      {
        mostrarFavorito ? 
        <img src="/Images/love/love_blue3.png" alt="" onClick={eliminarFavorito} style={{ width: '100%'}} />
          :
        <img src="/Images/love/love3.png" alt="" onClick={agregarFavorito} style={{ width: '100%'}} />
      }
    </>
  )
}

export default Corazon