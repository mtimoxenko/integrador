import React, { useState, useEffect } from 'react';

const TypingEffect = ({ text, speed }) => {
  const [displayText, setDisplayText] = useState('');
  const [currentIndex, setCurrentIndex] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      setDisplayText(prevText => {
        if (currentIndex < text.length) {
          return prevText + text[currentIndex];
        } else {
          clearInterval(interval); // Limpiar el intervalo cuando se haya mostrado todo el texto
          return prevText;
        }
      });
      setCurrentIndex(prevIndex => prevIndex + 1);
    }, speed);

    return () => clearInterval(interval);
  }, [text, speed, currentIndex]);

  return <>{displayText}</>;
};

export default TypingEffect;