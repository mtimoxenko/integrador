import React, { useState, useEffect } from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import estiloBuscador from "../Estilos/Buscador.module.css";
import Cardbuscador from './Cardbuscador';
import Spinner from './Spinner';
import { useNavigate } from 'react-router-dom';
import customPicker from '../Estilos/DatePicker.module.css'
import Selecmarca from './Selecmarca';
import useInput from '../Hooks/useInput';


const Buscador = () => {

  const [marcaInput, setMarcaInput] = useState("");
  const [categoria, setCategoria] = useState('');
  const [fechaInicio, setFechaInicio] = useState(null);
  const [fechaFin, setFechaFin] = useState(null);
  const [categoriasEnBaseDeDatos, setCategoriasEnBaseDeDatos] = useState([])
  const [responsiveButtonSwitch, setResponsiveButtonSwitch] = useState(false)
  const [lupaDisplay, setLupaDisplay] = useState('none')
  const [buscadorDisplay, setBuscadorDisplay] = useState('flex')
  const [spanDisplay, setSpanDisplay] = useState('none')
  const [spinnerDisplay, setSpinnerDisplay] = useState('none')
  const [cargandoDatos, setCargandoDatos] = useState(true)
  const [mesesMostrados, setMesesMostrados] = useState(2)
  const [switchLimpiar, setSwitchLimpiar] = useState(false)
  const [numeroPagina, setNumeroPagina] = useState(0)
  const [hayMasDatos, setHayMasDatos] = useState(false)
  const [paginacionAnterior, setPaginacionAnterior] = useState(false)
  const [paginacionSiguiente, setPaginacionSiguiente] = useState(false)
  const [resultadoInfo, setResultadoInfo] = useState('none')

  const today = new Date();


  const url = import.meta.env.VITE_API_URL

  const [datos, setDatos] = useState([])
  const [vehiculosMostrados, setVehiculosMostrados] = useState([])

  const handleInputChange = (newValue) => {
    setMarcaInput(newValue);
  };

  const handleFechaInicioChange = (date) => {
    setFechaInicio(date);
    if (fechaFin && date > fechaFin) {
      setFechaInicio(null);
    }
  };

  const handleFechaFinChange = (date) => {
    if (fechaInicio && date < fechaInicio) {
      return;
    }
    setFechaFin(date);
  };

  const filterTime = (time) => {
    if (fechaFin) {
      const twoDaysDate = new Date(fechaFin);
      twoDaysDate.setDate(fechaFin.getDate() - 1);
      return time < twoDaysDate;
    }

    return time;
  };

  const filterPassedTime = (time) => {
    const twoDaysDate = new Date(fechaInicio);
    if (fechaInicio) {
      twoDaysDate.setDate(fechaInicio.getDate() + 1);
    }

    return time > twoDaysDate;
  };


  useEffect(() => {
    if (datos.length > 0) {
      setVehiculosMostrados(datos.slice(0, 12))
    }
  }, [datos])


  useEffect(() => {
    fetch(`${url}/category/public`)
      .then(res => {
        if (!res.ok) {
          throw new Error(res.status)
        }
        else {
          return res.json()
        }
      })
      .then((data) => {
        setCategoriasEnBaseDeDatos(data)
      })
      .catch((error) => console.error(error))

  }, []);


  const handleClear = () => {
    setCategoria('');
    setFechaInicio(null);
    setFechaFin(null);
    setDatos([]);
    setVehiculosMostrados([])
    setNumeroPagina(0)
    setCargandoDatos(true)
    setSpinnerDisplay('none')
    setResultadoInfo('none')
    setSpanDisplay('none')
    setSwitchLimpiar(!switchLimpiar)
  }

  const consultaDatos = (pagina) => {

    const payload = {
      criteria: {
        category: categoria,
        brand: marcaInput,
        startDate: fechaInicio,
        endDate: fechaFin,
        pageableDto: {
            pageNumber: parseInt(pagina),
            pageSize: parseInt(12),
            sortBy: "name",
            direction: "ASC"
        }}
    }
  
    const configuraciones = {
      method: 'POST',
      headers: {
          'Content-Type': 'application/json'
      },
      body: JSON.stringify(payload)
    }
  
      fetch(`${url}/products/public/get`, configuraciones)
      .then(res=>{
        return res.json()
      })
      .then((data)=>{
        if(data.products.length > 0){
          setHayMasDatos(true)
        }
        else{
          setHayMasDatos(false)
        }
      })
  }


  const handleSearch = (pagina) => {

  const payload = {
    criteria: {
      category: categoria,
      brand: marcaInput,
      startDate: fechaInicio,
      endDate: fechaFin,
      pageableDto: {
          pageNumber: parseInt(pagina),
          pageSize: parseInt(12),
          sortBy: "name",
          direction: "ASC"
      }
  }
  }

  const configuraciones = {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify(payload)
  }

  setDatos([]);
  setVehiculosMostrados([])
  setNumeroPagina(pagina)
  setCargandoDatos(true)
  setSpinnerDisplay('flex')
  setResultadoInfo('flex')


    fetch(`${url}/products/public/get`, configuraciones)
      .then(res=>{
        return res.json()
      })
      .then((data)=>{
        setHayMasDatos(false)
        if(data.products.length === 12){
          consultaDatos(pagina+1)
        }
        setDatos(data.products)
      })
     
    setTimeout(() => {
      setCargandoDatos(false)
    }, 4000);
  }


  const handleSearchResponsive = (pagina) => {

    const payload = {
      criteria: {
        category: categoria,
        brand: marcaInput,
        startDate: fechaInicio,
        endDate: fechaFin,
        pageableDto: {
            pageNumber: parseInt(pagina),
            pageSize: parseInt(12),
            sortBy: "name",
            direction: "ASC"
        }
    }
    }
  
    const configuraciones = {
      method: 'POST',
      headers: {
          'Content-Type': 'application/json'
      },
      body: JSON.stringify(payload)
    }

    handleClear()
    setSpinnerDisplay('flex')
    setResultadoInfo('flex')

    fetch(`${url}/products/public/get`, configuraciones)
      .then(res => { return res.json() })
      .then(data => {
        if(data.products.length === 12){
          consultaDatos(pagina+1)
        }
        setDatos(data.products)
        setResponsiveButtonSwitch(!responsiveButtonSwitch)
      })

    setTimeout(() => {
      setCargandoDatos(false)
    }, 4000);
  }


  const buscadorSwitch = () => {
    setResponsiveButtonSwitch(!responsiveButtonSwitch)
  }

  useEffect(() => {
    if (responsiveButtonSwitch) {
      setLupaDisplay('flex')
      setBuscadorDisplay('none')
      setSpanDisplay('none')
    } else {
      setLupaDisplay('none')
      setBuscadorDisplay('flex')
      setSpanDisplay('flex')
    }
  }, [responsiveButtonSwitch])

  useEffect(() => {
    setSpanDisplay('none')
  }, [])

  useEffect(() => {
    function handleResize() {
      if (window.innerWidth > 768) {
        setLupaDisplay('none')
        setBuscadorDisplay('flex')
        setMesesMostrados(2)        
      }
      if (window.innerWidth < 768 && vehiculosMostrados.length > 0) {
        setLupaDisplay('flex')
        setBuscadorDisplay('none')
      }
      if (window.innerWidth < 768) {
        setMesesMostrados(1)
      }
    }

    handleResize()

    window.addEventListener('resize', handleResize);
    return () => window.removeEventListener('resize', handleResize);
  }, []);


  useEffect(() => {
    if(datos.length <= 11 && hayMasDatos){
      setHayMasDatos(true)
    }
  }, [datos])

  const setPaginaSiguiente = (pagina) => {

    if(datos.length > 11 && hayMasDatos){
      setNumeroPagina((prev)=> prev + 1)
      handleSearch(pagina+1)
    }
  }

  const setPaginaAnterior = (pagina) => {

    if(numeroPagina > 0){
      setNumeroPagina((prev)=> prev - 1)
      handleSearch(pagina-1)
    }
  }

  useEffect(() => {
    if(parseInt(numeroPagina) === 0){
      setPaginacionAnterior(false)
    }
    if(parseInt(numeroPagina) >= 1){
      setPaginacionAnterior(true)
    }
    if(hayMasDatos){
      setPaginacionSiguiente(true)
    }    
    if(!hayMasDatos){
      setPaginacionSiguiente(false)
    }
  }, [hayMasDatos, numeroPagina])



  return (
    <div className={estiloBuscador.divPrincipal}>
      <div style={{ display: `${buscadorDisplay}` }} className={estiloBuscador.divContenido}>

        <div className={estiloBuscador.selectores}>
          <div className={estiloBuscador.marca}>
            <Selecmarca marcaChange={handleInputChange} limpiar={switchLimpiar}/>
          </div>

          <div className={estiloBuscador.categoria}>
            <label htmlFor="categoria">Categoría:</label>
            <select
              id="categoria"
              value={categoria}
              onChange={(e) => setCategoria(e.target.value)}
            >
              <option value="">Seleccione una categoría</option>
              {
                (categoriasEnBaseDeDatos.length > 0) &&
                categoriasEnBaseDeDatos.map(categoria => {
                  return <option key={categoria.id} value={categoria.name}>{categoria.name}</option>
                })
              }
            </select>
          </div>          
        </div>

        <div className={estiloBuscador.selectores}>
          <div className={estiloBuscador.fechaInicio}>
            <label htmlFor="fechaInicio">Fecha de inicio:</label>
            <DatePicker
              className={customPicker.date}
              calendarClassName={customPicker.calendar}
              selected={fechaInicio}
              onChange={(date) => handleFechaInicioChange(date)}
              dateFormat="yyyy-MM-dd"
              monthsShown={`${mesesMostrados}`}
              filterDate={filterTime}
              placeholderText="Fecha Inicio"
              minDate={today}
            />
          </div>
          <div className={estiloBuscador.fechaFin}>
            <label htmlFor="fechaFin">Fecha de fin:</label>
            <DatePicker
              className={customPicker.date}
              calendarClassName={customPicker.calendar}
              selected={fechaFin}
              onChange={(date) => handleFechaFinChange(date)}
              dateFormat="yyyy-MM-dd"
              monthsShown={`${mesesMostrados}`}
              filterDate={filterPassedTime}
              placeholderText="Fecha Fin"
              minDate={today}
            />
          </div>          
        </div>




        <div className={estiloBuscador.botonBuscarDiv}>
          <button className={estiloBuscador.botonBuscar} onClick={() => handleSearch(0)}>Buscar</button>
          <button className={estiloBuscador.botonBuscar} onClick={handleClear}>Limpiar</button>

          <button className={estiloBuscador.botonBuscarResponsive} onClick={() => handleSearchResponsive(0)}>Buscar</button>
          <button className={estiloBuscador.botonBuscarResponsive} onClick={handleClear}>Limpiar</button>

          <span className={estiloBuscador.span} style={{ display: `${spanDisplay}` }}>
            <i onClick={buscadorSwitch} className="fa-solid fa-chevron-up fa-lg"></i>
          </span>

        </div>
      </div>
      <div onClick={buscadorSwitch} className={estiloBuscador.lupa} style={{ display: `${lupaDisplay}` }}>
        <h3>Buscar</h3>
        <i className="fa-solid fa-magnifying-glass fa-lg"></i>
      </div>


      <div className={estiloBuscador.resultadoBuscador} style={{paddingBottom: (datos.length > 0) ? '20px' : '0'}}>
        
        {
          (vehiculosMostrados.length > 0) ?
          <>
            {
              vehiculosMostrados.map((vehiculo) => {
                return <div style={{height: (lupaDisplay === 'flex')&& '75px'}} key={vehiculo.id} className={estiloBuscador.carddt}><Cardbuscador vehiculo={vehiculo} /></div>
              })
            }
            <div className={estiloBuscador.busquedaAvanzada}>
              <h3 onClick={() => setPaginaAnterior(numeroPagina)} style={{opacity: (!paginacionAnterior) ? '.4' : '1' ,cursor: (!paginacionAnterior) ? 'default' : 'pointer' }}>«</h3>
              <h4>{numeroPagina + 1}</h4>
              <h3 onClick={() => setPaginaSiguiente(numeroPagina)} style={{opacity: (!paginacionSiguiente) ? '.4' : '1', cursor: (!paginacionSiguiente) ? 'default' : 'pointer' }}>»</h3>
            </div>          
          </> 
          :
          <>
            {cargandoDatos ?
              <div className={estiloBuscador.spinnerDiv} style={{ display: `${spinnerDisplay}`}}>
                <Spinner />
              </div>:
              <div className={estiloBuscador.spinnerDiv} style={{ display: `${resultadoInfo}`, padding: '26px'}}>
                <p style={{textAlign: 'center',fontWeight: '500', fontSize: '18px', color: 'white', margin: '0'}}>Búsqueda sin resultados.</p>
              </div>
            }  
          </>
        }

      </div>

    </div>
  );
};

export default Buscador;
