import React, { useState, useEffect } from 'react'
import Card from "./Card";
import Carddetalle from "./Carddetalle";
import estiloBody from "../Estilos/Body.module.css"
import backgroundBody from "/Images/lamborgini_home.jpg"
import backgroundBody2 from "/Images/banner.jpg"
import Buscador from './Buscador';
import Skeleton from './Skeleton';
import HeadChat from './HeadChat';


const body = () => {

    const [vehiculosRecomendados, setVehiculosRecomendados] = useState([])
    const [categoriasDeAutos, setCategoriasDeAutos] = useState([])
    const [tokenUsuario, setTokenUsuario] = useState('')

    const url = import.meta.env.VITE_API_URL

    const token = localStorage.getItem('token')

    useEffect(() => {
        if (token !== 'undefined') {
            setTokenUsuario(JSON.parse(token))
        }
    }, [])

    useEffect(() => {
        fetch(`${url}/category/public`)
            .then(res => {
                /* if(!res.ok){
                    throw new Error(res.status)
                }else {
                   res.json() 
                } */
                return res.json()
            })
            .then((data) => {
                setCategoriasDeAutos(data)
            })
        /* .catch((err) => console.error(err)) */

    }, [])


    useEffect(() => {
        fetch(`${url}/products/public`)
            .then(res => {
                if (!res.ok) {
                    throw new Error(res.status)
                }
                else {
                    return res.json()
                }
            })
            .then((data) => setVehiculosRecomendados(data.products))
            .catch((error) => console.error(error))
    }, [])

    const [backgroundPrincipal, setBackgroundPrincipal] = useState(backgroundBody2)

    useEffect(() => {
  
      const timer = setTimeout(() => {
        setBackgroundPrincipal(backgroundBody)
  
        const timer2 = setTimeout(() => {
          setBackgroundPrincipal(backgroundBody2)
        }, 10000);
        return () => clearTimeout(timer2);
      }, 10000);
  
      return () => clearTimeout(timer);
    }, [backgroundPrincipal]);


    const [chatPosition, setChatPosition] = useState('fixed');

    useEffect(() => {
        const handleScroll = () => {
            const scrollY = window.scrollY;
            const windowHeight = window.innerHeight;
            const documentHeight = document.documentElement.scrollHeight;
            const bottomOffset = documentHeight - (scrollY + windowHeight);

            if (bottomOffset > 190) { // Cambia este valor según el espacio que desees dejar al final
                setChatPosition('fixed');
            } else {
                setChatPosition('absolute');
            }
        };

        window.addEventListener('scroll', handleScroll);

        return () => {
            window.removeEventListener('scroll', handleScroll);
        };
    }, []);


    return (
        <div className={estiloBody.divPrincipal}>
            <div className={estiloBody.divImgBg}>
                <img className={estiloBody.imgBackground} src={backgroundPrincipal} alt="imagen lamborgini" />
                <div className={estiloBody.buscadorDiv}>
                    <Buscador />                    
                </div>

                {/* <div className={estiloBody.info}>
                    <Info/>
                </div> */}

            </div>
            
            <div id="categorias" className={estiloBody.divCategorias}>
                <div className={estiloBody.divSeccionDeContenidos}>
                    <div className={estiloBody.categoriah2}>
                        <h2 >Categorías</h2>
                    </div>
                    
                    {
                        (categoriasDeAutos.length > 0) ?
                            <div className={estiloBody.divContenidoCategorias}>
                                {
                                    categoriasDeAutos.map((categoria) => {
                                        return <div key={categoria.id} className={estiloBody.cardCategoria}>
                                            <Card categoria={categoria} />
                                        </div>
                                    })
                                }
                            </div> :

                            <article className={estiloBody.skeletonDiv}>
                                <div><Skeleton /></div>
                                <div><Skeleton /></div>
                                <div><Skeleton /></div>
                                <div><Skeleton /></div>
                                <div><Skeleton /></div>
                                <div><Skeleton /></div>
                                <div><Skeleton /></div>
                                <div><Skeleton /></div>
                            </article>
                    }

                    <div className={estiloBody.divImgAuto}>
                        <img style={{marginRight: '15px', marginTop: '7px'}} width={'160px'} src='/Images/vehiculo_body_rojo(1).png' alt="imagen sedan" />
                    </div>

                </div>

            </div>

            <div className={estiloBody.divRecomendados}>
                <div className={estiloBody.divSeccionDeContenidos}>
                    <h2>Recomendados</h2>

                    <div className={estiloBody.divContenidoRecomendados}>

                        {
                            (vehiculosRecomendados.length > 0) &&
                            vehiculosRecomendados.map((item) => (
                                <div key={item.id} className={estiloBody.cardRecomendados}>
                                    <Carddetalle vehiculo={item} />
                                </div>
                            ))
                        }

                    </div>

                    <div className={estiloBody.divImgAuto}>
                        <img src='/Images/vehiculo_body_amarillo.png' alt="imagen sedan" />
                    </div>
                </div>

                <div className={estiloBody.headChat} style={{ position: chatPosition, bottom: (chatPosition === 'absolute') ? '-10px' : '6%'}}>
                    <HeadChat />
                </div>



            </div>

        </div>
    )
}

export default body;
