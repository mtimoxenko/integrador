import React, { useEffect, useState } from 'react';
import estiloCardbuscador from '../Estilos/Cardbuscador.module.css';
import { useNavigate } from 'react-router-dom';

const Cardbuscador = ({vehiculo}) => {

    const [cardImg, setCardImg] = useState('')
    const navigate = useNavigate()

    useEffect(() => {
        if(vehiculo.images.length > 0){
            setCardImg(vehiculo.images[0].imageUrl)
        }
    })

    const navegacionDetalleVehiculo = () => {
        navigate(`/detalle/${vehiculo.id}`)
    }


    return (
        <div onClick={navegacionDetalleVehiculo} className={estiloCardbuscador.cardContainer} style={{backgroundImage: `url(${cardImg})`}}>

            <div className={estiloCardbuscador.info}>
                <h4 className={estiloCardbuscador.nombre}>{vehiculo.brand} {vehiculo.model}</h4>
                {
                    vehiculo.category_name ? 
                    <h4 className={estiloCardbuscador.categoria}>{vehiculo.category_name}</h4>
                        :
                    <h4 className={estiloCardbuscador.categoria}>{vehiculo.category.name}</h4>
                }  
            </div>
        </div>
    )
}

export default Cardbuscador;
