import React, { useEffect, useRef, useState } from 'react'
import useInput from '../Hooks/useInput';
import selecMarca from '../Estilos/Selecmarca.module.css'

const Selecmarca = ({ marcaChange, limpiar }) => {
    const marca = useInput("text")
    const url = import.meta.env.VITE_API_URL
    const [marcas, setMarcas] = useState([]);
    const [marcasFiltradas, setMarcasFiltradas] = useState([])
    const [listaDisplay, setListaDisplay] = useState("none");
    const [placeHolder, setPlaceHolder] = useState("");
    const listaRef = useRef(null);

    useEffect(() => {
        marca.onChange({ target: { value: "" } })
    }, [limpiar])

    useEffect(() => {
        fetch(`${url}/products/public/brand`)
            .then(res => {
                return res.json()
            })
            .then((data) => {
                setMarcas(data.brands)
                setMarcasFiltradas(data.brands);
            })
    }, [])


    const handleInput = () => {
        setListaDisplay("flex")
    }

    useEffect(() => {
        function clickOut(event) {
            if (listaRef.current && !listaRef.current.contains(event.target)) {
                setListaDisplay("none");
            }
        }
        document.addEventListener('mousedown', clickOut);
        return () => {
            document.removeEventListener('mousedown', clickOut);
        };

    }, [])

    const inputSetMarca = (item) => {
        marca.onChange({ target: { value: item } })
        setListaDisplay('none')
    }

    const filtrar = (busqueda) => {
        const arrayDeMarcas = marcas.filter((item) => item.toString().toLowerCase().includes(busqueda.toLowerCase()));
        setMarcasFiltradas(arrayDeMarcas);
    }

    useEffect(() => {
        filtrar(marca.value)
        marcaChange(marca.value)
    }, [marca.value])

    const clickMarcas = () => {
        marca.onChange({ target: { value: "" } })
        setPlaceHolder("Todas las marcas")
        setListaDisplay('none')
    }


    return (
        <div className={selecMarca.container}>
            <label>Marca:</label>
            <input {...marca} onClick={handleInput} ref={listaRef} placeholder={placeHolder} />
            <ul style={{ display: `${listaDisplay}` }} ref={listaRef} >
                <li onClick={clickMarcas}>Todas las marcas</li>

                    {
                        marcasFiltradas &&
                        marcasFiltradas.map((marca, index) => {
                            return <li key={index} onClick={() => inputSetMarca(marca)}>{marca}</li>
                        })
                    }

            </ul>

        </div>
    )
}

export default Selecmarca