import React, {useState, useEffect} from 'react';
import { useNavigate } from 'react-router-dom'
import estiloCard from "../Estilos/Card.module.css";

const Card = ({ categoria }) => {

  const navigate = useNavigate()
  const [state, setState] = useState(false)
  const [alto, setAlto] = useState('23%')
  const [display, setDisplay] = useState('none')
  const [h5opacity, setH5opacity] = useState('0')


  const onClickVerMas = () => {
    sessionStorage.setItem('categoria', JSON.stringify(categoria))
    navigate(`/categorias/${categoria.id}`)
  }

  const mouseOver = () => {
    setH5opacity('1')
  }

  const mousseLeave = () => {
    setH5opacity('0')
  }

  function MontarDesmontar(){     
      setState(!state)
  }

  function cardMouseOver(){
    if(alto === '23%'){
      setAlto('26%')
    }
  }

  function cardMouseDown(){     
    if(alto === '26%'){
      setAlto('23.001%')
      setDisplay('none')
      setTimeout(()=>{
        setAlto('23%')
      },200)
      clearTimeout()
    }
  }

  useEffect(() => {
    if(state){
      setAlto('100%')
      setTimeout(()=>{
        setDisplay('block')
      },400)
      clearTimeout()
    } else{
      setAlto('23.0011%')
      setDisplay('none')
      setTimeout(()=>{
        setAlto('23%')
      },400)
      clearTimeout()
    }

  }, [state])


  return (
    <div onClick={MontarDesmontar} onMouseOver={cardMouseOver} onMouseLeave={cardMouseDown} className={estiloCard.card}>

      <div className={estiloCard.imagenCard} style={{backgroundImage: `url(${categoria.imageUrl})`}}>
      </div>

      <div style={{height: `${alto}`, transition: (alto === '26%' || alto === '23.001%') && 'height .2s ease'}} className={estiloCard.tituloCategoriaCard}>
        <div className={estiloCard.head}>
          <h3>{categoria.name}</h3>

          {
            (alto === '100%') ? <span className={estiloCard.span}><i className="fa-solid fa-chevron-up fa-rotate-180 fa-lg"></i></span> :
            <span className={estiloCard.span}><i className="fa-solid fa-chevron-up fa-lg"></i></span>            
          }

          <span onMouseOver={mouseOver} onMouseLeave={mousseLeave} onClick={onClickVerMas} className={estiloCard.verMas}><i className="fa-solid fa-circle-plus fa-xl"></i></span>
          <h5 style={{opacity: `${h5opacity}`}}>Ver Autos</h5>
        </div>

        {
          state && 
            <p style={{display: `${display}`}}>{categoria.description}</p>
        }
      </div>

    </div>
  );
};

export default Card;