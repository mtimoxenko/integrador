import React, { useEffect, useState } from 'react'
import alert from '../Estilos/Alert.module.css'

const Alert = ({aceptar, container, cancelar, mensaje, warning}) => {

  const [background, setBackground] = useState('transparent')
  const [height, setHeight] = useState(0)
  const [opacidad, setOpacidad] = useState(0)

  useEffect(() => {
    if(container === 'formIngreso' || container === "DetalleCalendar" || container === "Reservas"){
      const timer = setTimeout(() => {
        setHeight('140px')
        setOpacidad(1)
        setBackground('rgb(0, 0, 0,.2)')
      }, 100);
      return () => clearTimeout(timer)
    }
    const timer = setTimeout(() => {
      setHeight('140px')
      setOpacidad(1)
      setBackground('rgb(0, 0, 0,.2)')
      if(container === 'Categoria' || container === 'Registro' || container === 'Reserva' ){
        setHeight('180px')
      }
    }, 25);

    return () => clearTimeout(timer)
  }, []);


  return (
    <div className={alert.container} style={{background: `${background}`}}>
        <section style={{height: `${height}`, opacity: `${opacidad}`}}>

          <img src="/logo.ico" alt="" />

          {
            (container === 'formIngreso') &&
            <>
              <p>Se Inició sesión correctamente</p>
              <div className={alert.buttons}>
                <div className={alert.buttonDiv}><button onClick={aceptar}>Aceptar</button></div>
              </div>
            </>
          }

          {
            (container === 'Registro') &&
            <>
              <h4>{mensaje}</h4>
              <p>{warning}</p>
              <div className={alert.buttons}>
                <div className={alert.buttonDiv}><button onClick={aceptar}>Aceptar</button></div>
              </div>
            </>
          }

          {
            (container === 'cerrarSesion') &&
            <>
              <p>¿Seguro que quieres Cerrar Sesión?</p>
              <div className={alert.buttons}>
                <div className={alert.buttonDiv}><button onClick={aceptar}>Aceptar</button></div>
                <div className={alert.buttonDiv}><button onClick={cancelar}>Cancelar</button></div>
              </div>
            </>
          }

          {
            (container === 'Categoria') &&
            <>
              <h4>Se perderán todos los productos asociados a esta categoría</h4>
              <p>¿Estás seguro que quieres eliminarla?</p>
              <div className={alert.buttons}>
                <div className={alert.buttonDiv}><button onClick={aceptar}>Aceptar</button></div>
                <div className={alert.buttonDiv}><button onClick={cancelar}>Cancelar</button></div>
              </div>
            </>
          }

          {
            (container === 'Vehiculo') &&
            <>
              <p>¿Estás seguro que quieres eliminar el Vehículo?</p>
              <div className={alert.buttons}>
                <div className={alert.buttonDiv}><button onClick={aceptar}>Aceptar</button></div>
                <div className={alert.buttonDiv}><button onClick={cancelar}>Cancelar</button></div>
              </div>
            </>
          }

          {
            container === 'Reserva' &&
            <>
              <h4>{mensaje}</h4>
              <>{warning}</>
              <div className={alert.buttons}>
                <div className={alert.buttonDiv}><button onClick={aceptar}>Aceptar</button></div>
              </div>
            </>
          }

          {
            (container === 'DetalleCalendar' || container === 'Reservas') &&
            <>
              <h4>{mensaje}</h4>
              <div className={alert.buttons}>
                <div className={alert.buttonDiv}><button onClick={aceptar}>Aceptar</button></div>
              </div>
            </>
          }

        </section>
    </div>

  )
}

export default Alert