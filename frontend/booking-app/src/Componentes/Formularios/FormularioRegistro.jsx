import React, { useState } from 'react'
import useInput from '../../Hooks/useInput'
import Container from '../../Estilos/Formularios/FormularioRegistro.module.css'
import Alert from '../Alert'

const FormularioRegistro = ({switchDelFormRegistro, superSwitchClick}) => {

  const [errores, setErrores] = useState({})
  const [errorNotificacion, setErrorNotificacion] = useState(false)
  const [alertRegistro, setAlertRegistro] = useState(false)
  const [mensaje, setMensaje] = useState('')
  const [warning, setWarning] = useState('')

  const url = import.meta.env.VITE_API_URL
  
  const nombre = useInput('text')
  const apellido = useInput('text')
  const email = useInput('text')
  const contrasenia = useInput('password')

  const alertRegistroSwitch = () => {
    setAlertRegistro(!alertRegistro)
  }

  const alertRegistroSwitch2 = () => {
    window.location.replace('/')
  }

  const validarNombre = (nombre) => {
    const sinEspacios = nombre.trim()
    if(sinEspacios.length > 1){
      return true
    } else{
      return false
    }
  }

  const validarApellido = (apellido) => {
    const sinEspacios = apellido.trim()
    if(sinEspacios.length > 1){
      return true
    } else{
      return false
    }
  }

  const validarEmail = (email) => {
    const regexp = /^[^\.\s][\w\-]+(\.[\w\-]+)*@([\w-]+\.)+[\w-]{2,}$/gm

    if(regexp.test(email)){
      return true
    } else{
      return false
    }
  }

  const validarContrasenia = (contrasenia) => {
    const sinEspacios = contrasenia.trim()

    const contraseniaArr = sinEspacios.split("")

    const validarNumero = contraseniaArr.some((caracter) => {
      if(isNaN(caracter)){
        return false
      } else{
        return true
      }
    })

    if(sinEspacios.length > 7 && validarNumero){
      return true
    } else{
      return false
    }
  }

  const validaciones = () => {

    const error = {}

    const nombreValido = validarNombre(nombre.value)
    const apellidoValido = validarApellido(apellido.value)
    const contraseniaValido = validarContrasenia(contrasenia.value)
    const emailValido = validarEmail(email.value)

    if(!nombreValido){
      error.nombre = true
    }else{
      error.nombre = false
    }
    if(!apellidoValido){
      error.apellido = true
    }else{
      error.apellido = false
    }
    if(!emailValido){
      error.email = true
    }else{
      error.email = false
    }
    if(!contraseniaValido){
      error.contrasenia = true
    }else{
      error.contrasenia = false
    }

    return error
  }

  const payload = {
      name: nombre.value,
      lastName: apellido.value,
      email: email.value,
      password: contrasenia.value
  }

  const configuraciones = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(payload)
  }


  const handleSubmitForm = (e) => {
    e.preventDefault()

    const erroresValidacion = validaciones()
    setErrores(erroresValidacion)

    if(erroresValidacion.nombre === false && erroresValidacion.apellido ===false && erroresValidacion.email ===false && erroresValidacion.contrasenia ===false){
      fetch(`${url}/user/public/register`, configuraciones)
      .then(res=> {
        if(!res.ok){
          throw new Error (res.status)
        }
        else{
          return res.json()
        }
      })
      .then((data) => {
        localStorage.setItem('token', JSON.stringify(data.token))
        localStorage.setItem('nombreUsuario', JSON.stringify(data.name))
        localStorage.setItem('apellidoUsuario', JSON.stringify(data.lastName))
        localStorage.setItem('emailUsuario', JSON.stringify(data.email))
        localStorage.setItem('rolUsuario', JSON.stringify(data.userRol))
        localStorage.setItem('userId', JSON.stringify(data.id));

        nombre.onChange({target : { value : ''}})
        apellido.onChange({target : { value : ''}})
        email.onChange({target : { value : ''}})
        contrasenia.onChange({target : { value : ''}})        

        setMensaje(data.message)
        setWarning(data.warning)
        alertRegistroSwitch()
      })
      .catch(error=>{
        console.error('Hay un error!', error)
        setErrorNotificacion(true)
      })
    }
  }


  return (
    <div className={Container.fondo}>
      <div className={Container.formRegistro}>
        <h1>Crear Cuenta</h1>
        <form onSubmit={handleSubmitForm}>
          <div className={Container.inputDiv}>
            <label>Nombre</label>          
            <input {...nombre} />
            {errores.nombre && <p>+2 caracteres</p>}
          </div>
          <div className={Container.inputDiv}>
            <label>Apellido</label>
            <input {...apellido} />          
            {errores.apellido && <p>+2 caracteres</p>}
          </div>
          <div className={Container.inputDiv}>
            <label>Email</label>
            <input {...email} />          
            {errores.email && <p>Email inválido</p>}
          </div>
          <div className={Container.inputDiv}>
            <label>Contraseña</label>
            <input {...contrasenia} />
            {errores.contrasenia && <p>+8 caracteres, al menos 1 número</p>}
          </div>

          <button type='Submit'>Crear Cuenta</button>
        </form>

        <div className={Container.notificacionError}>
          {
            errorNotificacion &&
            <p>El correo ingresado ya se encuentra registrado.</p>
          }
        </div>

        <h4>Ya tengo una cuenta!</h4><h3 onClick={superSwitchClick}>Ingresar</h3>

        <span><i onClick={switchDelFormRegistro} className="fa-solid fa-x fa-xl"></i></span>
      </div>

      {
        alertRegistro &&
        <Alert aceptar={alertRegistroSwitch2} container='Registro' mensaje={mensaje} warning={warning}/>
      }

    </div>


  )
}

export default FormularioRegistro