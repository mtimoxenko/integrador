import React, { useState, useEffect } from 'react'
import useInput from '../../Hooks/useInput'
import Spinner from '../Spinner';
import { formularioVehiculos, customSelect, digitos, radioAut, texto, radio, archivos, error, controlImagenes, descripcionSection, descripcionInput, spinnerDiv } from '../../Estilos/Formularios/FormularioVehiculos.module.css'
import { toast } from 'react-toastify'


const FormularioVehiculos = () => {

  const url = import.meta.env.VITE_API_URL

  const token = JSON.parse(localStorage.getItem('token'))

  const [categoria, setCategoria] = useState('')
  const [automatico, setAutomatico] = useState(false)
  const [combustible, setCombustible] = useState('')
  const [imagenesURL, setImagenesURL] = useState([])
  const [errores, setErrores] = useState({})
  const [nombreImagenes, setNombreImagenes] = useState([])
  const [categoriasEnBaseDeDatos, setCategoriasEnBaseDeDatos] = useState([])
  const [spinnerDisplpay, setSpinnerDisplpay] = useState(false)

  const configuracionesGet = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  }

  useEffect(() => {
    fetch(`${url}/category/public`, configuracionesGet)
    .then(res=>{
      if(!res.ok){
        throw new Error(res.status)
      }
      else{
        return res.json()
      }
    })
    .then((data)=>{
      setCategoriasEnBaseDeDatos(data)
    })
    .catch((error)=>console.error(error))

  }, []);


  const imgOnChange = (event) => {
    const archivosSeleccionados = event.target.files
    const imagenesSeleccionadas = []
    const imagenesName = []


    for (let i = 0; i < archivosSeleccionados.length; i++) {
      const file = archivosSeleccionados[i]

      if (file.type === 'image/jpeg' || file.type === 'image/png') {
        imagenesName.push(file.name)
        imagenesSeleccionadas.push(file)

      }
    }

    setImagenesURL([...imagenesURL, ...imagenesSeleccionadas])
    setNombreImagenes([...nombreImagenes, ...imagenesName])
  }


  const marca = useInput('text')
  const modelo = useInput('text')
  const pasajeros = useInput('number')
  const maletas = useInput('number')
  const puertas = useInput('number')
  const descripcion = useInput('text')

  const vehiculo = {
    categoria: parseInt(categoria),
    marca: marca.value,
    modelo: modelo.value,
    pasajeros: parseInt(pasajeros.value),
    maletas: parseInt(maletas.value),
    puertas: parseInt(puertas.value),
    descripcion: descripcion.value,
    esAutomatico: automatico,
    combustible: combustible,
    imagenesUrls: imagenesURL
  }

  const handleCategoria = (event) => {
    setCategoria(event.target.value)
  }

  const setRadioInputCheck = (event) => {
    setAutomatico((event.target.value))
  }

  const setCombustibleInputCheck = (event) => {
    setCombustible(event.target.value)
  }

  const ValidacionUnitaria = (value) => {

    const valueSinEspacios = value.trim()
    if(valueSinEspacios.length === 0){
      return false
    } else {
      return true
    }
  }

  const ValidacionGlobal = () => {

    const error = {}

    const marcaOk = ValidacionUnitaria(marca.value)
    const modeloOk = ValidacionUnitaria(modelo.value)
    const pasajerosOk = ValidacionUnitaria(pasajeros.value)
    const maletasOk = ValidacionUnitaria(maletas.value)
    const puertasOk = ValidacionUnitaria(puertas.value)
    const descripcionOk = ValidacionUnitaria(descripcion.value)

    if(categoria === ''){
      error.categoria = true
    }else{
      error.categoria = false
    }
    if(!automatico){
      error.automatico = true
    }else{
      error.automatico = false
    }
    if(combustible === ''){
      error.combustible = true
    }else{
      error.combustible = false
    }
    if(imagenesURL.length < 5){
      error.imagenes = true
    }else{
      error.imagenes = false
    }
    if(!marcaOk){
      error.marca = true
    }else{
      error.marca = false
    }
    if(!modeloOk){
      error.modelo = true
    }else{
      error.modelo = false
    }
    if(!pasajerosOk){
      error.pasajeros = true
    }else{
      error.pasajeros = false
    }
    if(!maletasOk){
      error.maletas = true
    }else{
      error.maletas = false
    }
    if(!puertasOk){
      error.puertas = true
    }else{
      error.puertas = false
    }
    if(!descripcionOk){
      error.descripcion = true
    }else{
      error.descripcion = false
    }

    return error
  }


  let char_Id = [1,2];
  let formData = new FormData();

  formData.append('name', `${vehiculo.marca}${vehiculo.modelo}`);
  formData.append('category_id', vehiculo.categoria);
  formData.append('brand', vehiculo.marca);
  formData.append('model', vehiculo.modelo);
  formData.append('description', vehiculo.descripcion);
  formData.append('fuel', vehiculo.combustible);
  formData.append('numPassengers', vehiculo.pasajeros);
  formData.append('numBags', vehiculo.maletas);
  formData.append('numDoors', vehiculo.puertas);
  formData.append('isAutomatic', vehiculo.esAutomatico);
  for (let i = 0; i < vehiculo.imagenesUrls.length; i++) {
    formData.append('images', vehiculo.imagenesUrls[i]);
  }
  formData.append('characteristicsId', char_Id);

  const configuraciones = {
    method: 'POST',
    headers: {
      //'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    //body: JSON.stringify(payload)
    body: formData
  }

  
  const enviarFormulario = (e) => {
    e.preventDefault()

    const erroresValidacion = ValidacionGlobal()
    setErrores(erroresValidacion)

    const isValid = Object.values(erroresValidacion).every(valor => valor === false)

    if(isValid){

      setSpinnerDisplpay(true)
      // toast.promise(

        fetch(`${url}/products`, configuraciones)
          .then(res => {
            if(!res.ok){
              throw new Error (res.status)
            }
            else{
              return res.json()              
            }
          })
          .then(data => {
            setSpinnerDisplpay(false)
            toast.success('Vehículo registrado en Base de Datos con éxito!', {
              position: "bottom-left",
              autoClose: 7000,
              hideProgressBar: true,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: 0,
              theme: "dark",
            });
            marca.onChange({ target: { value: '' } });
            modelo.onChange({ target: { value: '' } });
            pasajeros.onChange({ target: { value: '' } });
            maletas.onChange({ target: { value: '' } });
            puertas.onChange({ target: { value: '' } });
            descripcion.onChange({ target: { value: '' } });
            setCategoria('')
            setImagenesURL([])
            setNombreImagenes([])
          })
          .catch(error => {
            setSpinnerDisplpay(false)
            console.error('Error!', error)
            return toast.error('Error al enviar el formulario', {
              position: "bottom-left",
              autoClose: 7000,
              hideProgressBar: true,
              closeOnClick: true,
              pauseOnHover: false,
              draggable: true,
              progress: 0,
              theme: "dark",
            });
          })
      // );
    }  
  }

  const removerImagen = (index) => {
    const actualizarImagenesURL = [...imagenesURL]
    const actualizarNombreImagenes = [...nombreImagenes]
    actualizarImagenesURL.splice(index, 1)
    actualizarNombreImagenes.splice(index, 1)
    setImagenesURL(actualizarImagenesURL)
    setNombreImagenes(actualizarNombreImagenes)
  };


  return (
    <div className={formularioVehiculos}>
      <form onSubmit={enviarFormulario}>

        <div className={customSelect}>
          <select value={categoria} onChange={handleCategoria}>
            <option value=""></option>

            {
              (categoriasEnBaseDeDatos.length > 0) && 
              categoriasEnBaseDeDatos.map(categoria=>{
                return <option key={categoria.id} value={categoria.id}>{categoria.name}</option>
              })
            }
            
          </select>
          <label>Categoria</label>
        </div>
        <div className={error}>{errores.categoria && <p>*Campo obligatorio</p>}</div>

        <fieldset className={texto}>
          <input {...marca}/>          
          <label>Marca</label>
        </fieldset>
        <div className={error}>{errores.marca && <p>*Campo obligatorio</p>}</div>

        <fieldset className={texto}>
          <input {...modelo}/>          
          <label>Modelo</label>
        </fieldset>
        <div className={error}>{errores.modelo && <p>*Campo obligatorio</p>}</div>


        <section className={descripcionSection}>

          <div className={digitos}>
            <fieldset>
              <input {...pasajeros}/>          
              <label>Pasajeros</label>
            </fieldset>
            <div className={error}>{errores.pasajeros && <p>*Campo obligatorio</p>}</div>

            <fieldset>
              <input {...maletas}/>          
              <label>Maletas</label>
            </fieldset>
            <div className={error}>{errores.maletas && <p>*Campo obligatorio</p>}</div>

            <fieldset>
              <input {...puertas}/>          
              <label>Puertas</label>
            </fieldset>
            <div className={error}>{errores.puertas && <p>*Campo obligatorio</p>}</div>
          </div>

          <div className={radioAut}>
            <fieldset>
              <p>Automatico</p>
              <input type='radio' value={true} onChange={setRadioInputCheck} name='automatico'/>
              <label>Si</label>
              <input type='radio' value={false} onChange={setRadioInputCheck} name='automatico'/>          
              <label>No</label>
            </fieldset>
          </div>

          <div className={descripcionInput}>
            <textarea {...descripcion} placeholder='Descripcion' style={errores.descripcion ? {borderColor: 'red'} : {borderColor: 'initial'}}/>
          </div>
        </section>

        <div className={error}>{errores.automatico && <p>*Campo obligatorio</p>}</div>

        <div className={radio}>
          <fieldset>
            <p>Combustible</p>
            <input type='radio' value='GASOLINA' onChange={setCombustibleInputCheck} name='combustible'/>
            <label>Gasolina</label>
            <input type='radio' value='GASOIL' onChange={setCombustibleInputCheck} name='combustible'/>          
            <label>Gasoil</label>
            <input type='radio' value='GNC' onChange={setCombustibleInputCheck} name='combustible'/>          
            <label>GNC</label>
            <input type='radio' value='ELECTRICO' onChange={setCombustibleInputCheck} name='combustible'/>          
            <label>Electrico</label>
          </fieldset>
        </div>
        <div className={error}>{errores.combustible && <p>*Campo obligatorio</p>}</div>

        <div className={archivos}>
          <label style={errores.imagenes ? {background: 'yellow'} : {background: 'white'}} htmlFor="imgCarga">
            <span><i className="fa-regular fa-image fa-lg"></i></span>
            <h4>Cargar Imagenes</h4>
            <h4 style={{position: 'absolute', right: '4px', top: '0', margin: '0'}}>+5</h4>
          </label>
          <input type="file" accept="image/jpeg,image/png" id="imgCarga" multiple onChange={imgOnChange} />
        </div>

        <section className={controlImagenes}>
          {nombreImagenes.map((imagen, index)=>(
            <div key={index}>
              <i onClick={() => removerImagen(index)} className="fa-regular fa-circle-xmark fa-lg"></i><p>{imagen}</p>
            </div>
          ))}
        </section>

        <button>Registrar Vehiculo</button>
          
          {
            spinnerDisplpay &&
              <div className={spinnerDiv}>
                <Spinner />
              </div>
          }        
      </form>

      <div style={{display: 'flex', width:'100%'}}>
        <ul style={{display: 'flex', justifyContent:'center', width:'100%', flexWrap:'wrap', gap:'8px',listStyleType: 'none', padding: '0'}}>
          {imagenesURL.map((imagen, index)=>(
            <li key={index}>
              <img width={'50px'} height={'50px'} src={URL.createObjectURL(imagen)} alt="" />              
            </li>
          ))}          
        </ul>

      </div>

    </div>
  )
}

export default FormularioVehiculos