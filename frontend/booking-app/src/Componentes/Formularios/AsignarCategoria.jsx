import React, { useState, useEffect } from 'react'
import formulario from '../../Estilos/Formularios/AsignarCategoria.module.css'
import { toast } from 'react-toastify'

const AsignarCategoria = ({setSwitch, vehiculoId, switchCatForm, token}) => {

    const [vehiculo, setVehiculo] = useState({})
    const [categoriaActual, setCategoriaActual] = useState('')
    const [categorias, setCategorias] = useState([])
    const [categoria, setCategoria] = useState('')
    const [nuevaCategoria, setNuevaCategoria] = useState('')

    const url = import.meta.env.VITE_API_URL
    
    const handleCategoria = (event) => {
        setCategoria(event.target.value)
    }

    useEffect(() =>{
        fetch(`${url}/products/public/${vehiculoId}`)
        .then(res=>{
          if(!res.ok){
            throw new Error(`Error en la solicitud: ${res.status}`)
          }
          else{
            return res.json()
          }
        })
        .then((data)=>{
          setVehiculo(data.currentProduct)
          setCategoriaActual(data.currentProduct.category.name);
        })
        .catch(error=>console.error(error))
      }, [])

      useEffect(() => {
        fetch(`${url}/category/public`)
        .then(res=>{
          if(!res.ok){
            throw new Error(res.status)
          }
          else{
            return res.json()
          }
        })
        .then((data)=>{
          setCategorias(data)
        })
        .catch((error)=>console.error(error))
    
      }, []);


      useEffect(() => {
        if(categoria === ''){
            return setNuevaCategoria('')
        }
        if(categorias.length > 0){
            const categoriaEncontrada = categorias.find(cat => cat.id == categoria)
            setNuevaCategoria(categoriaEncontrada.name)
        }
      }, [categoria])


      const payload = {
        id: parseInt(vehiculoId),
        category_id: parseInt(categoria)
      }

      const configuracionesPut = {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify(payload)
      }

      const handleClickButton = () => {

        fetch(`${url}/products/update`,configuracionesPut)
        .then(res => {
          if(!res.ok){
            throw new Error (res.status)
          }else{
            return res.json()            
          }
        })
        .then((data)=>{
          setSwitch()
          switchCatForm()
          toast.success('Cambio de categoria Asignado!', {
            position: "bottom-left",
            autoClose: 7000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: 0,
            theme: "dark",
          });
        })
        .catch((error)=>{
          console.error(error)
          return toast.error('No se pudo asignar el cambio de categoria!', {
            position: "bottom-left",
            autoClose: 7000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: false,
            draggable: true,
            progress: 0,
            theme: "dark",
          });
        })
      }


    return (
        <div className={formulario.fondo}>
            <div className={formulario.container}>

                <div className={formulario.customSelect}>
                    <label>Asignar Nueva Categoria</label>
                    <select value={categoria} onChange={handleCategoria}>
                        <option value="">Seleccionar Categoria</option>

                        {
                            (categorias.length > 0) && 
                            categorias.map(categoria=>{
                                return <option key={categoria.id} value={categoria.id}>{categoria.name}</option>
                            })
                        }
                        
                    </select>
                </div>

                <div>
                    <h4>Vehiculo: {vehiculo.brand} {vehiculo.model}</h4>
                    <h4>Id: {vehiculo.id}</h4>
   
                    <h4>Categoria Actual: {categoriaActual}</h4>
                </div>

                    <h3 style={{fontSize: '20px'}}>Nueva Categoria: {nuevaCategoria}</h3>
                    
                    <button onClick={handleClickButton}>Guardar</button>


                <span onClick={switchCatForm}><i className="fa-solid fa-x fa-xl"></i></span>
            </div>
        </div>
    )
}

export default AsignarCategoria