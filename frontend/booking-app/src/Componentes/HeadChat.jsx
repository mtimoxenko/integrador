import React, { useEffect, useRef, useState } from 'react'
import Chat from './Chat'
import headChat from '../Estilos/HeadChat.module.css'

const HeadChat = () => {

  const [chatSwitch, setChatSwitch] = useState(false)
  const [showMsj, setShowMsj] = useState(false)
  const [mobile, setMobile] = useState(false)
  const chatRef = useRef()

  const click = () => {
    setChatSwitch(!chatSwitch)
  }

  const overHead = () => {
    setShowMsj(true)
  }

  const leaveHead = () => {
    setShowMsj(false)
  }


  useEffect(() => {
    function handleClickOutside(event) {
      if (chatRef.current && !chatRef.current.contains(event.target)) {
        setChatSwitch(false)
      }
    }
  
    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, []);


  useEffect(() => {
    function handleResize() {
      if (window.innerWidth > 1024) {
        setMobile(false)
      }
      if (window.innerWidth < 1024) {
        setMobile(true)
      }
    }

    handleResize()

    window.addEventListener('resize', handleResize);
    return () => window.removeEventListener('resize', handleResize);
  }, []);


  return (
    <div className={headChat.container} ref={chatRef}>

      <div className={headChat.chatContainer} style={{boxShadow: (showMsj || chatSwitch) && '-5px 5px 0px 0px rgb(0, 0, 0, .0)'}}>
        {
          (showMsj && !chatSwitch && !mobile) &&
          <p className={headChat.mensajeInfo}>Haz Click para inciar chat, describe la utilidad y te recomendaré el vehículo ideal.</p>        
        }

        {
          (showMsj && !chatSwitch && mobile) &&
          <p className={headChat.mensajeInfo}>Describe la utilidad y te recomendaré el vehículo ideal.</p>        
        }

        {
          chatSwitch &&
            <div className={headChat.componentChatCatch}>
              <Chat />                  
            </div>
        }
      </div>


      <img onClick={click} onMouseOver={overHead} onMouseLeave={leaveHead} style={{width: '120px'}} src="/Images/Autobots-Symbol.png" alt="" />
    </div>
  )
}

export default HeadChat