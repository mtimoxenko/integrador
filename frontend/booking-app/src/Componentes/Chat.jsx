import React, { useEffect, useRef, useState } from 'react'
import chatcomponent from '../Estilos/Chat.module.css'
import useInput from '../Hooks/useInput'
import TypingEffect from './TypingEffect'

const Chat = () => {

  const [chatRespuesta, setChatRespuesta] = useState('')
  const [switchRespuesta, setSwitchRespuesta] = useState(false)
  const [msjError, setMsjError] = useState(false)
  const [msjObligatorio, setMsjObligatorio] = useState(false)
  const [opacidad, setOpacidad] = useState('0')  
  const pregunta = useInput('text')

  const url = 'http://54.209.90.187:8080/openai/recommendation'

  useEffect(() => {
    const timer = setTimeout(() => {
      setOpacidad('1')
    }, 200);

    return () => clearTimeout(timer)
  }, [])

  const activarRespuesta = () => {
    setSwitchRespuesta(true)
  }

  const payload = {
    text: pregunta.value,
  };

  const configuraciones = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "Accept": "application/json, text/plain, */*",
      "Accept-Charset": "utf-8"
    },
    body: JSON.stringify(payload),
  };


  const preguntar = () => {

    if(pregunta.value === ''){
      return setMsjObligatorio(true)
    }

    setChatRespuesta('')
    setSwitchRespuesta(false)
    setMsjError(false)
    setMsjObligatorio(false)
    activarRespuesta()

    fetch(`${url}`, configuraciones)
    .then(res => {
      if(!res.ok){
        throw new Error (res.status)
      }
      else{
        return res.text()
      }
    })
    .then((data) => {
      setChatRespuesta(data)
    })
    .catch(error=>{
      console.error(error)
      setMsjError(true)
    })
  }


  const pressEnter = (event) => {
    if (event.key === 'Enter' && event.shiftKey === false){
      event.preventDefault()
      preguntar()
    }
  }

  const textAreaRef = useRef(null);

  useEffect(() => {
    textAreaRef.current.focus();
    textAreaRef.current.select();
  }, []);


  return (
    <div className={chatcomponent.container} style={{opacity: `${opacidad}`}}>
      <div className={chatcomponent.containerOpacity}>
        <textarea {...pregunta} placeholder='Escribe aquí...' className={chatcomponent.inputPrincipal} onKeyDown={pressEnter} ref={textAreaRef}/>

        <div className={chatcomponent.span}>
          <span onClick={preguntar}>Enviar <i className="fa-solid fa-arrow-right"></i></span>
          {
            msjObligatorio && <h5>*Campo obligatorio</h5>    
          } 
        </div>

        
        <div className={chatcomponent.respuestaDiv}>
          {
            switchRespuesta &&
            <>
              <span className={chatcomponent.spanRespuesta}>...</span>
              {
                chatRespuesta !== '' &&
                <p><TypingEffect text={chatRespuesta} speed={15} /> </p>                
              }
              {
                msjError &&
                <p>error en la conexión, intente nuevamente</p>
              }
            </>
          }
        </div>        
      </div>
    </div>
  )
}

export default Chat