import React, { useEffect, useState } from 'react'
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import reserva from '../Estilos/DetalleCalendar.module.css'
import { toast } from 'react-toastify';
import { useNavigate } from 'react-router-dom';
import Reservas from '../Pages/Reservas';
import Alert from './Alert';

const DetalleCalendar = ({ vehiculoId }) => {

  const navigate = useNavigate()
  const [meses, setMeses] = useState(true)
  const [date, setDate] = useState(new Date())
  const [inicio, setInicio] = useState('')
  const [fin, setFin] = useState('')
  const [reservas, setReservas] = useState([])
  const [reservasFiltradas, setReservasFiltradas] = useState([])
  const [fechasDeshabilitadas, setFechasDeshabilitadas] = useState([])
  const id = JSON.parse(localStorage.getItem('userId'))
  const token = JSON.parse(localStorage.getItem('token'))
  const [alertSeleccfechas, setAlertSeleccfechas] = useState(false)
  const [alertRangoFechas, setAlertRangoFechas] = useState(false)
  const [alert2Dias, setAlert2Dias] = useState(false)

  const url = import.meta.env.VITE_API_URL

  const navegarReservas = () => {
    navigate(`/reservas/${vehiculoId}`)
  }

  const switchAlertSeleccFechas = () => {
    setAlertSeleccfechas(!alertSeleccfechas)
  }

  const switchAlertRangoFechas = () => {
    setFin('')
    setAlertRangoFechas(!alertRangoFechas)
  }

  const switchAlert2Dias = () => {
    setFin('')
    setAlert2Dias(!alert2Dias)
  }

  function limpiarRango() {
    setInicio('')
    setFin('')
  }

  useEffect(() => {

    fetch(`${url}/reservation/public`)
      .then(res => {
        if (!res.ok) {
          throw new Error(res.status)
        }
        else {
          return res.json()
        }
      })
      .then((data) => {
        setReservas(data);
      })
      .catch((error) => console.error(error))

  }, [])

  useEffect(() => {
    if (reservas.length > 0) {
      const reservasFiltradas = reservas.filter(reserva => reserva.product_id === vehiculoId)
      setReservasFiltradas(reservasFiltradas)
    }
  }, [reservas])

  useEffect(() => {
    if (reservasFiltradas.length > 0) {
      const nuevasFechasDeshabilitadas = reservasFiltradas.map(reserva => ({
        startDate: `${reserva.startDate[0]}-${reserva.startDate[1]}-${reserva.startDate[2]}`,
        endDate: `${reserva.endDate[0]}-${reserva.endDate[1]}-${reserva.endDate[2]}`
      }))

      setFechasDeshabilitadas(nuevasFechasDeshabilitadas);
    }
  }, [reservasFiltradas])


  const handleDateChange = (date) => {
    let showAlert = false
    if (inicio === '') {
      setDate(date);
      const month = date.getMonth();
      const day = date.getDate();
      const year = date.getFullYear();

      console.log('fecha:', `${day}-${month + 1}-${year}`);

      setInicio(`${year}-${month + 1}-${day}`)
    }

    else {
      const fechaInicio = new Date(inicio);
      const fechaFin = date;

      if (fechasDeshabilitadas.length > 0) {
        fechasDeshabilitadas.forEach(({ startDate, endDate }) => {
          const disabledStartDate = new Date(startDate);
          const disabledEndDate = new Date(endDate);
          console.log(disabledStartDate);
          console.log(disabledEndDate);

          console.log(fechaInicio);
          console.log(fechaFin);

          if (fechaInicio < disabledStartDate && fechaFin > disabledEndDate) {
            showAlert = true
          }
        })
        if (showAlert) {
          switchAlertRangoFechas()
        }
      }
      const diffDays = Math.floor((fechaFin - fechaInicio) / (1000 * 3600 * 24));

      if (diffDays >= 2) {
        const month = date.getMonth();
        const day = date.getDate();
        const year = date.getFullYear();
        console.log(`${day}-${month + 1}-${year}`);

        setFin(`${year}-${month + 1}-${day}`)
      }

      else {
        return switchAlert2Dias()
      }
      /* else{
        const diffDays = Math.floor((fechaFin - fechaInicio) / (1000 * 3600 * 24));

        if (diffDays >= 2){
          const month = date.getMonth();
          const day = date.getDate();
          const year = date.getFullYear();
          console.log('Fecha de fin del rango:', `${day}-${month + 1}-${year}`);

          setFin(`${year}-${month + 1}-${day}`)
        }
        else {
          setFin('')
          return alert('La fecha de fin debe ser al menos dos días posteriores a la fecha de inicio.')
        }
      } */
    }
  };


  useEffect(() => {
    function handleResize() {
      if (window.innerWidth > 768) {
        setMeses(true)
      }
      if (window.innerWidth < 768) {
        setMeses(false)
      }
    }

    handleResize()

    window.addEventListener('resize', handleResize);
    return () => window.removeEventListener('resize', handleResize);
  }, []);



  const tileDisabled = ({ date }) => {
    for (const { startDate, endDate } of fechasDeshabilitadas) {
      const fechaInicio = new Date(startDate);
      const fechaFin = new Date(endDate);

      const fechaActual = new Date(date.getFullYear(), date.getMonth(), date.getDate());

      if (fechaActual >= fechaInicio && fechaActual <= fechaFin) {
        return true;
      }
    }

    const fechaActual = new Date();
    fechaActual.setHours(0, 0, 0, 0);

    return date < fechaActual;
  };


  const tileClassName = ({ date, view }) => {
    if (tileDisabled({ date })) {
      return "disabledDate"; // Clase CSS para fechas deshabilitadas
    }
    return null; // De lo contrario, no aplicar ninguna clase
  };


  const payload = {
    user_id: parseInt(id),
    product_id: parseInt(vehiculoId),
    startDate: parseInt(inicio),
    endDate: parseInt(fin)
  }

  const configuracionesPost = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    body: JSON.stringify(payload)
  }

  const clickReserva = () => {

    if (!token) {
      sessionStorage.setItem('vehiculoEnVista', JSON.stringify(vehiculoId))
      return window.location.replace(`/detalle/${vehiculoId}`)
    }

    if (inicio === "") {
      return switchAlertSeleccFechas()
    }

    if (fin === "") {
      return switchAlertSeleccFechas()
    }

    if (inicio && fin) {
      sessionStorage.setItem('fechaInicio', JSON.stringify(inicio))
      sessionStorage.setItem('fechaFin', JSON.stringify(fin))
      navegarReservas()
    }


    // fetch(`${url}/reservation`, configuracionesPost)
    //   .then(res => {
    //     if (!res.ok) {
    //       throw new Error(res.status)
    //     }
    //     console.log(res)
    //     return res.json()
    //   })
    //   .then((data) => {
    //     console.log(data)

    //   })
    //   .catch(error => console.error(error))
  }

  const clickSinFecha = () => {
    if (inicio === "") {
      return switchAlertSeleccFechas()
    }
    if (fin === "") {
      switchAlertSeleccFechas()
    }
  }


  return (
    <div className={reserva.container}>
      <div className={reserva.reservaContainer}>
        <div className={reserva.userFechasDiv}>
          <div className={reserva.rangoFechasDiv}>
            <article style={{ borderTop: '1px solid black' }}>
              <h4>Inicio de reserva</h4>
              <h3 onClick={clickSinFecha}>{inicio}</h3>
            </article>
            <article style={{ borderBottom: '1px solid black' }}>
              <h4>Fin de reserva</h4>
              <h3 onClick={clickSinFecha}>{fin}</h3>
            </article>
            <button className={reserva.limpiarButton} onClick={limpiarRango}>Reset</button>
          </div>
          <div className={reserva.reservaButton}>
            <button onClick={clickReserva}>Reservar</button>
          </div>
        </div>
      </div>
      <div className={reserva.divCalendario}>
        <Calendar
          onChange={handleDateChange}
          value={date}
          showDoubleView={meses}
          className={reserva.calendario}
          tileDisabled={tileDisabled}
          tileClassName={tileClassName}
        />
      </div>

      {
        alertSeleccfechas &&
        <Alert mensaje="Selecciona una fecha en el calendario" aceptar={switchAlertSeleccFechas} container="DetalleCalendar" />
      }
      {
        alertRangoFechas &&
        <Alert mensaje="No se puede seleccionar un rango de fechas que incluyan fechas reservadas" aceptar={switchAlertRangoFechas} container="DetalleCalendar" />
      }
      {
        alert2Dias &&
        <Alert mensaje="Debes seleccionar un rango de fechas mayor o igual a 2 días" aceptar={switchAlert2Dias} container="DetalleCalendar" />
      }
    </div>
  )
}

export default DetalleCalendar