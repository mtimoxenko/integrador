import React from 'react'
import skeleton from '../Estilos/Skeleton.module.css'

const Skeleton = () => {
  return (
    <div className={skeleton.loadingImage}></div>
  )
}

export default Skeleton