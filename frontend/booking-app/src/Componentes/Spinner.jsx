import React from 'react'
import spinner from '../Estilos/Spinner.module.css'


const Spinner = () => {
  return (

    <div className={spinner.loader}></div>

  )
}

export default Spinner