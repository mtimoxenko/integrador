import React, { useEffect, useRef, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import estiloCarddetalle from "../Estilos/Carddetalle.module.css";
import Corazon from './Corazon';

const Carddetalle = ({ vehiculo, switchFav }) => {

    const navigate = useNavigate()
    const [cardImg, setCardImg] = useState('')
    const [vehiculoCategory, setVehiculoCategory] = useState({})
    const corazonRef = useRef()

    const navegacion=() => {
        navigate(`/detalle/${vehiculo.id}`);
    };

    useEffect(() => {
        if(vehiculo.images.length > 0){
            setCardImg(vehiculo.images[0].imageUrl)
        }
        if(vehiculo.category){
            setVehiculoCategory(vehiculo.category)
        }
    })

    const navegarADetalle = (event) => {
        if (corazonRef.current.contains(event.target)) {
            return
        }
        navegacion()
    }

    const combustible = (fuel) =>{
        if (fuel === 'GNC') {
            return fuel
        }
        const combustibleCap = fuel.charAt(0).toUpperCase() + fuel.slice(1).toLowerCase()
        return combustibleCap
    }


    return (
        <div className={estiloCarddetalle.card} onClick={navegarADetalle}>
            <div className={estiloCarddetalle.divImagen} style={{
                backgroundImage: `url(${cardImg})`
            }}>
            </div>

            <div className={estiloCarddetalle.detalles}>

                <div className={estiloCarddetalle.corazon} ref={corazonRef}>
                    <Corazon productId={vehiculo.id} switchFav={switchFav}/>
                </div>

                <div className={estiloCarddetalle.tituloCard}>
                        <h3 className={estiloCarddetalle.marca}>{vehiculo.brand} {vehiculo.model}</h3>
                    {
                        vehiculoCategory.name ? 
                        <h4 className={estiloCarddetalle.categoria}>{vehiculoCategory.name}</h4>
                        :
                        <h4 className={estiloCarddetalle.categoria}>{vehiculo.category_name}</h4>
                    }
                </div>


                <div className={estiloCarddetalle.detalle}>
                    <div className={estiloCarddetalle.span}><span><i className="fas fa-user fa-lg"></i></span><h4>{vehiculo.numPassengers}</h4></div> 
                    <div className={estiloCarddetalle.span}><span><i className="fa-solid fa-suitcase fa-lg"></i></span><h4>{vehiculo.numBags}</h4></div>
                    
                    
                    <div className={estiloCarddetalle.span}>
                        <span>
                            <i className={(vehiculo.fuel === 'ELECTRICO') ? "fas fa-bolt fa-lg" : "fas fa-gas-pump fa-lg"}></i>
                        </span>
                        <h4>{combustible(vehiculo.fuel)}</h4>
                    </div>
                </div>

            </div>
        </div>
    );
};

export default Carddetalle;

