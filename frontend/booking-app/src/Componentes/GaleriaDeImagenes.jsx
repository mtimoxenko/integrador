import React, { useEffect, useState } from 'react'
import galeria from '../Estilos/GaleriaDeImagenes.module.css'

const GaleriaDeImagenes = ({salirDeGaleriaDeImagenes}) => {

  const [imagen, setImagen] = useState('')

  useEffect(() => {
    setImagen(JSON.parse(sessionStorage.getItem('imagen')))
  }, [])

  return (
    <div className={galeria.galeriaContainer}>
      <div className={galeria.galeriaContainerDiv}>
        <div className={galeria.galeriaImgDiv}>
          <img src={imagen} alt="" />
        </div>          

        <span onClick={salirDeGaleriaDeImagenes}>
          <i className="fa-solid fa-x fa-lg"></i>
        </span>
      </div>
    </div>
  )
}

export default GaleriaDeImagenes