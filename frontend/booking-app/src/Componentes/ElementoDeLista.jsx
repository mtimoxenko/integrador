import React, { useState } from 'react'
import elementoDeLista from '../Estilos/Administracion/ElementoDeLista.module.css'
import { toast } from 'react-toastify'
import AsignarCategoria from './Formularios/AsignarCategoria'
import Alert from './Alert'


const ElementoDeLista = ({ identificacion, nombre, apellido, rol, url, token, entidad, categoriaVehiculo, setSwitch }) => {

  const [asignarCatForm, setAsignarCatForm] = useState(false)
  const [alertCategoria, setAlertCategoria] = useState(false)
  const [alertVehiculo, setAlertVehiculo] = useState(false)


  const switchCatForm = () => {
    setAsignarCatForm(!asignarCatForm)
  }

  const alertCategoriaSwitch = () => {
    setAlertCategoria(!alertCategoria)
  }

  const alertVehiculoSwitch = () => {
    setAlertVehiculo(!alertVehiculo)
  }

  const convertirRol = (rol) => {
    if(rol === "ADMINISTRADOR"){
      return "CLIENTE"
    }
    if(rol === "CLIENTE") {
      return "ADMINISTRADOR"
    }
  }

  const rolConvertido = convertirRol(rol)

  const configuracionesPut = {
    method: 'PUT',
    headers: {
      'Content-Type': 'text/plain',
      'Authorization': `Bearer ${token}`
    },
    body: identificacion
  }
  
  const urlPUT = `${url}/admin/changeTo/role/${rolConvertido}`

  const cambiarRol = () => {
    fetch(urlPUT, configuracionesPut)
      .then(res => {
        if (!res.ok) {
          throw new Error(res.status)
        } else {
          console.log(res);
        }
      })
      .then((data) => {
        setSwitch()
        return toast.success('Cambio de rol asignado!', {
          position: "bottom-left",
          autoClose: 7000,
          hideProgressBar: true,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: 0,
          theme: "dark",
        });
      })
      .catch(error => {
        console.error(error)
        return toast.error('No se pudo ejecutal cambio de rol!', {
          position: "bottom-left",
          autoClose: 7000,
          hideProgressBar: true,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: 0,
          theme: "dark",
        });
      })
  }


  const configuraciones = {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    }
  }

  const deleteProductById = () => {

    function fetchDelete(){
      fetch(`${url}/${identificacion}`, configuraciones)
        .then(res => {
          if (!res.ok) {
            throw new Error(res.status)
          }
          else {
            return res.json()
          }
        })
        .then(data => {
          console.log(data);
          setSwitch()
          return toast.success('Elemento removido de Base de Datos!', {
            position: "bottom-left",
            autoClose: 7000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: 0,
            theme: "dark",
          });
        })
        .catch(error => {
          console.error(error)
          return toast.error('No se pudo eliminar Item', {
            position: "bottom-left",
            autoClose: 7000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: false,
            draggable: true,
            progress: 0,
            theme: "dark",
          });
        })      
    }

    if(entidad === 'Vehiculo'){
      fetchDelete()
    }
    if(entidad === 'Categoria'){
      fetchDelete()
    }
  }

  const eliminarCategoria = () => {
    alertCategoriaSwitch()
    deleteProductById()
  }

  const eliminarVehiculo = () => {
    alertVehiculoSwitch()
    deleteProductById()
  }
 
  return (
    <>
      {
        (entidad === "Usuario") ?
          <>
            <h4 style={{ width: "25.1%", margin: "0", overflow: 'hidden', textOverflow: "ellipsis" }} className={elementoDeLista.itemId}>{identificacion}</h4>
            <h4 style={{ width: "25.05%", margin: "0" }} className={elementoDeLista.nombreItem}>{nombre} {apellido}</h4>
            <h4 style={{ width: "25%", margin: "0", border: "1px solid black" }}>{rol}</h4>
            <h4 style={{
              width: "25%",
              margin: "0",
              color: "rgb(6, 6, 187)",
              cursor: "pointer",
              textDecoration: "underline",
              border: "1px solid black"
            }}
              onClick={cambiarRol}>Cambiar Rol</h4>
          </>
          :
          <>

            {
              (entidad === "Vehiculo") ?
              <>
              <h4 className={elementoDeLista.itemId} style={{width: '10.2%'}}>{identificacion}</h4>
              <h4 className={elementoDeLista.nombreItem} style={{width: '25.1%'}}>{nombre} {apellido}</h4>
              <h4 className={elementoDeLista.nombreItem} style={{width: '20.2%'}}>{categoriaVehiculo.name}</h4>
              </> :
              <>
              <h4 className={elementoDeLista.itemId}>{identificacion}</h4>
              <h4 className={elementoDeLista.nombreItem}>{nombre} {apellido}</h4>
              </>
            }
            <div className={elementoDeLista.acciones}>

              {
                (entidad === "Vehiculo") ?
                <>
                <h4 onClick={switchCatForm} style={{
                  width: "50%",
                  margin: "0",
                  color: "rgb(6, 6, 187)",
                  cursor: "pointer",
                  textDecoration: "underline"
                }}>Asignar Categoria</h4>                
                <h4 onClick={alertVehiculoSwitch} style={{width: "50%"}}>Eliminar {entidad}</h4>
                </>
                :
                <>
                <h4 onClick={alertCategoriaSwitch}>Eliminar {entidad}</h4>
                </>
              }
              
            </div>
          </>
      }



      {
        asignarCatForm &&
        <>
          <AsignarCategoria setSwitch={setSwitch} switchCatForm={switchCatForm} vehiculoId={identificacion} token={token}/>
        </>
      }

      {
        alertCategoria &&
        <Alert aceptar={eliminarCategoria} cancelar={alertCategoriaSwitch} container='Categoria'/>
      }

      {
        alertVehiculo &&
        <Alert aceptar={eliminarVehiculo} cancelar={alertVehiculoSwitch} container='Vehiculo'/>
      }

    </>
  )
}

export default ElementoDeLista