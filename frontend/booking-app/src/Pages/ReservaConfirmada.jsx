import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import reservaConfirmStyles from "../Estilos/ReservaConfirmada.module.css"

const ReservaConfirmada = () => {
    const [inicio, setInicio] = useState("")
    const [fin, setFin] = useState("")
    const [mensaje, setMensaje] = useState("")

    const navegate = useNavigate()

    useEffect(() => {
        if (sessionStorage.getItem('fechaInicio') && sessionStorage.getItem('fechaFin')) {
            setInicio(JSON.parse(sessionStorage.getItem('fechaInicio')))
            setFin(JSON.parse(sessionStorage.getItem('fechaFin')))
            setMensaje(JSON.parse(sessionStorage.getItem('mensaje')))
            return sessionStorage.clear()
        } else {
            navegate("/")
        }
    }, [])


    return (

        <div className={reservaConfirmStyles.container}>
            <h1>¡{mensaje}</h1>
            <h2 style={{ color: "#B93442" }}>¡Gracias por preferirnos!</h2>
            <div className={reservaConfirmStyles.containerFechas}>
                <div className={reservaConfirmStyles.containerFechaInicio}>
                    <h3>Fecha inicio de la reserva: </h3>
                    <h3 style={{ color: "#B93442" }}>{inicio}</h3>
                </div>
                <div className={reservaConfirmStyles.containerFechaFin}>
                    <h3>Fecha fin de la reserva:</h3>
                    <h3 style={{ color: "#B93442" }}>{fin}</h3>
                </div>
            </div>
            <a className={reservaConfirmStyles.divA} href='/'>Aceptar</a>
        </div>
    )
}

export default ReservaConfirmada