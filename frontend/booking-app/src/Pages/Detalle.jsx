import React, { useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { cartaDelVehiculo, definicion, descripcion, nombreCarroDiv, CarroCorazonDiv, nombreCarroDivCorazon, cartaDelVehiculoDiv, imagenes, imgPrincipal, caracteristicas, calendar } from '../Estilos/Detalle.module.css'
import GaleriaDeImagenes from '../Componentes/GaleriaDeImagenes'
import DetalleCalendar from '../Componentes/DetalleCalendar'
import Corazon from '../Componentes/Corazon';

const Detalle = () => {

  const [verMas, setVerMas] = useState(false)
  const navigate = useNavigate()

  const [vehiculo, setVehiculo] = useState({})
  const [imgs, setImgs] = useState([])
  const [category, setCategory] = useState({})
  const {id} = useParams()
  const url = import.meta.env.VITE_API_URL

  useEffect(() =>{
    fetch(`${url}/products/public/${id}`)
    .then(res=>{
      if(!res.ok){
        throw new Error(`Error en la solicitud: ${res.status}`)
      }
      else{
        return res.json()
      }
    })
    .then((data)=>{
      setVehiculo(data.currentProduct)
      setImgs(data.currentProduct.images)
      setCategory(data.currentProduct.category)
    })
    .catch(error=>{
      console.error(error)
      navigate('/')
    })
  }, [])


  const verGaleriaDeImagenes = (img) => {
    sessionStorage.setItem('imagen', JSON.stringify(img))
    setVerMas(true)
  }

  const salirDeGaleriaDeImagenes = () => {
    setVerMas(false)
    sessionStorage.clear()
  }

  const irAtras = () => {
    navigate(-1)
  }

  const combustible = (fuel) =>{
    if (fuel === 'GNC') {
        return fuel
    }
    const combustibleCap = fuel.charAt(0).toUpperCase() + fuel.slice(1).toLowerCase()
    return combustibleCap
  }


  return (
    <>
    {
    vehiculo.id ? 
    <div className={cartaDelVehiculo}>
      <div className={definicion}>
        <div className={nombreCarroDiv}>
          <div className={CarroCorazonDiv}>
            <h2>{vehiculo.brand} {vehiculo.model}</h2>
            <div className={nombreCarroDivCorazon}>
              <Corazon detalleId={id}/>
            </div>
          </div>
          <h3>{category.name}</h3>
        </div>
        <span onClick={irAtras}><i className="fa-solid fa-arrow-left fa-2xl"></i></span>
      </div>
      <div className={cartaDelVehiculoDiv}>

        {
          (imgs.length > 0) &&
        <div className={imagenes}>
          <div onClick={() => verGaleriaDeImagenes(imgs[0].imageUrl)} className={imgPrincipal}>
            <img src={imgs[0].imageUrl} alt=''/>
          </div>
          {
            (imgs.length > 4) &&
          <article>
            <div onClick={() => verGaleriaDeImagenes(imgs[1].imageUrl)}><img src={imgs[1].imageUrl} alt="Imagen del vehiculo (modelo-categoria)" /></div>
            <div onClick={() => verGaleriaDeImagenes(imgs[2].imageUrl)}><img src={imgs[2].imageUrl} alt="Imagen del vehiculo (modelo-categoria)" /></div>
            <div onClick={() => verGaleriaDeImagenes(imgs[3].imageUrl)}><img src={imgs[3].imageUrl} alt="Imagen del vehiculo (modelo-categoria)" /></div>
            <div onClick={() => verGaleriaDeImagenes(imgs[4].imageUrl)}><img src={imgs[4].imageUrl} alt="Imagen del vehiculo (modelo-categoria)" /></div>
          </article>            
          }

        </div>}
        <div className={descripcion}>

            <p>{vehiculo.description}</p>
            
            <div className={caracteristicas}  >
                      <h2>Características</h2>

              <ul>
               <section>
                  <li><span><i className="fa-solid fa-suitcase"></i></span><p>{vehiculo.numBags}</p></li>
                  <li><span><i className="fas fa-user"></i></span><p>{vehiculo.numPassengers}</p></li>
                  <li><span><i className="fa-solid fa-car-side"></i></span> 
                  
                  {
                    (vehiculo.isAutomatic) ? <p>Automático</p> : <p>Manual</p>
                  }</li>
                </section>
               <section>

                  <li>
                    <span>
                      <i className={(vehiculo.fuel === 'ELECTRICO') ? "fas fa-bolt fa-lg" : "fas fa-gas-pump fa-lg"}></i>
                    </span>
                    <p>{combustible(vehiculo.fuel)}</p>
                  </li>

                  <li><span><i className="fa-brands fa-bluetooth"></i></span><p>Bluetooth</p></li>
                  <li><span><i className="fa-solid fa-snowflake"></i></span><p>A/C</p></li>
                </section>

              </ul>
            </div>


        </div>
        <div className={calendar}>
          <DetalleCalendar vehiculoId={vehiculo.id}/>
        </div>
      </div>

      {verMas && <GaleriaDeImagenes salirDeGaleriaDeImagenes={salirDeGaleriaDeImagenes}/>}
    </div>      
    
    :
    <div></div>
    }    
    
    
    
    </>


  )
}

export default Detalle