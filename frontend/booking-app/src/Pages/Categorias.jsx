import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom';
import categoriaDeVehiculo from '../Estilos/Categorias.module.css'
import Carddetalle from '../Componentes/Carddetalle'
import { useParams } from 'react-router-dom'
import Cardbuscador from '../Componentes/Cardbuscador'


const Categorias = () => {

  const url = import.meta.env.VITE_API_URL

  const navigate = useNavigate();

  const irAtras = () => {
      navigate(-1)
  }

    const {id} = useParams()
    const [categoriasEnBaseDeDatos, setCategoriasEnBaseDeDatos] = useState([])
    const [vehiculos, setVehiculos] = useState([])
    const [vehiculosOrdenados, setVehiculosOrdenados] = useState([])

    const [categoriasSeleccionadas, setCategoriasSeleccionadas] = useState([]);    
    const [productosEncontrados, setProductosEncontrados] = useState('0')

    const compareById = (a, b) => {
      return a.category_id - b.category_id;
    }

    const [listaDeVehiculos, setListaDeVehiculos] = useState([])

    const payload = {
      criteria: {
        pageableDto: {
            pageNumber: parseInt(0),
            pageSize: parseInt(500),
            sortBy: "name",
            direction: "ASC"
        }
    }
    }
    
      const configuraciones = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(payload)
    }
  
    useEffect(() => {
      fetch(`${url}/products/public/get`, configuraciones)
      .then(res=>{
        if(!res.ok){
          throw new Error (res.status)
        }else{
          return res.json()
        }
      })
      .then((data)=>{
        setListaDeVehiculos(data.products)})
      .catch((error) => console.error(error) )
    }, [])


    useEffect(() => {
      setCategoriasSeleccionadas(prevCategorias => {
        if (!prevCategorias.includes(parseInt(id))) {
          return [...prevCategorias, parseInt(id)];
        }
        return prevCategorias;
      });
    }, [id]);


    const handleCheckboxChange = (categoriaId) => {

      if (categoriasSeleccionadas.includes(categoriaId)) {
        setCategoriasSeleccionadas(categoriasSeleccionadas.filter(id => id !== categoriaId));
      } else {
        setCategoriasSeleccionadas([...categoriasSeleccionadas, categoriaId]);
      }
    };


    
      useEffect(() => {
        fetch(`${url}/category/public`)
        .then(res=>{
          if(!res.ok){
            throw new Error(res.status)
          }
          else{
            return res.json()
          }
        })
        .then((data)=>{
          setCategoriasEnBaseDeDatos(data)
        })
        .catch((error)=>console.error(error))
    
      }, []);


      useEffect(() => {       

        if (categoriasSeleccionadas.length === 0) {
          setVehiculos([])
          setProductosEncontrados(0)
          return
        }

        setVehiculos([])
        setProductosEncontrados(0)

        
        const fetchVehiculos = async () => {
          try {
            const promises = categoriasSeleccionadas.map(async (categoriaId) => {
              const response = await fetch(`${url}/products/public/category/${categoriaId}`);
              if (response.ok) {
                const data = await response.json();
                return data.products;
              } else {
                return [];
              }
            });
            const vehiculosData = await Promise.all(promises);
            const mergedVehiculos = vehiculosData.flat();
            if (mergedVehiculos.length > 0) {
              setVehiculos(prevVehiculos => [...prevVehiculos, ...mergedVehiculos]);
              setProductosEncontrados(prevProductosEncontrados => prevProductosEncontrados + mergedVehiculos.length);
            }
          } catch (error) {
            console.error(error);
          }
        };

        fetchVehiculos();
      }, [categoriasSeleccionadas]);
        

    useEffect(() => {
      const sortedData = [...vehiculos].sort(compareById);
      setVehiculosOrdenados(sortedData)
    }, [vehiculos])

    
    function resultados (categoriaId){
      if(listaDeVehiculos.length === 0){
        return 0
      }
      else{
        const vehiculosEnCategoria = listaDeVehiculos.filter(vehiculo => vehiculo.category.id === categoriaId)
        return vehiculosEnCategoria.length
      }
    }

    
  return (
    <div className={categoriaDeVehiculo.contenedor}>

        <div className={categoriaDeVehiculo.titulos}>
          <h2>Categorias</h2>
          <span onClick={irAtras} style={{ cursor: 'pointer' }}><i style={{ color: '#373866' }} className="fa-solid fa-arrow-left fa-2xl"></i></span>
        </div>

        <div className={categoriaDeVehiculo.buscadorAvanzado}>
          <section className={categoriaDeVehiculo.listadoCategorias}>
            <ul>
                {
                    (categoriasEnBaseDeDatos.length > 0) &&
                    categoriasEnBaseDeDatos.map(categoria=>{
                        return <li key={categoria.id}>
                          <h3>{categoria.name} &#40;{resultados(categoria.id)}&#41;</h3>
                          <input
                            type="checkbox"
                            checked={categoriasSeleccionadas.includes(categoria.id)}
                            onChange={() => handleCheckboxChange(categoria.id)}
                          />
                        </li>
                    })
                }
            </ul>
          </section>

          <div className={categoriaDeVehiculo.seccionResultados}>

            <h3 className={categoriaDeVehiculo.seccionResultadosh3}>Resultados &#40;{productosEncontrados}&#41;</h3>

                <ul>
                  {
                    (vehiculosOrdenados.length > 0) && 
                      vehiculosOrdenados.map((vehiculo) => {
                      return <li key={vehiculo.id}><Cardbuscador vehiculo={vehiculo}/></li>
                    })
                  }
                </ul>

          </div>          
        </div>


    </div>
  )
}

export default Categorias