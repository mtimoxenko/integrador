import React from 'react'
import estiloContacto from "../Estilos/Contacto.module.css"

const Contacto = () => {
    return (
        <div className={estiloContacto.container}>
            <div className={estiloContacto.h2}>
                <h2>Contacto</h2>
            </div>
            <div className={estiloContacto.contactos}>
                <article className={estiloContacto.rol}>
                    <div className={estiloContacto.rolImagen}><img src="/Images/icono_scrum.png" alt="" /></div>
                    <div className={estiloContacto.rolNombre}><h4>Marcos Lopez</h4></div>
                    <div className={estiloContacto.rolEmail}><h4>Email: marcoshld72@gmail.com</h4></div>
                    <div className={estiloContacto.rolRol}><h4>Rol: Scrum</h4></div>
                </article>
                <article className={estiloContacto.rol}>
                    <div className={estiloContacto.rolImagen}><img src="/Images/icono_ux.png" alt="" /></div>
                    <div className={estiloContacto.rolNombre}><h4>Amy Alvarado</h4></div>
                    <div className={estiloContacto.rolEmail}><h4>Email: amyalvaradodecobos@gmail.com</h4></div>
                    <div className={estiloContacto.rolRol}><h4>Rol: UX/UI</h4></div>
                </article>
                <article className={estiloContacto.rol}>
                    <div className={estiloContacto.rolImagen}><img src="/Images/icono_front.png" alt="" /></div>
                    <div className={estiloContacto.rolNombre}><h4>Facundo Elorz</h4></div>
                    <div className={estiloContacto.rolEmail}><h4>Email: elorzfacundo@gmail.com</h4></div>
                    <div className={estiloContacto.rolRol}><h4>Rol: FrontEnd</h4></div>
                </article>
                <article className={estiloContacto.rol}>
                    <div className={estiloContacto.rolImagen}><img src="/Images/icono_front.png" alt="" /></div>
                    <div className={estiloContacto.rolNombre}><h4>Mateo Romano</h4></div>
                    <div className={estiloContacto.rolEmail}><h4>Email: mate.romano83@gmail.com</h4></div>
                    <div className={estiloContacto.rolRol}><h4>Rol: FrontEnd</h4></div>
                </article>
                <article className={estiloContacto.rol}>
                    <div className={estiloContacto.rolImagen}><img src="/Images/icono_front.png" alt="" /></div>
                    <div className={estiloContacto.rolNombre}><h4>Amy Alvarado</h4></div>
                    <div className={estiloContacto.rolEmail}><h4>Email: amyalvaradodecobos@gmail.com</h4></div>
                    <div className={estiloContacto.rolRol}><h4>Rol: FrontEnd</h4></div>
                </article>
                <article className={estiloContacto.rol}>
                    <div className={estiloContacto.rolImagen}><img src="/Images/icono_back.png" alt="" /></div>
                    <div className={estiloContacto.rolNombre}><h4>Maria F. Ardila</h4></div>
                    <div className={estiloContacto.rolEmail}><h4>Email: mfardilam2@gmail.com</h4></div>
                    <div className={estiloContacto.rolRol}><h4>Rol: BackEnd</h4></div>
                </article>
                <article className={estiloContacto.rol}>
                    <div className={estiloContacto.rolImagen}><img src="/Images/icono_back.png" alt="" /></div>
                    <div className={estiloContacto.rolNombre}><h4>Melissa Miranda</h4></div>
                    <div className={estiloContacto.rolEmail}><h4>Email: memiv21@gmail.com</h4></div>
                    <div className={estiloContacto.rolRol}><h4>Rol: BackEnd</h4></div>  
                </article>
                <article className={estiloContacto.rol}>
                    <div className={estiloContacto.rolImagen}><img src="/Images/icono_base.png" alt="" /></div>
                    <div className={estiloContacto.rolNombre}><h4>Luis Curetti</h4></div>
                    <div className={estiloContacto.rolEmail}><h4>Email: curetti.luis@gmail.com</h4></div>
                    <div className={estiloContacto.rolRol}><h4>Rol: Base de datos</h4></div>   
                </article>
                <article className={estiloContacto.rol}>
                    <div className={estiloContacto.rolImagen}><img src="/Images/icono_infra.png" alt="" /></div>
                    <div className={estiloContacto.rolNombre}><h4>Maximo Timochenko</h4></div>
                    <div className={estiloContacto.rolEmail}><h4>Email: mtimochenko@tutanota.com</h4></div>
                    <div className={estiloContacto.rolRol}><h4>Rol: Infraestructura</h4></div>   
                </article>
                <article className={estiloContacto.rol}>
                    <div className={estiloContacto.rolImagen}><img src="/Images/icono_test.png" alt="" /></div>
                    <div className={estiloContacto.rolNombre}><h4>Marcos Lopez</h4></div>
                    <div className={estiloContacto.rolEmail}><h4>Email: marcoshld72@gmail.com</h4></div>
                    <div className={estiloContacto.rolRol}><h4>Rol: Testing</h4></div>
                </article>

            </div>

        </div>
    )
}

export default Contacto