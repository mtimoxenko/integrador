import React from 'react'
import estiloNosotros from "../Estilos/Nosotros.module.css"

const Nosotros = () => {
  return (
    <div className={estiloNosotros.container}>
      <div className={estiloNosotros.h2}>
        <h2>Nosotros</h2>
      </div>
      <div className={estiloNosotros.nosotros}>
        <div className={estiloNosotros.imagen}>
          <img src="./Images/vehiculo_nosotros.jpg" alt="" />
        </div>

        <div className={estiloNosotros.texto}>
          <p>Somos un equipo de ocho estudiantes del programa <strong> Certified Tech Developer de Digital House</strong>, comprometidos con la culminación exitosa de nuestra formación. Como parte integral de este proceso, hemos desarrollado un proyecto integrador que representa un hito significativo en nuestro camino hacia la obtención del diploma de la carrera.</p>
          <p> Nuestro proyecto consistió en la creación de un sitio web orientado a la gestión de reservas de productos, con un enfoque particular en la facilidad de uso y la experiencia del usuario. Entre las características principales que incorporamos se encuentran: </p>
          <ul>
            <li>
              La posibilidad para los usuarios de alquilar productos por un período mínimo de dos días consecutivos.
            </li>
            <li>
              Funcionalidades de búsqueda y filtros avanzados, junto con información detallada para facilitar la toma de decisiones del usuario durante el proceso de reserva.
            </li>
            <li>
              Gestión integral de reservas, permitiendo a los usuarios administrar sus transacciones de manera eficiente.
            </li>
            <li>
              Herramientas de administración para el personal autorizado, facilitando la adición y eliminación de productos según sea necesario.
            </li>
          </ul>
          <p> Optamos por desarrollar un sitio de alquiler de vehículos, el cual cumple con todos los requisitos establecidos para el proyecto integrador. Este incluye un buscador intuitivo con autocompletado, sección de categorías, recomendaciones, detalles de cada vehículo, visualización de fechas reservadas y una función de favoritos para los usuarios registrados. Además, implementamos un panel de administración completo que permite la gestión de vehículos, categorías y usuarios con distintos roles. </p>
          <p> Nuestro enfoque educativo se basa en la metodología de aprendizaje por proyectos, donde combinamos teoría y práctica para adquirir competencias y conocimientos fundamentales. Este enfoque nos ha permitido abordar problemas reales a través de la exploración, la creatividad y la iniciativa, desarrollando tanto habilidades técnicas como blandas. Trabajamos de manera colaborativa, fomentando el trabajo en equipo, la comunicación efectiva y el aprendizaje mutuo a través del feedback constructivo. </p>
          <p> Durante el proceso de desarrollo, nos hemos centrado en los ocho ejes temáticos clave de nuestra formación: Fundamentos, Soft Skill trainings, Infraestructura, Base de Datos, Talleres técnicos complementarios, Back End, Front End y Calidad. </p>
          <p> Durante el desarrollo de este proyecto integrador, cada uno de los ocho integrantes del equipo desempeñó roles específicos, contribuyendo de manera activa y coordinada para alcanzar nuestros objetivos. Establecimos una estructura de roles claramente definida, donde cada miembro se enfocó en áreas especializadas para garantizar el éxito del proyecto. Tuvimos un responsable de la  <strong> Infraestructura </strong>, encargado de gestionar la continuidad de la integración y el despliegue continuo en AWS utilizando Gitlab. En el frente del desarrollo <strong> Back End </strong>, contamos con un equipo dedicado a trabajar en Java con Spring, siguiendo el patrón de diseño MVC, mientras que otro equipo se centró en el <strong> Front End </strong> utilizando tecnologías como HTML, CSS, JavaScript y ReactJS para crear una interfaz dinámica y atractiva para el usuario. Además, asignamos un diseñador <strong> UX/UI </strong> para garantizar una experiencia de usuario intuitiva y eficiente. También tuvimos un miembro del equipo dedicado a implementar metodologías ágiles como <strong> Scrum </strong> para una gestión eficaz del proyecto, así como un <strong> Tester </strong> responsable de realizar pruebas exhaustivas para garantizar la calidad del software. Por último, contamos con una persona en <strong> bases de datos </strong> que se encargó de diseñar y mantener la estructura de nuestra base de datos MySQL. Esta distribución de roles nos permitió trabajar de manera eficiente y colaborativa, aprovechando las fortalezas individuales de cada miembro para lograr un resultado final exitoso. </p>

          <p> Este proyecto integrador representa no solo un hito en nuestro proceso de aprendizaje, sino también un testimonio de nuestro compromiso con la excelencia y la innovación en el desarrollo de soluciones tecnológicas. Estamos orgullosos del trabajo realizado y confiamos en que nuestro proyecto será una contribución valiosa para nuestra formación profesional. </p>

        </div>


      </div>


    </div>
  )
}

export default Nosotros