import React from 'react'
import { useNavigate } from 'react-router-dom'
import { useState, useEffect } from 'react'
import ElementoDeLista from '../../Componentes/ElementoDeLista'
import adminUsuarios from '../../Estilos/Administracion/AdministracionDeUsuarios.module.css'
import Spinner from '../../Componentes/Spinner'

const AdministracionDeUsuarios = () => {

  const [usuarios, setUsuarios] = useState([])
  const [usuarioSwitch, setUsuarioSwitch] = useState(false)
  
  const url = import.meta.env.VITE_API_URL
  
  const rolUser = JSON.parse(localStorage.getItem('rolUsuario'))
  const token = JSON.parse(localStorage.getItem('token'))
  const entidad = "Usuario"


  const navigate = useNavigate()

  const irAtras = () => {
    navigate(-1)
  }

  const switchUsuario = () => {
    setUsuarioSwitch(!usuarioSwitch)
  }

  const configuraciones = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    }
  }

  useEffect(() => {
    fetch(`${url}/admin/getAllUsers`, configuraciones)
    .then(res=>{
      return res.json()
    })
    .then((data)=>{
      setUsuarios(data)
    })
  }, [usuarioSwitch])


  return (
    <>
      {
        (rolUser === 'ADMINISTRADOR' || rolUser === 'SUPER_ADMINISTRADOR') &&
        <div className={adminUsuarios.contenedor}>
          <div className={adminUsuarios.navegacion}>
            <h3>Panel de Administracion</h3>
            <span onClick={irAtras} style={{ cursor: 'pointer', }}><i style={{ color: '#373866' }} className="fa-solid fa-arrow-left fa-2xl"></i></span>
          </div>
          <h2>Listado de Usuarios Registrados</h2>


        {
          (usuarios.length > 0) ?
            <article>
              <div className={adminUsuarios.caracteristicas}>
                <h4 style={{ width: '25%' }}>Email</h4>
                <h4 style={{ width: '25%' }}>Nombre y Apellido</h4>
                <h4 style={{ width: '25%' }}>Rol</h4>
                <h4 style={{ width: '25%' }}>Acciones</h4>
              </div>

              <ul>
                {
                  usuarios.map((usuario) => {
                    return <li key={usuario.email}>
                      <ElementoDeLista setSwitch={switchUsuario} token={token} identificacion={usuario.email} nombre={usuario.name} apellido={usuario.lastName} url={url} entidad={entidad} rol={usuario.userRol}/>
                    </li>
                
                })
                }
              </ul>

            </article> :

            <div style={{display: 'flex', alignSelf: 'center', padding: '96px'}}>
              <Spinner />
            </div>
        }


        </div>


      }

      <div className={adminUsuarios.oculto}>
        <h2>Panel de Administracion no disponible en dispositivos móviles</h2>
      </div>
    </>
  )
}

export default AdministracionDeUsuarios