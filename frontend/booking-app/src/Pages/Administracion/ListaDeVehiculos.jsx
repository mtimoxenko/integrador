import React, { useState, useEffect } from 'react'
import { navegacion, contenedor, oculto, caracteristicas } from '../../Estilos/Administracion/ListaDeVehiculos.module.css'
import ElementoDeLista from '../../Componentes/ElementoDeLista'
import { useNavigate } from 'react-router-dom'
import Spinner from '../../Componentes/Spinner'


const ListaDeVehiculos = () => {

  const navigate = useNavigate()
  const [vehiculos, setVehiculos] = useState([])
  const [vehiculosSwitch, setVehiculosSwitch] = useState(false)
  const [vehiculosOrdenados, setVehiculosOrdenados] = useState([])

  const url = import.meta.env.VITE_API_URL

  const rolUser = JSON.parse(localStorage.getItem('rolUsuario'))
  const entidad = 'Vehiculo'
  const token = JSON.parse(localStorage.getItem('token'))

  const switchVehiculo = () => {
    setVehiculosSwitch(!vehiculosSwitch)
  }

  const compareById = (a, b) => {
    return a.id - b.id;
  }

  useEffect(() => {

    const sortedData = [...vehiculos].sort(compareById);
    setVehiculosOrdenados(sortedData)

  }, [vehiculos])


  const configuraciones = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    body: JSON.stringify({
    criteria: {
      pageableDto: {
        pageNumber: 0,
        pageSize: 250,
        sortBy: 'name',
        direction: 'ASC'
      }
    }
  })
  }


  useEffect(() => {
    fetch(`${url}/products/public/get`, configuraciones)
    .then(res=>{
      if(!res.ok){
        throw new Error (res.status)
      }else{
        return res.json()
      }
    })
    .then((data)=>setVehiculos(data.products))
    .catch((error) => console.error(error) )
  }, [vehiculosSwitch])

  const irAtras = () => {
    navigate(-1)
  }


  return (
    <>
    {
      (rolUser === 'ADMINISTRADOR' || rolUser === 'SUPER_ADMINISTRADOR') &&
        <div className={contenedor}>

          <div className={navegacion}>
            <h3>Panel de Administracion</h3>
            <span onClick={irAtras} style={{cursor: 'pointer'}}><i style={{color: '#373866'}} className="fa-solid fa-arrow-left fa-2xl"></i></span>    
          </div>

          <h2>Vehiculos Registrados</h2>

          {
            (vehiculosOrdenados.length > 0) ?
            <article>
              <div className={caracteristicas}>
                <h4 style={{width: '10%'}}>ID</h4>
                <h4 style={{width: '25%'}}>Auto</h4>
                <h4 style={{width: '20%'}}>Categoria</h4>
                <h4 style={{width: '45%'}}>Acciones</h4>
              </div>

              <ul>
                {
                  vehiculosOrdenados.map((vehiculo) => {
                    return <li key={vehiculo.id}>
                      <ElementoDeLista setSwitch={switchVehiculo} token={token} identificacion={vehiculo.id} nombre={vehiculo.brand+vehiculo.model} categoriaVehiculo={vehiculo.category} url={`${url}/products`} entidad={entidad}/>
                    </li>
                  })
                }
              </ul>

            </article> :
            <div style={{display: 'flex', alignSelf: 'center', padding: '96px'}}>
              <Spinner />
            </div>

          }


        </div>      
    }



      <div className={oculto}>
        <h2>Panel de Administracion no disponible en dispositivos móviles</h2>
      </div>
    </>
  )
}

export default ListaDeVehiculos