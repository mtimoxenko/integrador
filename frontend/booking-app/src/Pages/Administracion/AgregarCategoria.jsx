import React, { useEffect , useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { navegacion, contenedor, oculto, formulario, registro, caracteristicas, fileInput } from '../../Estilos/Administracion/AgregarCategoria.module.css'
import useInput from '../../Hooks/useInput'
import ElementoDeLista from '../../Componentes/ElementoDeLista'
import { toast } from 'react-toastify'
import Spinner from '../../Componentes/Spinner'


const AgregarCategoria = () => {

  const url = import.meta.env.VITE_API_URL
  const rolUser = JSON.parse(localStorage.getItem('rolUsuario'))

  const entidad = 'Categoria'

  const [categoriasEnBaseDeDatos, setCategoriasEnBaseDeDatos] = useState([])
  const [categoriaSwitch, setCategoriaSwitch] = useState(false)
  const token = JSON.parse(localStorage.getItem('token'))

  const navigate = useNavigate()

  const irAtras = () => {
    navigate(-1)
  }

  const switchCategoria = () => {
    setCategoriaSwitch(!categoriaSwitch)
  }

  const nombreCategoria = useInput('text')
  const descripcion = useInput('text')
  const [imagen, setImagen] = useState('');

  const imgOnChange = (event) => {
    const archivoSeleccionado = event.target.files[0];
    setImagen(archivoSeleccionado)
  };

  const payload = {
    name: nombreCategoria.value,
    description: descripcion.value,
    imageEncode: imagen
  }

  let formData = new FormData();

  formData.append('name', nombreCategoria.value);
  formData.append('description', descripcion.value);
  formData.append('image', imagen);
  

  const configuraciones = {
    method: 'POST',
    headers: {
      'Authorization': `Bearer ${token}`,
    },
    body: formData
  }


  const enviarFormulario = (e) => {
    e.preventDefault()

    fetch(`${url}/category`,configuraciones)
    .then(res=>{
      if(!res.ok){
        throw new Error (res.status)
      }
      else{
        return res.json()
      }
    })
    .then((data)=>{
      console.log(data)
      switchCategoria()
      nombreCategoria.onChange({ target: { value: '' } })
      descripcion.onChange({ target: { value: '' } })
      setImagen('')
      toast.success('Categoria registrada en Base de Datos con éxito!', {
        position: "bottom-left",
        autoClose: 7000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: true,
        progress: 0,
        theme: "dark",
      });
    })
    .catch((error)=>{
      console.error('Error!', error)
      return toast.error('Error al enviar el formulario', {
        position: "bottom-left",
        autoClose: 7000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: true,
        progress: 0,
        theme: "dark",
      });
    })
  }


  const configuracionesGet = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    }
  }

  useEffect(() => {
    fetch(`${url}/category/public`, configuracionesGet)
    .then(res=>{
      if(!res.ok){
        throw new Error(res.status)
      }
      else{
        return res.json()
      }
    })
    .then((data)=>{
      setCategoriasEnBaseDeDatos(data)
    })
    .catch((error)=>console.error(error))

  }, [categoriaSwitch]);


  return (
    <>
    {
      (rolUser === 'ADMINISTRADOR' || rolUser === 'SUPER_ADMINISTRADOR') &&

        <div className={contenedor}>
          <div className={navegacion}>
            <h3>Panel de Administracion</h3>
            <span onClick={irAtras} style={{cursor: 'pointer', }}><i style={{color: '#373866'}} className="fa-solid fa-arrow-left fa-2xl"></i></span>   
          </div>
          <h2>Registro de Categorías</h2>
          <div className={registro} >

            <section>
            {
              (categoriasEnBaseDeDatos.length > 0) ?
              <>
                <div className={caracteristicas}>
                  <h4 style={{width: '15%'}}>ID</h4>
                  <h4 style={{width: '40%'}}>Categoria</h4>
                  <h4 style={{width: '45%'}}>Acciones</h4>
                </div>
                <ul>
                  {
                    
                    categoriasEnBaseDeDatos.map((item) => {
                      return <li key={item.id}>
                      <ElementoDeLista setSwitch={switchCategoria} token={token} identificacion={item.id} nombre={item.name} url={`${url}/category`} entidad={entidad}/>
                        </li>
                    })
                  }
                </ul>              
              </>
              :
              <div style={{display: 'flex', alignSelf: 'center', padding: '96px'}}>
                <Spinner />
              </div>
            }
            </section>

            <form onSubmit={enviarFormulario}  className={formulario} >
              <label htmlFor="categoryName">Nombre de la Categoría:</label>
              <input {...nombreCategoria}/>

              <textarea {...descripcion} placeholder='Descripcion' ></textarea>

              <label htmlFor="agregarImagen">Agregar Imagen:</label>
              <input className={fileInput} type="file" accept="image/jpeg,image/png" id="imgCarga" onChange={imgOnChange} />

              <button type="submit">Enviar</button>
            </form>


          </div>
        </div>
    }


      <div className={oculto}>
        <h2>Panel de Administracion no disponible en dispositivos móviles</h2>
      </div>
    </>
  )
}

export default AgregarCategoria