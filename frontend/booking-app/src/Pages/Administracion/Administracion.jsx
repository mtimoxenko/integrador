import React from 'react'
import { useNavigate } from 'react-router-dom'
import administracion from '../../Estilos/Administracion/Administracion.module.css'


const Administracion = () => {

  const navigate = useNavigate()

  const rolUser = JSON.parse(localStorage.getItem('rolUsuario'))


  const navegarAagregarVehiculo = ()=>{
    navigate("/administracion/agregarvehiculo")
  }

  const navegarAlistarVehiculos = ()=>{
    navigate("/administracion/gestiondevehiculos")
  }
  const navegarAgregarCategoria = ()=>{
    navigate("/administracion/agregarcategoria")
  }
  const navegarAdministacionUsuarios = ()=>{
    navigate("/administracion/administracionusuarios")
  }


  return (
    <>
      {
        (rolUser === 'ADMINISTRADOR' || rolUser === 'SUPER_ADMINISTRADOR') &&
          <div className={administracion.container}>
              <h1>ADMINISTRACION</h1>
              <div className={administracion.panel}>
                <div className={administracion.navegaciones}>
                  <button onClick={navegarAagregarVehiculo}>Agregar Vehículo</button>       
                </div>
                <div className={administracion.navegaciones}>
                  <button onClick={navegarAlistarVehiculos}>Lista de Vehículos</button>     
                </div>
                <div className={administracion.navegaciones}>
                  <button onClick={navegarAgregarCategoria}>Categorías</button>     
                </div>
                <div className={administracion.navegaciones}>
                  <button onClick={navegarAdministacionUsuarios}>Usuarios</button>     
                </div>
              </div>
              {/* <img className={administracion.logo} src="/logo.ico" alt="logo" /> */}
          </div>
      }

      
      <div className={administracion.oculto}>
        <h2>Panel de Administracion no disponible en dispositivos móviles</h2>
      </div>
    </>

  )
}

export default Administracion