import React  from 'react'
import { useNavigate } from 'react-router-dom';
import estiloUser from '../Estilos/User.module.css';


const User = () => {

    const navigate = useNavigate();

    const irAtras = () => {
        navigate(-1)
    }

    const nombreUsuario = JSON.parse(localStorage.getItem('nombreUsuario'));
    const apellidoUsuario = JSON.parse(localStorage.getItem('apellidoUsuario'));
    const emailUsuario = JSON.parse(localStorage.getItem('emailUsuario'));
    const rolUsuario = JSON.parse(localStorage.getItem('rolUsuario'));
    const tokenUsuario = JSON.parse(localStorage.getItem('token'));

    const iniciales = (str) => {
        return str.charAt(0).toUpperCase();
    }

    const inicialNombre = iniciales(nombreUsuario);
    const inicialApellido = iniciales(apellidoUsuario);

    return (
        <div className={estiloUser.divPrincipal}>
            <div className={estiloUser.divTituloFlecha}>
                <h2>Perfil de Usuario</h2>
                <span onClick={irAtras} style={{ cursor: 'pointer' }}><i style={{ color: '#373866' }} className="fa-solid fa-arrow-left fa-2xl"></i></span>
            </div>
            
            <div className={estiloUser.avatar}>
                <h3 >{inicialNombre}{inicialApellido}</h3>
            </div>
            
            <article className={estiloUser.divCaracteristicas}>
                <div className={estiloUser.divNombre}>
                    <h4 >Nombre: </h4>
                    <h3 >{nombreUsuario}</h3>
                </div>
                <div className={estiloUser.divApellido}>
                    <h4 >Apellido: </h4>
                    <h3 >{apellidoUsuario}</h3>
                </div>
                <div className={estiloUser.divEmail}>
                    <h4 >Email: </h4>
                    <h3 >{emailUsuario}</h3>
                </div>
                {
                    (rolUsuario === 'ADMINISTRADOR' || rolUsuario === 'SUPER_ADMINISTRADOR') &&
                    <div className={estiloUser.divRol}>
                        <h4 >Rol: </h4>
                        <h3 >{rolUsuario}</h3>
                    </div>                    
                }

            </article>


        </div>
    )
}

export default User;