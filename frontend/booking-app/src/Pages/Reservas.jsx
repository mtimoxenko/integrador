import React, { useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import reservasStyles from '../Estilos/Reservas.module.css'
import Alert from '../Componentes/Alert';

const Reservas = () => {

  const navigate = useNavigate()
  const { id } = useParams()
  const [vehiculo, setVehiculo] = useState({})
  const [imgs, setImgs] = useState([])
  const [category, setCategory] = useState({})
  const [dataMessage, setDataMessage] = useState('')
  const [alertError, setAlertError] = useState(false)

  const nombreUsuario = JSON.parse(localStorage.getItem('nombreUsuario'))
  const apellidoUsuario = JSON.parse(localStorage.getItem('apellidoUsuario'))
  const emailUsuario = JSON.parse(localStorage.getItem('emailUsuario'))
  const tokenUsuario = JSON.parse(localStorage.getItem('token'))
  const idUsuario = JSON.parse(localStorage.getItem('userId'))

  const [inicio, setInicio] = useState("")
  const [fin, setFin] = useState("")

  const url = import.meta.env.VITE_API_URL

  const switchAlertError =()=>{
    setAlertError(!alertError)
  }

useEffect(() =>{
  if(sessionStorage.getItem('fechaInicio') && sessionStorage.getItem('fechaFin')){
    setInicio(JSON.parse(sessionStorage.getItem('fechaInicio')))
    setFin(JSON.parse(sessionStorage.getItem('fechaFin')))
    return sessionStorage.clear()
  }else {
    navigate("/")
  }
},[])


  const irAtras = () => {
    navigate(-1)
  }

  useEffect(() => {
    fetch(`${url}/products/public/${id}`)
      .then(res => {
        if (!res.ok) {
          throw new Error(`Error en la solicitud: ${res.status}`)
        }
        else {
          return res.json()
        }
      })
      .then((data) => {
        setVehiculo(data.currentProduct)
        setImgs(data.currentProduct.images)
        setCategory(data.currentProduct.category)
      })
      .catch(error => console.error(error))
  }, [])


  const partesFechaInicio = inicio.split('-');

  const anioInicio = parseInt(partesFechaInicio[0], 10);
  const mesInicio = parseInt(partesFechaInicio[1], 10) - 1;
  const diaInicio = parseInt(partesFechaInicio[2], 10);
  const fechaInicioParseada = new Date(anioInicio, mesInicio, diaInicio);

  const partesFechaFin = fin.split('-');

  const anioFin = parseInt(partesFechaFin[0], 10);
  const mesFin = parseInt(partesFechaFin[1], 10) - 1;
  const diaFin = parseInt(partesFechaFin[2], 10);
  const fechaFinParseada = new Date(anioFin, mesFin, diaFin);


  const payload = {
    user_id: parseInt(idUsuario),
    product_id: parseInt(id),
    startDate: fechaInicioParseada,
    endDate: fechaFinParseada
  }

  const configuracionesPost = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${tokenUsuario}`
    },
    body: JSON.stringify(payload)
  }


  const reservar = () => {

    fetch(`${url}/reservation`, configuracionesPost)
      .then(res => {
        if (!res.ok) {
          throw new Error(res.status)
        }
        return res.json()
      })
      .then((data) => {
        setDataMessage(data.message)
        sessionStorage.setItem('mensaje', JSON.stringify(data.message))
        sessionStorage.setItem('fechaInicio', JSON.stringify(inicio))
        sessionStorage.setItem('fechaFin', JSON.stringify(fin))
        navigate("/reservaconfirmada")
      })
      .catch(error => {
        switchAlertError()
        console.error(error)
      })
  }

  const combustible = (fuel) =>{
    if (fuel === 'GNC') {
        return fuel
    }
    const combustibleCap = fuel.charAt(0).toUpperCase() + fuel.slice(1).toLowerCase()
    return combustibleCap
  }


  return (
    <>
    {vehiculo.id &&
  
    <div className={reservasStyles.container}>

      <div className={reservasStyles.divTituloFlecha}>
        <h2>Reservas</h2>
      </div>

      <div className={reservasStyles.divPrincipalReservas}>
        <div className={reservasStyles.divInfoReservas}>

          <section className={reservasStyles.detalles}>
            <h2>Datos del vehículo</h2>
            <h3><strong>Nombre: </strong>{vehiculo.name}</h3>
            <h3><strong>Marca: </strong>{vehiculo.brand}</h3>
            <h3><strong>Modelo: </strong>{vehiculo.model}</h3>
            <h3><strong>Descripción: </strong>{vehiculo.description}</h3>
          </section>

          <section className={reservasStyles.caracteristicas} >
            <h2>Características</h2>

            <ul>
              <li style={{ display: "flex", alignItems: "center", gap: "10px" }}>
                <span><i className="fa-solid fa-car-side"></i></span>
                {(vehiculo.isAutomatic) ? <p>Automático</p> : <p>Manual</p>}
              </li>
              <li style={{ display: "flex", alignItems: "center", gap: "10px" }}>
                <span><i className="fa-solid fa-suitcase"></i></span><p>{vehiculo.numBags}</p>
              </li>
              <li style={{ display: "flex", alignItems: "center", gap: "10px" }}>
                <span><i className="fa-brands fa-bluetooth"></i></span><p>Bluetooth</p>
              </li>
              <li style={{ display: "flex", alignItems: "center", gap: "10px" }}>
                <span><i className="fa-solid fa-snowflake"></i></span><p>A/C</p>
              </li>

              <li style={{ display: "flex", alignItems: "center", gap: "10px" }}><span><i className="fa-solid fa-car-side"></i></span>
                {
                  (vehiculo.isAutomatic) ? <p>Automático</p> : <p>Manual</p>
                }</li>
              <li style={{ display: "flex", alignItems: "center", gap: "10px" }}><span><i className="fa-solid fa-suitcase"></i></span><p>{vehiculo.numBags}</p></li>

              <li style={{ display: "flex", alignItems: "center", gap: "10px" }}><span><i className="fa-brands fa-bluetooth"></i></span><p>Bluetooth</p></li>
              <li style={{ display: "flex", alignItems: "center", gap: "10px" }}><span><i className="fa-solid fa-snowflake" ></i></span><p>A/C</p></li>

              <li style={{ display: "flex", alignItems: "center", gap: "10px" }}>
                <span>
                  <i className={(vehiculo.fuel === 'ELECTRICO') ? "fas fa-bolt fa-lg" : "fas fa-gas-pump fa-lg"}></i>
                </span>
                <p>{combustible(vehiculo.fuel)}</p>
              </li>
              <li style={{ display: "flex", alignItems: "center", gap: "10px" }}><span><i className="fas fa-user"></i></span><p>{vehiculo.numPassengers}</p></li>
            </ul>
          </section>


          <section className={reservasStyles.imagenes}>
            <img className={reservasStyles.imagenesDiv} src={imgs[0] ? imgs[0].imageUrl : ''} alt='' />
            <img className={reservasStyles.imagenesDiv} src={imgs[0] ? imgs[1].imageUrl : ''} alt='' />
            <img className={reservasStyles.imagenesDiv} src={imgs[0] ? imgs[2].imageUrl : ''} alt='' />
            <img className={reservasStyles.imagenesDiv} src={imgs[0] ? imgs[3].imageUrl : ''} alt='' />
            <img className={reservasStyles.imagenesDiv} src={imgs[0] ? imgs[4].imageUrl : ''} alt='' />
          </section>

        </div>

        <div className={reservasStyles.divInformacion}>
          <section className={reservasStyles.divBotonReserva}>
            <h3><strong>Fecha inicio de la reserva:</strong></h3>
            <h3 style={{ color: "#B93442" }}>{inicio}</h3>
            <h3 ><strong>Fecha fin de la reserva:</strong></h3>
            <h3 style={{ color: "#B93442" }}>{fin}</h3>
            <div className={reservasStyles.divButton}>
              <button onClick={reservar}>Confirmar reserva</button>
              <button style={{ background: "#B93442" }} onClick={irAtras}>Seleccionar otra fecha</button>
            </div>
          </section>

          <section className={reservasStyles.divInformacionUsuario}>
            <h2>Datos del usuario</h2>
            <h3><strong>Nombre: </strong>{nombreUsuario}</h3>
            <h3><strong>Apellido: </strong>{apellidoUsuario}</h3>
            <h3><strong>Email: </strong>{emailUsuario}</h3>
          </section>

        </div>
      </div>
          {
            alertError && 
            <Alert mensaje="⚠️ Ha ocurrido un error! Verifica tus datos o vuelve a seleccionar otra fecha." aceptar={switchAlertError} container="Reservas" />
          }
    </div>}
    </>
  )
}

export default Reservas