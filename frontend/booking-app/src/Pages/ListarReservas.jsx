import React, { useEffect, useState } from 'react'
import listarRStyles from "../Estilos/ListarReservas.module.css"
import { useNavigate } from 'react-router-dom';

const ListarReservas = () => {

    const url = import.meta.env.VITE_API_URL
    const navigate = useNavigate()

    const [listaReservas, setListarReservas] = useState([])

    const id = JSON.parse(localStorage.getItem('userId'))
    const token = JSON.parse(localStorage.getItem('token'))

    const irAtras = () => {
        navigate(-1)
    }

    const [listaDeVehiculos, setListaDeVehiculos] = useState([])

    const recorrerVehiculosMarca = (itemId) => {
        if (listaDeVehiculos.length > 0) {
            const datosVehiculo = listaDeVehiculos.find(vehiculo => vehiculo.id === itemId)
            console.log(datosVehiculo);
            return datosVehiculo.brand            
        }
    }

    const recorrerVehiculosModelo = (itemId) => {
        if (listaDeVehiculos.length > 0) {
            const datosVehiculo = listaDeVehiculos.find(vehiculo => vehiculo.id === itemId)
            console.log(datosVehiculo);
            return datosVehiculo.model            
        }
    }

    const recorrerVehiculosCategoria = (itemId) => {
        if (listaDeVehiculos.length > 0) {
            const datosVehiculo = listaDeVehiculos.find(vehiculo => vehiculo.id === itemId)
            console.log(datosVehiculo);
            return datosVehiculo.category.name            
        }
    }

    const payload = {
      criteria: {
        pageableDto: {
            pageNumber: parseInt(0),
            pageSize: parseInt(500),
            sortBy: "name",
            direction: "ASC"
        }
    }}
    
    const configuracionesGetCarros = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(payload)
    }
  
    useEffect(() => {
      fetch(`${url}/products/public/get`, configuracionesGetCarros)
      .then(res=>{
        if(!res.ok){
          throw new Error (res.status)
        }else{
          return res.json()
        }
      })
      .then((data)=>{
        setListaDeVehiculos(data.products)})
      .catch((error) => console.error(error) )
    }, [])


    const configuraciones = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    }

    useEffect(() => {
        fetch(`${url}/reservation/byUser/${id}`, configuraciones)
            .then(res => {
                if (!res.ok) {
                    throw new Error(res.status)
                } else {
                    return res.json()
                }
            })
            .then((data) => {
                setListarReservas(data.reservations)
            })
            .catch(error => console.error(error))
    }, [])


    return (
        <div className={listarRStyles.container}>
            <div className={listarRStyles.divTituloFlecha}>
                <h2>Tus Reservas</h2>
                <span onClick={irAtras} style={{ cursor: 'pointer' }}><i style={{ color: '#373866' }} className="fa-solid fa-arrow-left fa-2xl"></i></span>
            </div>

            <table className={listarRStyles.minimalistBlack}>
                <thead>
                    <tr>
                        <th>Categoría</th>
                        <th>Marca</th>
                        <th>Modelo</th>
                        <th>Fecha inicio de reserva</th>
                        <th>Fecha fin de reserva</th>
                    </tr>
                </thead>

                        {
                            listaReservas.length > 0 && 
                            
                            <tbody>
                                
                            {
                                listaReservas.map((reserva, index) => {
                                return <tr key={index}>
                                    <td>
                                        {
                                            recorrerVehiculosCategoria(reserva.product_id)
                                        }
                                    </td>
                                    <td>
                                        {
                                            recorrerVehiculosMarca(reserva.product_id)
                                        }
                                    </td>
                                    <td>
                                        {
                                            recorrerVehiculosModelo(reserva.product_id)
                                        }
                                    </td>
                                    <td>{reserva.startDate[2]}-{reserva.startDate[1]}-{reserva.startDate[0]}</td>
                                    <td>{reserva.endDate[2]}-{reserva.endDate[1]}-{reserva.endDate[0]}</td>
                                </tr>
                                })
                            }

                            </tbody>
                        }

            </table>

            {listaReservas.length === 0 && <p>No hay reservas programadas aún</p>}

        </div >
    )
}

export default ListarReservas