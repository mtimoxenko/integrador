import React, { useState, useEffect } from 'react'
import favoritos from '../Estilos/Favoritos.module.css'
import Carddetalle from '../Componentes/Carddetalle'
import { useNavigate } from 'react-router-dom'


export const Favoritos = () => {

  const url = import.meta.env.VITE_API_URL
  const navigate = useNavigate();

  const [favoritosDelUsuario, setFavoritosDelUsuario] = useState([])
  const [switchFavoritos, setSwitchFavoritos] = useState(false)
  const [animationName, setAnimationName] = useState(false)

  const id = JSON.parse(localStorage.getItem('userId'))
  const token = JSON.parse(localStorage.getItem('token'))


  const irAtras = () => {
    navigate(-1)
  }


  const switchFav = () => {
    setSwitchFavoritos(!switchFavoritos)
  }

  const configuraciones = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    }
  }

  useEffect(() => {

    fetch(`${url}/favorito/${id}`, configuraciones)
      .then(res => {
        if (!res.ok) {
          throw new Error(res.status)
        }
        else {
          return res.json()
        }
      })
      .then((data) => {
        setFavoritosDelUsuario(data)
      })
      .catch(error => console.error(error))

  }, [switchFavoritos])

  const clickFav = () => {
    setAnimationName(true)
    setTimeout(() => {
      setAnimationName(false)      
    }, 10);
  }


  return (
    <div className={favoritos.divPrincipal}>
      <div className={favoritos.divTituloFlecha}>
        <div className={favoritos.titulo}>
          <div className={favoritos.corazones}>
            <img className={favoritos.corazon1} style={{animationName: (!animationName) ? '' : 'bounce'}} src="/Images/love/love_blue3.png" alt="" />
            <img className={favoritos.corazon2} style={{width:'18px', animationName: (!animationName) ? '' : 'bounce'}} src="/Images/love/love_blue3.png" alt="" />
            <img className={favoritos.corazon3} style={{animationName: (!animationName) ? '' : 'bounce'}} src="/Images/love/love_blue3.png" alt="" />            
          </div>
          <h2 onClick={clickFav}>Favoritos</h2>
          <div className={favoritos.corazones}>
            <img className={favoritos.corazon4} style={{animationName: (!animationName) ? '' : 'bounce'}} src="/Images/love/love_blue3.png" alt="" />
            <img className={favoritos.corazon5} style={{width:'18px', animationName: (!animationName) ? '' : 'bounce'}} src="/Images/love/love_blue3.png" alt="" />
            <img className={favoritos.corazon6} style={{animationName: (!animationName) ? '' : 'bounce'}} src="/Images/love/love_blue3.png" alt="" />            
          </div>
        </div>
        <span onClick={irAtras} style={{ cursor: 'pointer' }}><i style={{ color: '#373866' }} className="fa-solid fa-arrow-left fa-2xl"></i></span>
      </div>

      <div className={favoritos.listado}>

        {
          favoritosDelUsuario.length > 0 ?
            <ul>
                {favoritosDelUsuario.map((favorito) => {
                  return <li key={favorito.idFav}>
                    <Carddetalle vehiculo={favorito.productDto} switchFav={switchFav} />
                  </li>
                })}
            </ul>
            :
            <p>No hay favoritos en tu lista</p>
        }
      </div>
    </div>
  )
}
