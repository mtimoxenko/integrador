
# Docker Image Selector Script for Car Rental Application

This Bash script is designed to streamline the process of selecting Docker image tags from Docker Hub for both the frontend and backend components of the Car Rental application, and to automate the generation and execution of a `docker-compose.yml` file.

## Features

- **Interactive Tag Selection**: Fetches available Docker tags from Docker Hub for both frontend and backend images, allowing the user to select which version to deploy.
- **Automatic `docker-compose.yml` Generation**: Generates a `docker-compose.yml` file based on the selected tags, setting up the service infrastructure.
- **Simplified Deployment and Teardown**: Offers commands to easily bring up or tear down the application using Docker Compose.

## Prerequisites

- Bash shell
- `curl` and `jq` installed to fetch and parse Docker Hub tags
- Docker and Docker Compose installed

## Usage

### Running the Application

To start the application with your selected Docker images, run:

```bash
./docker_image_selector.sh up
```

This command will:
1. Prompt you to select tags for the frontend and backend images.
2. Generate a `docker-compose.yml` file with the selected images.
3. Bring up the services using Docker Compose.

### Stopping the Application

To stop and remove the application containers, run:

```bash
./docker_image_selector.sh down
```

This will use Docker Compose to stop the running services and remove the containers.

## Customization

The script is set up for the `fr3m3n/car-rental-frontend` and `fr3m3n/car-rental-backend` repositories. Modify the script as necessary to point to different Docker Hub repositories or to customize the Docker Compose configuration.
