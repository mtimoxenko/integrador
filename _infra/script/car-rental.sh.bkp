#!/bin/bash

# Function to fetch and select a Docker image tag from Docker Hub
choose_tag_from_dockerhub() {
    local repository=$1
    echo "Fetching tags for $repository..." >&2
    
    # Fetch tags from Docker Hub
    local tags=$(curl -s "https://hub.docker.com/v2/repositories/${repository}/tags/?page_size=100" | jq -r '.results|.[]|.name')
    
    # Check if we got any tags
    if [ -z "$tags" ]; then
        echo "No tags found for repository $repository. Please check the repository name and try again." >&2
        exit 1
    fi

    echo "Available tags for $repository:" >&2
    local i=1
    local options=()
    for tag in $tags; do
        echo "$i) $tag" >&2
        options+=("$tag")
        let "i+=1"
    done

    # Ask the user to choose a tag with a 5-second timeout
    echo "Enter the number of the tag you want to use (automatically selects the first option after 5 seconds):" >&2
    read -t 5 tag_choice || echo "Timeout reached, automatically selecting the first option." >&2
    
    # If no input is provided (or timeout occurs), default to the first option
    if [ -z "$tag_choice" ]; then
        selected_tag=${options[0]}
    else
        selected_tag=${options[$tag_choice-1]}
    fi

    if [ -z "$selected_tag" ]; then
        echo "Invalid selection. Exiting." >&2
        exit 1
    fi

    # Only the selected tag is echoed to stdout, everything else is directed to stderr
    echo "$selected_tag"
}

run_application() {
    echo "Select a tag for the frontend image:"
    FRONTEND_TAG=$(choose_tag_from_dockerhub "fr3m3n/car-rental-frontend")
    FRONTEND_IMAGE="fr3m3n/car-rental-frontend:$FRONTEND_TAG"

    echo "Select a tag for the backend image:"
    BACKEND_TAG=$(choose_tag_from_dockerhub "fr3m3n/car-rental-backend")
    BACKEND_IMAGE="fr3m3n/car-rental-backend:$BACKEND_TAG"

    # Use the captured tags to generate the docker-compose.yml
    cat <<EOF > docker-compose.yml
version: '3.8'
services:
  frontend:
    container_name: car-rental-frontend
    image: $FRONTEND_IMAGE
    ports:
      - "3000:80"
    depends_on:
      - backend
    networks:
      - car_rental_network

  backend:
    container_name: car-rental-backend
    image: $BACKEND_IMAGE
    ports:
      - "8080:8080"
    depends_on:
      - db
    networks:
      - car_rental_network

  db:
    container_name: car-rental-db
    image: mysql:5.7
    volumes:
      - car_rental_db_data:/var/lib/mysql
      - ./my.cnf:/etc/mysql/conf.d/custom.cnf # Mount the custom config file
    environment:
      MYSQL_ROOT_PASSWORD: rootpassword
      MYSQL_DATABASE: carrental
      MYSQL_USER: carrentaluser
      MYSQL_PASSWORD: carrentalpass
    ports:
      - "3306:3306"
    networks:
      - car_rental_network

  phpmyadmin:
    container_name: car-rental-phpmyadmin
    image: phpmyadmin/phpmyadmin
    ports:
      - "8081:80"
    environment:
      PMA_HOST: db
      PMA_USER: carrentaluser
      PMA_PASSWORD: carrentalpass
    depends_on:
      - db
    networks:
      - car_rental_network

  nagios:
    container_name: nagios
    image: jasonrivers/nagios:latest
    ports:
      - "8082:80"
    volumes:
      - /opt/car-rental/nagios/etc/:/opt/nagios/etc/
      - /opt/car-rental/nagios/var:/opt/nagios/var/
      - /opt/car-rental/nagios/custom-plugins:/opt/Custom-Nagios-Plugins
    depends_on:
      - db
      - backend
      - frontend
    networks:
      - car_rental_network

networks:
  car_rental_network:
    driver: bridge      

volumes:
  car_rental_db_data:
EOF

    echo "docker-compose.yml has been generated with the selected images."

    # Run Docker Compose up
    docker-compose up -d
}

# Function to stop the application
stop_application() {
    docker-compose down
}

# Main logic based on the command-line argument
case "$1" in
    up)
        run_application
        ;;
    down)
        stop_application
        ;;
    *)
        echo "Usage: $0 {up|down}"
        exit 1
        ;;
esac
