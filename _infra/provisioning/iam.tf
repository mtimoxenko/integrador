# ==========================================
# Scope: IAM policies
# Author: Maximo Timochenko
# Date: 17.03.24
# Version: 1.0
# ==========================================

# IAM policy granting basic access to the S3 bucket
resource "aws_iam_policy" "s3_bucket_basic_access" {
  name        = "s3_bucket_basic_access"
  description = "Basic access to car-rental-uploads S3 bucket"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Action = [
          "s3:GetObject",
          "s3:PutObject",
          "s3:DeleteObject"
        ],
        Resource = [
          "${aws_s3_bucket.car_rental_uploads.arn}/*" # Allows actions on objects in the bucket
        ],
      },
    ],
  })
}

# IAM role for the EC2 instance
resource "aws_iam_role" "ec2_s3_access_role" {
  name = "ec2_s3_access_role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Principal = {
          Service = "ec2.amazonaws.com"
        },
        Action = "sts:AssumeRole",
      },
    ],
  })
}

# IAM policy granting access to manage the S3 bucket and bucket policy
resource "aws_iam_policy" "s3_bucket_policy_management" {
  name        = "s3_bucket_policy_management"
  description = "Manage car-rental-uploads S3 bucket and its policy"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Action = [
          "s3:PutBucketPolicy",  # Permission to update bucket policy
          "s3:GetBucketPolicy",  # Permission to retrieve bucket policy
          "s3:DeleteBucketPolicy" # Permission to delete bucket policy
        ],
        Resource = [
          "${aws_s3_bucket.car_rental_uploads.arn}" # Policy actions on the bucket itself
        ]
      }
    ]
  })
}

# Attach the IAM policies to the role
resource "aws_iam_role_policy_attachment" "s3_basic_access_attachment" {
  role       = aws_iam_role.ec2_s3_access_role.name
  policy_arn = aws_iam_policy.s3_bucket_basic_access.arn
}

resource "aws_iam_role_policy_attachment" "s3_policy_management_attachment" {
  role       = aws_iam_role.ec2_s3_access_role.name
  policy_arn = aws_iam_policy.s3_bucket_policy_management.arn
}

# Instance profile for the IAM role
resource "aws_iam_instance_profile" "ec2_s3_access_profile" {
  name = "ec2_s3_access_profile"
  role = aws_iam_role.ec2_s3_access_role.name
}
