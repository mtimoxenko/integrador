# ==========================================
# Scope: S3 Bucket for Image Uploads
# Author: Maximo Timochenko
# Date: 17.03.24
# Version: 1.0
# ==========================================

resource "aws_s3_bucket" "car_rental_uploads" {
  bucket = "car-rental-uploads-booking-app-2"
  force_destroy = true
  
  tags = {
    Name = "CarRentalUploads"
  }
}

resource "aws_s3_bucket_public_access_block" "car_rental_uploads_public_access_block" {
  bucket = aws_s3_bucket.car_rental_uploads.id

  block_public_acls   = false
  block_public_policy = false
  ignore_public_acls  = false
  restrict_public_buckets = false
}

resource "aws_s3_bucket_policy" "car_rental_uploads_policy" {
  bucket = aws_s3_bucket.car_rental_uploads.id

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Principal = "*",
        Action = "s3:GetObject",
        Resource = "${aws_s3_bucket.car_rental_uploads.arn}/*"
      }
    ]
  })
}
