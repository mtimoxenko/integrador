# ==================================================================
# Scope: NAT Gateway and Elastic IP configurations
# Author: fr3m3n
# Date: 17.03.24
# Version: 1.0
# ==================================================================

# Allocate an Elastic IP for the NAT Gateway
# resource "aws_eip" "nat_eip" {
#   domain = "vpc" # Allocate the EIP in the VPC.

#   tags = {
#     Name = "NAT EIP"
#   }
# }

# Create a NAT Gateway in the public subnet.
# resource "aws_nat_gateway" "private_nat_gateway" {
#   allocation_id = aws_eip.nat_eip.id
#   subnet_id     = aws_subnet.car_rental_public_subnet.id # Place the NAT Gateway in the public subnet

#   tags = {
#     Name = "Private Subnet NAT Gateway"
#   }
# }
