# ==========================================
# Scope: Virtual Private Cloud definition
# Author: Maximo Timochenko
# Date: 15.03.24
# Version: 1.0
# ==========================================


# ===============[ VPC CIDR Block ]=======================
# IP addresses range from [10.0.1.0] to [10.0.1.63]
#
#   Reserved addresses:
#     [10.0.1.0]: Network address (the starting IP of the VPC's CIDR range)
#     [10.0.1.1]: Reserved by AWS for the VPC router (enables communication within the VPC)
#  -> [10.0.1.2] - [10.0.1.13]: Usable IPs for public subnet
#     [10.0.1.14]: Reserved by AWS for DNS services (public subnet, not typically usable)
#     [10.0.1.15]: Public subnet broadcast address (marks the end of the public subnet)
#     [10.0.1.16]: Private subnet network address (the starting IP of the first private subnet's CIDR range)
#     [10.0.1.17]: Reserved by AWS for the VPC router (first private subnet)
#  -> [10.0.1.18] - [10.0.1.29]: Usable IPs for first private subnet
#     [10.0.1.30]: Reserved by AWS for future use (first private subnet)
#     [10.0.1.31]: First private subnet broadcast address (marks the end of the first private subnet)
#     [10.0.1.32]: Additional private subnet network address (the starting IP of the second private subnet's CIDR range)
#     [10.0.1.33]: Reserved by AWS for the VPC router (second private subnet)
#  -> [10.0.1.34] - [10.0.1.45]: Usable IPs for second private subnet
#     [10.0.1.46]: Reserved by AWS for future use (second private subnet)
#     [10.0.1.47]: Second private subnet broadcast address (marks the end of the second private subnet)
#  *  [10.0.1.48] - [10.0.1.62]: Remaining IPs available for future subnet or expansion
#     [10.0.1.63]: Broadcast address for the entire VPC CIDR block
#
#       => 12 usable IPs for each defined subnet
#       => 14 IPs available for potential future subnet or assignment
# =========================================================
resource "aws_vpc" "car_rental_vpc" {
  cidr_block = "10.0.1.0/26"
  instance_tenancy = "default" # The instance is on "shared" hardware.
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name = "car_rental_vpc"
  }
}



# ==========[ Public Subnet ]=============================
# IP addresses range from [10.0.1.0] to [10.0.1.15]
#   ...
#       => 12 usable IPs for public subnet: [10.0.1.2] to [10.0.1.13]
# =========================================================
resource "aws_subnet" "car_rental_public_subnet" {
  vpc_id     = aws_vpc.car_rental_vpc.id
  cidr_block = "10.0.1.0/28"
  map_public_ip_on_launch = true

  tags = {
    Name = "car_rental_public_subnet"
  }
}


# ==========[ Private Subnet ]=============================
# IP addresses range from [10.0.1.16] to [10.0.1.31]
#   ...
#       => 12 usable IPs for private subnet: [10.0.1.18] to [10.0.1.29]
# =========================================================
resource "aws_subnet" "car_rental_private_subnet" {
  vpc_id     = aws_vpc.car_rental_vpc.id
  cidr_block = "10.0.1.16/28"
  map_public_ip_on_launch = false

  tags = {
    Name = "car_rental_private_subnet"
  }
}


# ==========[ Additional Private Subnet ]=============================
# IP addresses range from [10.0.1.32] to [10.0.1.47]
#   ...
#       => 12 usable IPs for the additional private subnet: [10.0.1.34] to [10.0.1.45]
# =========================================================
resource "aws_subnet" "car_rental_private_subnet_az2" {
  vpc_id            = aws_vpc.car_rental_vpc.id
  cidr_block        = "10.0.1.32/28"
  availability_zone = "us-east-1b"
  map_public_ip_on_launch = false

  tags = {
    Name = "car_rental_private_subnet_az2"
  }
}




# Internet Gateway for the VPC to enable access to the internet
resource "aws_internet_gateway" "car_rental_igw" {
  vpc_id = aws_vpc.car_rental_vpc.id

  tags = {
    Name = "car_rental_igw"
  }
}
