# ==========================================
# Scope: Amazon RDS Instance
# Author: Maximo Timochenko
# Date: 17.03.24
# Version: 1.0
# ==========================================

resource "aws_db_instance" "car_rental_db" {
  allocated_storage    = 20
  storage_type         = "gp2"
  engine               = "mysql"
  engine_version       = "8.0"
  instance_class       = "db.t3.micro"
  identifier           = "carrentaldb"
  username             = "admin"
  password             = "yoursecretpassword"
  parameter_group_name = "default.mysql8.0"
  db_subnet_group_name = aws_db_subnet_group.car_rental_db_subnet_group.name
  vpc_security_group_ids = [aws_security_group.rds_sg.id]

  tags = {
    Name = "CarRentalDB"
  }
}

resource "aws_db_subnet_group" "car_rental_db_subnet_group" {
  name       = "car-rental-db-subnet-group"
  subnet_ids = [
    aws_subnet.car_rental_private_subnet.id,
    aws_subnet.car_rental_private_subnet_az2.id  # Including the new subnet
  ]

  tags = {
    Name = "car_rental_db_subnet_group"
  }
}


output "db_endpoint" {
  value = aws_db_instance.car_rental_db.endpoint
}
