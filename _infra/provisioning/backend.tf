# ==========================================
# Scope: Backend config
# Author: Maximo Timochenko
# Date: 18.03.24
# Version: 1.0
# ==========================================

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }

  backend "http" {
  }
}

provider "aws" {
  region = "us-east-1"
}
