# ==================================================================
# Scope: Route tables and their associations.
# Author: fr3m3n
# Date: 17.03.24
# Version: 1.0
# ==================================================================

# Public Route Table
resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.car_rental_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.car_rental_igw.id
  }

  tags = {
    Name = "Public Route Table"
  }
}

# Associate the public route table with the public subnet
resource "aws_route_table_association" "public_subnet_association" {
  subnet_id      = aws_subnet.car_rental_public_subnet.id
  route_table_id = aws_route_table.public_route_table.id
}

# Private Route Table
resource "aws_route_table" "private_route_table" {
  vpc_id = aws_vpc.car_rental_vpc.id

  # route {
  #   cidr_block    = "0.0.0.0/0"
  #   nat_gateway_id = aws_nat_gateway.private_nat_gateway.id
  # }

  tags = {
    Name = "Private Route Table"
  }
}

# Associate the private route table with the private subnet
resource "aws_route_table_association" "private_subnet_association" {
  subnet_id      = aws_subnet.car_rental_private_subnet.id
  route_table_id = aws_route_table.private_route_table.id
}
