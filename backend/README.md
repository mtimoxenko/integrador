# Backend Service
The program was designed in Spring boot v3.2.2, therefore Java version 17 or higher is recommended

The program was designed with an MVC architecture, using Maven as a manager. On the other hand, the implemented dependencies are in the pom, it is important to update them and check the compatibility between them as the versions are modified.

## Usage
Can be found in the version of the program running locally, which uses H2 as a temporary database engine and saves the images in the 'Uploads' folder on the machine where it runs, receiving it as a Multipartfile in the Controller.

## Deploy version
The deployed version of the program is available at: http://184.73.88.160:3000
;It is located in AWS, connected to a DB that is located in an RDS and manages the images by uploading and consuming them from an S3.

### Postman test
The Postman collection used to test the program and the program configurations are available in the 'backend/CarRental/src/test/Postman' folder