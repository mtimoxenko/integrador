package com.CarRental.CarRental.service;




import com.CarRental.CarRental.dto.characteristic.CharacteristicDto;
import com.CarRental.CarRental.exception.ApiException;
import com.CarRental.CarRental.model.Characteristic;
import com.CarRental.CarRental.model.Product;
import com.CarRental.CarRental.repository.ICharacteristicRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class CharacteristicServiceTest {

    @Mock
    private ICharacteristicRepository characteristicRepository;

    @InjectMocks
    private CharacteristicService characteristicService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @DisplayName("Delete characteristic successfully")
    @Test
    void deleteCharacteristicSuccessful() throws ApiException {
        Characteristic characteristic = new Characteristic();
        characteristic.setProducts(Arrays.asList());

        when(characteristicRepository.findById(anyLong())).thenReturn(Optional.of(characteristic));

        assertDoesNotThrow(() -> characteristicService.deleteCharacteristic(1L));
    }

    @DisplayName("Delete characteristic fails due to characteristic in use")
    @Test
    void deleteCharacteristicFailedDueToCharacteristicInUse() throws ApiException {
        Characteristic characteristic = new Characteristic();
        characteristic.setProducts(Arrays.asList(new Product()));

        when(characteristicRepository.findById(anyLong())).thenReturn(Optional.of(characteristic));

        assertThrows(ApiException.class, () -> characteristicService.deleteCharacteristic(1L));
    }

    @DisplayName("Find characteristic by id fails due to not found")
    @Test
    void findCharacteristicByIdFailedDueToNotFound() throws ApiException {
        when(characteristicRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(ApiException.class, () -> characteristicService.findById(1L));
    }

    @DisplayName("Find characteristics by ids successfully")
    @Test
    void findCharacteristicsByIdsSuccessful() {
        when(characteristicRepository.findByIdIn(anyList())).thenReturn(Arrays.asList(new Characteristic(), new Characteristic()));

        assertDoesNotThrow(() -> characteristicService.findByInId(Arrays.asList(1L, 2L)));
    }
}