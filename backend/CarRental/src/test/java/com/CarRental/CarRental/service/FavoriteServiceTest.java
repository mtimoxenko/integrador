package com.CarRental.CarRental.service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import com.CarRental.CarRental.dto.product.FavoriteDto;
import com.CarRental.CarRental.exception.ApiException;
import com.CarRental.CarRental.model.Favorite;
import com.CarRental.CarRental.model.Product;
import com.CarRental.CarRental.model.User;
import com.CarRental.CarRental.repository.IFavoriteRepository;
import com.CarRental.CarRental.repository.IProductRepository;
import com.CarRental.CarRental.repository.IUserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

class FavoriteServiceTest {

    @Mock
    private IFavoriteRepository favoriteRepository;

    @Mock
    private IUserRepository userRepository;

    @Mock
    private IProductRepository productRepository;

    @InjectMocks
    private FavoriteService favoriteService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }




    @DisplayName("Create favorite successfully")
    @Test
    void createFavoriteSuccessful() throws ApiException {
        when(userRepository.findById(anyLong())).thenReturn(Optional.of(new User()));
        when(productRepository.findById(anyLong())).thenReturn(Optional.of(new Product()));

        assertDoesNotThrow(() -> favoriteService.createFavorite(1L, 1L));
    }

    @DisplayName("Create favorite fails due to not found user or product")
    @Test
    void createFavoriteFailedDueToNotFoundUserOrProduct() {
        when(userRepository.findById(anyLong())).thenReturn(Optional.empty());
        when(productRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(ApiException.class, () -> favoriteService.createFavorite(1L, 1L));
    }

    @DisplayName("Delete favorite successfully")
    @Test
    void deleteFavoriteSuccessful() throws ApiException {
        Favorite favorite = new Favorite();
        favorite.setId(1L);
        when(favoriteRepository.findByUserIdAndProductId(anyLong(), anyLong())).thenReturn(Optional.of(favorite));

        assertDoesNotThrow(() -> favoriteService.deleteFavorite(1L, 1L));
    }

}