package com.CarRental.CarRental.service;

import com.CarRental.CarRental.dto.category.CategoryRequest;
import com.CarRental.CarRental.exception.ApiException;
import com.CarRental.CarRental.model.Category;
import com.CarRental.CarRental.repository.ICategoryRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class CategoryServiceTest {

    @Mock
    private ICategoryRepository categoryRepository;

    @InjectMocks
    private CategoryService categoryService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }


    @DisplayName("Delete category successfully")
    @Test
    void deleteCategorySuccessful() throws ApiException {
        when(categoryRepository.findById(anyLong())).thenReturn(Optional.of(new Category()));

        assertDoesNotThrow(() -> categoryService.deleteCategory(1L));
    }

    @DisplayName("Delete category fails due to not found")
    @Test
    void deleteCategoryFailedDueToNotFound() throws ApiException {
        when(categoryRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(ApiException.class, () -> categoryService.deleteCategory(1L));
    }

    @DisplayName("Find category by id successfully")
    @Test
    void findCategoryByIdSuccessful() throws ApiException {
        when(categoryRepository.findById(anyLong())).thenReturn(Optional.of(new Category()));

        assertDoesNotThrow(() -> categoryService.findById(1L));
    }

    @DisplayName("Find category by id fails due to not found")
    @Test
    void findCategoryByIdFailedDueToNotFound() throws ApiException {
        when(categoryRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(ApiException.class, () -> categoryService.findById(1L));
    }
}