package com.CarRental.CarRental.service;

import static org.junit.jupiter.api.Assertions.*;
import com.CarRental.CarRental.dto.user.LoginUserRequest;
import com.CarRental.CarRental.dto.user.RegisterRequest;
import com.CarRental.CarRental.exception.ApiException;
import com.CarRental.CarRental.model.User;
import com.CarRental.CarRental.model.UserRol;
import com.CarRental.CarRental.repository.IUserRepository;
import com.CarRental.CarRental.security.JwtTokenUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Arrays;
import java.util.Optional;

import static org.mockito.Mockito.*;

class UserServiceTest {

    @Mock
    private IUserRepository userRepository;

    @Mock
    private JwtTokenUtil jwtTokenUtil;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private UserService userService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void loginSuccessful() throws ApiException {
        User user = new User();
        user.setPassword("password");
        when(userRepository.findByEmail(anyString())).thenReturn(Optional.of(user));
        when(passwordEncoder.matches(anyString(), anyString())).thenReturn(true);

        LoginUserRequest request = new LoginUserRequest("test@test.com","password");
        assertDoesNotThrow(() -> userService.login(request));
    }

    @Test
    void loginFailedDueToInvalidCredentials() throws ApiException {
        User user = new User();
        user.setPassword("password");
        when(userRepository.findByEmail(anyString())).thenReturn(Optional.of(user));
        when(passwordEncoder.matches(anyString(), anyString())).thenReturn(false);

        LoginUserRequest request = new LoginUserRequest("test@test.com","wrongpassword");
        assertThrows(ApiException.class, () -> userService.login(request));
    }

    @Test
    void logoutSuccessful() throws ApiException {
        doNothing().when(jwtTokenUtil).removeToken(anyString());

        assertDoesNotThrow(() -> userService.logout("jwtToken"));
    }

    @Test
    void getAllUsersSuccessful() {
        when(userRepository.findAll()).thenReturn(Arrays.asList(new User(), new User()));

        assertDoesNotThrow(() -> userService.getAllUsers());
    }

    @Test
    void changeRoleSuccessful() {
        when(userRepository.findByEmailIn(anyList())).thenReturn(Arrays.asList(new User(), new User()));
        doNothing().when(jwtTokenUtil).removeToken(anyString());

        assertDoesNotThrow(() -> userService.changeRole(Arrays.asList("test1@test.com", "test2@test.com"), UserRol.ADMINISTRADOR));
    }
}