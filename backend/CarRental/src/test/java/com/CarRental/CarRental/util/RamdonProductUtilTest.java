package com.CarRental.CarRental.util;

import com.CarRental.CarRental.dto.product.ProductDto;
import com.CarRental.CarRental.model.Image;
import com.CarRental.CarRental.model.Product;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RamdonProductUtilTest {
    List<Product> products = new ArrayList<>();

    @BeforeEach
    public void setUp() {
        /*products.add(new Product(1L, "Product 1",""));
        products.add(new Product(2L, "Product 2",""));
        products.add(new Product(3L, "Product 3",""));*/
    }

    private List<Image> crearImagenesParaProducto(Product product, int count) {
        List<Image> imagenes = new ArrayList<>();
        for (int i = 1; i <= count; i++) {
            Image image = new Image();
            //imagen.setEncodeImage("assets/Images/" + product.getName().toLowerCase().replace(" ", "-") + "-img" + i + ".jpg");
            image.setProduct(product);
            imagenes.add(image);
        }
        return imagenes;
    }


    @Test
    public void givenListOfProductsLessThanTenWhenCallGetRamdomProducThenReturnList(){

        List<ProductDto>  list =RamdonProductUtil.getRamdomProduct(products,"melissa");
        assertTrue(list.size()<10);
        assertEquals(products.size(), list.size());
    }

}
