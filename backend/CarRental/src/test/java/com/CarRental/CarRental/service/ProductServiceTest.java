package com.CarRental.CarRental.service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import com.CarRental.CarRental.dto.product.ProductDtoCreate;
import com.CarRental.CarRental.exception.ApiException;
import com.CarRental.CarRental.model.Category;
import com.CarRental.CarRental.model.Product;
import com.CarRental.CarRental.repository.IProductRepository;
import com.CarRental.CarRental.service.CategoryService;
import com.CarRental.CarRental.service.ProductService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class ProductServiceTest {

    @Mock
    private IProductRepository productRepository;

    @Mock
    private CategoryService categoryService;

    @InjectMocks
    private ProductService productService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @DisplayName("Delete product successfully")
    @Test
    void deleteProductSuccessful() throws ApiException {
        when(productRepository.findById(anyLong())).thenReturn(Optional.of(new Product()));

        assertDoesNotThrow(() -> productService.deleteProduct(1L));
    }

    @DisplayName("Delete product fails due to not found product")
    @Test
    void deleteProductFailedDueToNotFoundProduct() {
        when(productRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(ApiException.class, () -> productService.deleteProduct(1L));
    }

    @DisplayName("Update product fails due to not found product")
    @Test
    void updateProductFailedDueToNotFoundProduct() {
        when(productRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(ApiException.class, () -> productService.updateProduct(1L, 1L));
    }


    @DisplayName("Get product by id fails due to not found product")
    @Test
    void getProductByIdFailedDueToNotFoundProduct() {
        when(productRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(ApiException.class, () -> productService.getProductById(1L));
    }
}