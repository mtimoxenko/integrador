package com.CarRental.CarRental.service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import com.CarRental.CarRental.dto.reservation.ReservationDto;
import com.CarRental.CarRental.exception.ApiException;
import com.CarRental.CarRental.model.Product;
import com.CarRental.CarRental.model.Reservation;
import com.CarRental.CarRental.model.User;
import com.CarRental.CarRental.repository.IProductRepository;
import com.CarRental.CarRental.repository.IReservationRepository;
import com.CarRental.CarRental.repository.IUserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

class ReservationServiceTest {

    @Mock
    private IReservationRepository reservationRepository;

    @Mock
    private IProductRepository productRepository;

    @Mock
    private IUserRepository userRepository;

    @InjectMocks
    private ReservationService reservationService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }



    @DisplayName("Create reservation fails due to not found user or product")
    @Test
    void createReservationFailedDueToNotFoundUserOrProduct() {
        when(userRepository.findById(anyLong())).thenReturn(Optional.empty());
        when(productRepository.findById(anyLong())).thenReturn(Optional.empty());

        ReservationDto request = new ReservationDto(1L,1L,LocalDate.now().plusDays(2),LocalDate.now().plusDays(4));
        //request.setProduct_id(1L);
        //request.setUser_id(1L);
        //request.setStartDate(LocalDate.now().plusDays(2));
        //request.setEndDate(LocalDate.now().plusDays(4));

        assertThrows(ApiException.class, () -> reservationService.createReservation(request));
    }





    @DisplayName("Get reservations by user id fails due to not found user")
    @Test
    void getReservationsByUserIdFailedDueToNotFoundUser() {
        when(userRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(ApiException.class, () -> reservationService.getReservationsByUserId(1L));
    }

    @DisplayName("Delete reservation successfully")
    @Test
    void deleteReservationSuccessful() throws ApiException {
        when(reservationRepository.findById(anyLong())).thenReturn(Optional.of(new Reservation()));

        assertDoesNotThrow(() -> reservationService.deleteReservation(1L));
    }

    @DisplayName("Delete reservation fails due to not found reservation")
    @Test
    void deleteReservationFailedDueToNotFoundReservation() {
        when(reservationRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(ApiException.class, () -> reservationService.deleteReservation(1L));
    }
}