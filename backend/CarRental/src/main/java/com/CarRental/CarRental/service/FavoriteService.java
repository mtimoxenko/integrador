package com.CarRental.CarRental.service;

import com.CarRental.CarRental.dto.product.FavoriteDto;
import com.CarRental.CarRental.dto.product.response.Response;
import com.CarRental.CarRental.exception.ApiException;
import com.CarRental.CarRental.exception.dto.ApiErrorDto;
import com.CarRental.CarRental.mapper.FavoriteMapper;
import com.CarRental.CarRental.model.Favorite;
import com.CarRental.CarRental.model.Product;
import com.CarRental.CarRental.model.User;
import com.CarRental.CarRental.repository.IFavoriteRepository;
import com.CarRental.CarRental.repository.IProductRepository;
import com.CarRental.CarRental.repository.IUserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class FavoriteService implements com.CarRental.CarRental.service.interfaces.IFavoriteService {
    private final IFavoriteRepository favoriteRepository;
    private final IUserRepository userRepository;
    private final IProductRepository productRepository;

    public FavoriteService(IFavoriteRepository favoriteRepository, IUserRepository userRepository, IProductRepository productRepository) {
        this.favoriteRepository = favoriteRepository;
        this.userRepository = userRepository;
        this.productRepository = productRepository;
    }

    @Override
    public List<FavoriteDto> getFavoritesByUser(Long id) {
      return favoriteRepository.findByUserId(id).stream()
                .map(FavoriteMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public Response createFavorite(Long user_id, Long productoId) throws ApiException {
        Optional<User> userOptional = userRepository.findById(user_id);
        Optional<Product> productOptional = productRepository.findById(productoId);
        if (!userOptional.isPresent() || !productOptional.isPresent()) {
            throw new ApiErrorDto("not_found","Usuario o producto no encontrado",400).toException();
        }
        Favorite favorite = new Favorite();
        favorite.setUser(userOptional.get());
        favorite.setProduct(productOptional.get());
        this.favoriteRepository.save(favorite);

        return Response.builder()
                .success(true)
                .message("El producto fue Agregado a la lista de favoritos")
                .build();
    }

    @Override
    public Response deleteFavorite(Long userId, Long productId) {
        Optional<Favorite> favorite = favoriteRepository.findByUserIdAndProductId(userId, productId);

        favoriteRepository.deleteById(favorite.get().getId());
        return Response.builder()
                .success(true)
                .message("El producto fue eiliminado de la lista de favoritos")
                .build();
    }
}
