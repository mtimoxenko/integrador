package com.CarRental.CarRental.model;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "reservations")
public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Schema(description = "Identificador único", example = "1L")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_id", referencedColumnName = "id")
    @Schema(description = "ID del producto asociado a la reserva", example = "1L")
    private Product product;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @Schema(description = "ID del usuario asociado a la reserva", example = "1L")
    private User user;

    @Schema(description = "Fecha de inicio de la reserva", example = "2024-03-10")
    private LocalDate startDate;

    @Schema(description = "Fecha de inicio de la reserva", example = "2024-03-12")
    private LocalDate endDate;

}
