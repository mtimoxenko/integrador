package com.CarRental.CarRental.controller;


import com.CarRental.CarRental.dto.product.ProductDto;
import com.CarRental.CarRental.dto.product.ProductDtoCreate;
import com.CarRental.CarRental.dto.product.ProductDtoUpdate;
import com.CarRental.CarRental.dto.product.request.ProductRequest;
import com.CarRental.CarRental.dto.product.response.ProductBrandsResponse;
import com.CarRental.CarRental.dto.product.response.ProductCategoryResponse;
import com.CarRental.CarRental.dto.product.response.ProductResponse;
import com.CarRental.CarRental.exception.ApiException;
import com.CarRental.CarRental.exception.dto.ApiErrorDto;
import com.CarRental.CarRental.service.FileStorageService;
import com.CarRental.CarRental.service.S3FileStorage;
import com.CarRental.CarRental.service.interfaces.IProductService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import com.CarRental.CarRental.dto.image.ImageRequest;
import com.CarRental.CarRental.dto.product.response.ProductResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.CarRental.CarRental.util.GeneralConstansSwagger.*;

@Tag(name = "Productos", description = "Endpoints para la gestión de Productos")
@RequestMapping(value = "/products")
@RestController
public class ProductController {
    private final IProductService productService;
    private final S3FileStorage s3FileStorage;

    public ProductController(final IProductService productService, S3FileStorage s3FileStorage) {
        this.productService = productService;
        this.s3FileStorage = s3FileStorage;
    }
    @Operation(summary = "Crear producto", description = "Método encargado de registrar un producto")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "success", content = @Content(examples = {
                    @ExampleObject(name = "creacion exitosa de producto", value = "" +
                            "{\n" +
                            "    \"success\": true,\n" +
                            "    \"message\": \"Producto creado con exito\",\n" +
                            "    \"currentProduct\": {\n" +
                            "        \"id\": 1,\n" +
                            "        \"name\": \"Vehículo 1\",\n" +
                            "        \"category\": {\n" +
                            "            \"id\": 1,\n" +
                            "            \"name\": \"Categoria1\",\n" +
                            "            \"description\": \"Descripción categoría 1\",\n" +
                            "            \"imageUrl\": \"https://Imagen1.jpg\",\n" +
                            "            \"products\": []\n" +
                            "        },\n" +
                            "        \"brand\": \"Marca vehículo 1\",\n" +
                            "        \"model\": \"Modelo vehículo 1\",\n" +
                            "        \"description\": \"Descripción vehículo 1\",\n" +
                            "        \"fuel\": \"Combustible vehiculo 1\",\n" +
                            "        \"numPassengers\": 2,\n" +
                            "        \"numBags\": 5,\n" +
                            "        \"numDoors\": 4,\n" +
                            "        \"isAutomatic\": false,\n" +
                            "        \"images\": [],\n" +
                            "    }\n" +
                            "}" )
            })),
            @ApiResponse(responseCode = "400", description = "Bad request", content = @Content(schema = @Schema(implementation = ApiErrorDto.class), examples = {
                    @ExampleObject(name = "already exist product", value = RESPONSE_ERROR_400_ALREADY_EXIST_PRODUCT)
            }))
    })
    @PreAuthorize("hasRole('ADMINISTRADOR') || hasRole('SUPER_ADMINISTRADOR')")
    @PostMapping
    public ProductResponse createProduct(
            @RequestParam String name,
            @RequestParam Long category_id,
            @RequestParam String brand,
            @RequestParam String model,
            @RequestParam String description,
            @RequestParam String fuel,
            @RequestParam int numPassengers,
            @RequestParam int numBags,
            @RequestParam int numDoors,
            @RequestParam boolean isAutomatic,
            @RequestParam MultipartFile[] images,
            @RequestParam List<Long> characteristicsId

    ) throws ApiException {

        List<String> imagesUrls = new ArrayList<>();
        for (MultipartFile file : images) {
            String fileName = s3FileStorage.storeFile(file); // Store files in S3
            String URL = s3FileStorage.getSignedUrl(fileName);
            imagesUrls.add(URL);

        }

        ProductDtoCreate request = new ProductDtoCreate(null, name,category_id,brand,model,description,fuel,
                numPassengers, numBags, numDoors, isAutomatic, imagesUrls, characteristicsId);

        return productService.createProduct(request);
    }

    @Operation(summary = "Borrar producto", description = "Método encargado de borrar un producto")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "success", content = @Content(examples = {
                    @ExampleObject(name = "creacion exitosa de producto", value = "{\n" +
                            "  \"success\": true,\n" +
                            "  \"message\": \"Producto Eliminado con éxito\",\n" +
                            "  }")
            })),
            @ApiResponse(responseCode = "404", description = "Not found", content = @Content(schema = @Schema(implementation = ApiErrorDto.class), examples = {
                    @ExampleObject(name = "Delete product not found", value = RESPONSE_ERROR_404_DELETE_PRODUCT_ERROR_NOT_FOUND)
            }))
    })
    @PreAuthorize("hasRole('ADMINISTRADOR') || hasRole('SUPER_ADMINISTRADOR')")
    @DeleteMapping(value="/{id}")
    public ProductResponse deleteProduct(@PathVariable long id) throws ApiException {
        ProductResponse productResponse = productService.getProductById(id);

        if (productResponse != null && productResponse.getCurrentProduct() != null && !productResponse.getCurrentProduct().getImages().isEmpty()) {
            List<String> imageUrls = productResponse.getCurrentProduct().getImages().stream()
                    .map(ImageRequest::getImageUrl) // Extract image URLs
                    .toList();

            for (String imageUrl : imageUrls) {
                try {
                    // Extract the file name from the imageUrl if necessary
                    // Then, delete the image file from S3
                    s3FileStorage.deleteFile(imageUrl);
                } catch (Exception e) {
//                    LOGGER.error("Failed to delete image from S3 for product ID: " + id + ", Image URL: " + imageUrl, e);
                    // Depending on your application's needs, handle this error accordingly
                }
            }
        }
        return productService.deleteProduct(id);
    }


        @Operation(summary = "Actualizar producto", description = "Método encargado de actualizar la categoría de un producto")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "success", content = @Content(examples = {
                    @ExampleObject(name = "creacion exitosa de producto", value = "" +
                            "{\n" +
                            "    \"success\": true,\n" +
                            "    \"message\": \"Product actualizado con exito\",\n" +
                            "    \"currentProduct\": {\n" +
                            "        \"id\": 1,\n" +
                            "        \"name\": \"Vehículo 1\",\n" +
                            "        \"category\": {\n" +
                            "            \"id\": 5,\n" +
                            "            \"name\": \"Categoria 5\",\n" +
                            "            \"description\": \"Descripción categoría 5\",\n" +
                            "            \"imageUrl\": \"https://ImagenCat5.jpg\",\n" +
                            "            \"products\": []\n" +
                            "        },\n" +
                            "        \"brand\": \"Vehículo 1\",\n" +
                            "        \"model\": \"Modelo Vehículo 1\",\n" +
                            "        \"description\": \"Descripción Vehículo 1\",\n" +
                            "        \"fuel\": \"Combustible Vehículo 1\",\n" +
                            "        \"numPassengers\": 2,\n" +
                            "        \"numBags\": 2,\n" +
                            "        \"numDoors\": 0,\n" +
                            "        \"isAutomatic\": false,\n" +
                            "        \"images\": [\n" +
                            "            {\n" +
                            "                \"id\": 1,\n" +
                            "                \"imageUrl\": \"https://Imagen1.jpg\"\n" +
                            "            },\n" +
                            "            {\n" +
                            "                \"id\": 2,\n" +
                            "                \"imageUrl\": \"https://Imagen2.jpg\"\n" +
                            "            },\n" +
                            "            {\n" +
                            "                \"id\": 3,\n" +
                            "                \"imageUrl\": \"https://Imagen3.jpg\"\n" +
                            "            },\n" +
                            "            {\n" +
                            "                \"id\": 4,\n" +
                            "                \"imageUrl\": \"https://Imagen4.jpg\"\n" +
                            "            },\n" +
                            "            {\n" +
                            "                \"id\": 5,\n" +
                            "                \"imageUrl\": \"https://Imagen5.jpg\"\n" +
                            "            }\n" +
                            "        ],\n" +
                            "    }\n" +
                            "}")
            })),
            @ApiResponse(responseCode = "404", description = "Not found", content = @Content(schema = @Schema(implementation = ApiErrorDto.class), examples = {
                    @ExampleObject(name = "Update product not found", value = RESPONSE_ERROR_404_UPDATE_PRODUCT_ERROR_NOT_FOUND)
            }))
    })
    @PreAuthorize("hasRole('ADMINISTRADOR') || hasRole('SUPER_ADMINISTRADOR')")
    @PutMapping(value="/update")
    public ProductResponse updateProduct(@RequestBody ProductDtoUpdate request) throws ApiException {
        return productService.updateProduct(request.getId(), request.getCategory_id());
    }

    @Operation(summary = "Buscador", description = "Retorna una lista de productos basada en el criterio de búsqueda ingresado")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "success", content = @Content(examples = {
                    @ExampleObject(name = "Lista de productos filtrados ", value = "" +
                            "{\n" +
                            "    \"success\": true,\n" +
                            "    \"products\": [\n" +
                            "        {\n" +
                            "            \"id\": 4,\n" +
                            "            \"name\": \"Vehículo 4\",\n" +
                            "            \"category\": {\n" +
                            "                \"id\": 4,\n" +
                            "                \"name\": \"Categoría 4\",\n" +
                            "                \"description\": \"Descripción categoría 4\",\n" +
                            "                \"imageUrl\": \"https://Categoría4.jpg\",\n" +
                            "                \"products\": []\n" +
                            "            },\n" +
                            "            \"brand\": \"Marca Vehículo 4\",\n" +
                            "            \"model\": \"Modelo Vehículo 4\",\n" +
                            "            \"description\": \"Descripción Vehículo 4\",\n" +
                            "            \"fuel\": \"Combustible Vehículo 4\",\n" +
                            "            \"numPassengers\": 2,\n" +
                            "            \"numBags\": 2,\n" +
                            "            \"numDoors\": 0,\n" +
                            "            \"isAutomatic\": false,\n" +
                            "            \"images\": [\n" +
                            "                {\n" +
                            "                    \"id\": 16,\n" +
                            "                    \"imageUrl\": \"https://Imagen1.jpeg\"\n" +
                            "                },\n" +
                            "                {\n" +
                            "                    \"id\": 17,\n" +
                            "                    \"imageUrl\": \"https://Imagen2.jpeg\"\n" +
                            "                },\n" +
                            "                {\n" +
                            "                    \"id\": 18,\n" +
                            "                    \"imageUrl\": \"https://Imagen3.jpeg\"\n" +
                            "                },\n" +
                            "                {\n" +
                            "                    \"id\": 19,\n" +
                            "                    \"imageUrl\": \"https://Imagen4.jpeg\"\n" +
                            "                },\n" +
                            "                {\n" +
                            "                    \"id\": 20,\n" +
                            "                    \"imageUrl\": \"https://Imagen5.jpeg\"\n" +
                            "                }\n" +
                            "            ],\n" +
                            "            \"characteristics\": []\n" +
                            "        },\n" +
                            "        {...}\n" +
                            "    ],\n" +
                            "    \"pageable\": {\n" +
                            "        \"pageNumber\": 0,\n" +
                            "        \"pageSize\": 25,\n" +
                            "        \"totalElements\": 8,\n" +
                            "        \"totalPage\": 1,\n" +
                            "        \"sortBy\": \"name\",\n" +
                            "        \"direction\": \"ASC\"\n" +
                            "    }\n" +
                            "}")
            }))
    })
    @PostMapping(value="/public/get")
    public ProductResponse getProducts(final @RequestBody @io.swagger.v3.oas.annotations.parameters.RequestBody(
            description = "Ejemplo del cuerpo de la solicitud, puede ser cambiado el criterio de búsqueda" ,
            content = @Content(
                    mediaType = "application/json",
                    examples = {
                            @ExampleObject(
                                    name = "Búsqueda por nombre",
                                    value = "{\n" +
                                            "  \"criteria\": {\n" +
                                            "    \"name\": \"Ferrari\",\n" +
                                            "    \"pageableDto\": {\n" +
                                            "      \"pageNumber\": 0,\n" +
                                            "      \"pageSize\": 25,\n" +
                                            "      \"sortBy\": \"name\",\n" +
                                            "      \"direction\": \"ASC\"\n" +
                                            "    }\n" +
                                            "  }\n" +
                                            "}"
                            ),
                            @ExampleObject(
                                    name = "Búsqueda por categoría y marca",
                                    value = "{\n" +
                                            "  \"criteria\": {\n" +
                                            "    \"category\": \"Todo Terreno (4x4)\",\n" +
                                            "    \"brand\": \"Jaguar\",\n" +
                                            "    \"pageableDto\": {\n" +
                                            "      \"pageNumber\": 0,\n" +
                                            "      \"pageSize\": 25,\n" +
                                            "      \"sortBy\": \"name\",\n" +
                                            "      \"direction\": \"ASC\"\n" +
                                            "    }\n" +
                                            "  }\n" +
                                            "}"
                            )
                    }
            )
    )ProductRequest request){
        return productService.getProducts(request);
    }

    @Operation(summary = "Obtener productos aleatorios", description = "Retorna máximo 10 productos aleatorios")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "success", content = @Content(examples = {
                    @ExampleObject(name = "Lista aleatoria de productos", value = "" +
                            "{\n" +
                            "    \"success\": true,\n" +
                            "    \"id\": \"e1b1d218-aecf-4661-afe8-20e6a1a5a96d\",\n" +
                            "    \"products\": [\n" +
                            "        {\n" +
                            "            \"id\": 6,\n" +
                            "            \"name\": \"Ferrari6\",\n" +
                            "            \"category\": {\n" +
                            "                \"id\": 3,\n" +
                            "                \"name\": \"Categoría 3\",\n" +
                            "                \"description\": \"Descripción categoría 3.\",\n" +
                            "                \"imageUrl\": \"https://ImagenCat3.jpg\",\n" +
                            "                \"products\": null\n" +
                            "            },\n" +
                            "            \"brand\": \"Ferrari\",\n" +
                            "            \"model\": \"f40\",\n" +
                            "            \"description\": \"Auto italiano deportivo\",\n" +
                            "            \"fuel\": \"Gasolina\",\n" +
                            "            \"numPassengers\": 2,\n" +
                            "            \"numBags\": 2,\n" +
                            "            \"numDoors\": 0,\n" +
                            "            \"isAutomatic\": false,\n" +
                            "            \"images\": [\n" +
                            "                {\n" +
                            "                    \"id\": 26,\n" +
                            "                    \"imageUrl\": \"https://Imagen1.jpg\"\n" +
                            "                },\n" +
                            "                {\n" +
                            "                    \"id\": 27,\n" +
                            "                    \"imageUrl\": \"https://Imagen2.jpg\"\n" +
                            "                },\n" +
                            "                {\n" +
                            "                    \"id\": 28,\n" +
                            "                    \"imageUrl\": \"https://Imagen3.jpg\"\n" +
                            "                },\n" +
                            "                {\n" +
                            "                    \"id\": 29,\n" +
                            "                    \"imageUrl\": \"https://Imagen4.jpg\"\n" +
                            "                },\n" +
                            "                {\n" +
                            "                    \"id\": 30,\n" +
                            "                    \"imageUrl\": \"https://Imagen5.jpg\"\n" +
                            "                }\n" +
                            "            ],\n" +
                            "        },\n" +
                            "        {...}\n" +
                            "    ]\n" +
                            "}")
            }))
    })
    @GetMapping("/public")
    public ProductResponse getRandomProducts(@RequestHeader(value = "ID", required = false) String id){
        return productService.getRandomProducts(id);
    }

    @Operation(summary = "Obtener producto por ID", description = "Retorna el producto encontrado")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "success", content = @Content(examples = {
                    @ExampleObject(name = "Producto con ID ingresado", value =
                            "{\n" +
                                    "    \"success\": true,\n" +
                                    "    \"id\": \"1\",\n" +
                                    "    \"currentProduct\": {\n" +
                                    "        \"id\": 1,\n" +
                                    "        \"name\": \"Ferrari\",\n" +
                                    "        \"category\": {\n" +
                                    "            \"id\": 3,\n" +
                                    "            \"name\": \"Sedán\",\n" +
                                    "            \"description\": \"Automóvil de tamaño mediano a grande, con un diseño elegante y cómodo, ideal para viajes en carretera y uso diario.\",\n" +
                                    "            \"imageUrl\": \"https://ImageCategory.jpg\",\n" +
                                    "            \"products\": []\n" +
                                    "        },\n" +
                                    "        \"brand\": \"Ferrari\",\n" +
                                    "        \"model\": \"f40\",\n" +
                                    "        \"description\": \"Auto italiano deportivo\",\n" +
                                    "        \"fuel\": \"Gasolina\",\n" +
                                    "        \"numPassengers\": 2,\n" +
                                    "        \"numBags\": 2,\n" +
                                    "        \"numDoors\": 0,\n" +
                                    "        \"isAutomatic\": false,\n" +
                                    "        \"images\": [\n" +
                                    "            {\n" +
                                    "                \"id\": 1,\n" +
                                    "                \"imageUrl\": \"https://Imagen1.jpg\"\n" +
                                    "            },\n" +
                                    "            {\n" +
                                    "                \"id\": 2,\n" +
                                    "                \"imageUrl\": \"https://Imagen2.jpg\"\n" +
                                    "            },\n" +
                                    "            {\n" +
                                    "                \"id\": 3,\n" +
                                    "                \"imageUrl\": \"https://Imagen3.jpg\"\n" +
                                    "            },\n" +
                                    "            {\n" +
                                    "                \"id\": 4,\n" +
                                    "                \"imageUrl\": \"https://Imagen4.jpg\"\n" +
                                    "            },\n" +
                                    "            {\n" +
                                    "                \"id\": 5,\n" +
                                    "                \"imageUrl\": \"https://Imagen5.jpg\"\n" +
                                    "            }\n" +
                                    "        ],\n" +
                                    "    }\n" +
                                    "}")
            })),
            @ApiResponse(responseCode = "404", description = "Not found", content = @Content(schema = @Schema(implementation = ApiErrorDto.class), examples = {
                    @ExampleObject(name = "Error not found", value = RESPONSE_ERROR_404_PRODUCT_ERROR_NOT_FOUND)
            }))
    })
    @GetMapping (value="/public/{id}")
    public ProductResponse getById(final @PathVariable  long id) throws ApiException {
        return productService.getProductById(id);
    }


    @Operation(summary = "Obtener productos por ID de categoría", description = "Retorna la lista de productos asociados a la categoría indicada")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "success", content = @Content(examples = {
                    @ExampleObject(name = "Productos asociados a la categoría de interés", value =
                            "{\n" +
                                    "    \"success\": true,\n" +
                                    "    \"products\": [\n" +
                                    "        {\n" +
                                    "            \"id\": 2,\n" +
                                    "            \"name\": \"Vehículo 2\",\n" +
                                    "            \"category_id\": 1,\n" +
                                    "            \"category_name\": \"Categoría 1\",\n" +
                                    "            \"brand\": \"Marca Vehículo 2\",\n" +
                                    "            \"model\": \"Modelo Vehículo 2\",\n" +
                                    "            \"description\": \"Descripción Vehículo 2\",\n" +
                                    "            \"fuel\": \"Combustible Vehículo 2\",\n" +
                                    "            \"numPassengers\": 4,\n" +
                                    "            \"numBags\": 4,\n" +
                                    "            \"numDoors\": 1,\n" +
                                    "            \"isAutomatic\": false,\n" +
                                    "            \"images\": [\n" +
                                    "                {\n" +
                                    "                    \"id\": 6,\n" +
                                    "                    \"imageUrl\": \"https://Imagen1.jpg\"\n" +
                                    "                },\n" +
                                    "                {\n" +
                                    "                    \"id\": 7,\n" +
                                    "                    \"imageUrl\": \"https://Imagen2.jpg\"\n" +
                                    "                },\n" +
                                    "                {\n" +
                                    "                    \"id\": 8,\n" +
                                    "                    \"imageUrl\": \"https://Imagen3.jpg\"\n" +
                                    "                },\n" +
                                    "                {\n" +
                                    "                    \"id\": 9,\n" +
                                    "                    \"imageUrl\": \"https://Imagen4.jpg\"\n" +
                                    "                },\n" +
                                    "                {\n" +
                                    "                    \"id\": 10,\n" +
                                    "                    \"imageUrl\": \"https://Imagen5.jpg\"\n" +
                                    "                }\n" +
                                    "            ],\n" +
                                    "        },\n" +
                                    "        {...}\n" +
                                    "    ]\n" +
                                    "}"
                    )
            })),
            @ApiResponse(responseCode = "404", description = "Bad request", content = @Content(schema = @Schema(implementation = ApiErrorDto.class), examples = {
                    @ExampleObject(name = "Products for category not found", value = RESPONSE_ERROR_400_PRODUCT_FOR_CATEGORY_NOT_FOUND)
            }))
    })
    @GetMapping (value="/public/category/{id}")
    public ProductCategoryResponse getByCategory(@PathVariable  long id) throws ApiException {
        return productService.getProductsByCategory(id);
    }

    @Operation(summary = "Obtener productos por los IDs de varias categorías", description = "Retorna la lista de productos asociados a las categorías indicadas")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "success", content = @Content(examples = {
                    @ExampleObject(name = "Productos asociados a las categorías de interés", value =
                            "{\n" +
                                    "    \"success\": true,\n" +
                                    "    \"products\": [\n" +
                                    "        {\n" +
                                    "            \"id\": 2,\n" +
                                    "            \"name\": \"Vehículo 2\",\n" +
                                    "            \"category_id\": 1,\n" +
                                    "            \"category_name\": \"Categoría 1\",\n" +
                                    "            \"brand\": \"Marca Vehículo 2\",\n" +
                                    "            \"model\": \"Modelo Vehículo 2\",\n" +
                                    "            \"description\": \"Descripción Vehículo 2\",\n" +
                                    "            \"fuel\": \"Combustible Vehículo 2\",\n" +
                                    "            \"numPassengers\": 4,\n" +
                                    "            \"numBags\": 4,\n" +
                                    "            \"numDoors\": 1,\n" +
                                    "            \"isAutomatic\": false,\n" +
                                    "            \"images\": [\n" +
                                    "                {\n" +
                                    "                    \"id\": 6,\n" +
                                    "                    \"imageUrl\": \"https://Imagen1.jpg\"\n" +
                                    "                },\n" +
                                    "                {\n" +
                                    "                    \"id\": 7,\n" +
                                    "                    \"imageUrl\": \"https://Imagen2.jpg\"\n" +
                                    "                },\n" +
                                    "                {\n" +
                                    "                    \"id\": 8,\n" +
                                    "                    \"imageUrl\": \"https://Imagen3.jpg\"\n" +
                                    "                },\n" +
                                    "                {\n" +
                                    "                    \"id\": 9,\n" +
                                    "                    \"imageUrl\": \"https://Imagen4.jpg\"\n" +
                                    "                },\n" +
                                    "                {\n" +
                                    "                    \"id\": 10,\n" +
                                    "                    \"imageUrl\": \"https://Imagen5.jpg\"\n" +
                                    "                }\n" +
                                    "            ],\n" +
                                    "        },\n" +
                                    "        {...}\n" +
                                    "    ]\n" +
                                    "}"
                    )
            }))
    })
    @GetMapping (value="/public/categories")
    public ProductCategoryResponse getByCategories(@RequestParam List<Long> ids) throws ApiException {
        return productService.getProductsByCategories(ids);
    }

    @Operation(summary = "Obtener marcas asociadas a los productos registrados", description = "Retorna la lista de las marcas que contienen los vehículos registrados")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "success", content = @Content(examples = {
                    @ExampleObject(name = "Marcas: ", value =
                            "{\n" +
                                    "    \"success\": true,\n" +
                                    "    \"brands\": [\n" +
                                    "        \"Acura\",\n" +
                                    "        \"Ferrari\",\n" +
                                    "        \"Ford\",\n" +
                                    "        \"Jaguar\",\n" +
                                    "        \"Nissan\",\n" +
                                    "        \"X\"\n" +
                                    "    ]\n" +
                                    "}"
                    )
            }))
    })
    @GetMapping("/public/brand")
    public ProductBrandsResponse getBrands()throws ApiException{
        return productService.getProductBrands();
    }

}
