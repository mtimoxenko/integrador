package com.CarRental.CarRental.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "products") // Nombre en la BBDD
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Schema(description = "Identificador único", example = "1L")
    private Long id;

    @Schema(description = "Nombre del producto", example = "Carro")
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "category_id", referencedColumnName = "id")
    @Schema(description = "Categoria del producto.", example = "Compacto")
    private Category category;

    @Schema(description = "Marca del producto.", example = "Tesla")
    private String brand;

    @Schema(description = "Modelo del producto.", example = "Model S")
    private String model;

    @Schema(description = "Descripción asociada al producto.", example = "El Model S está diseñado para ofrecer velocidad y autonomía, además de contar con una aceleración espectacular, un rendimiento incomparable")
    private String description;

    @Schema(description = "Tipo de combustible.", example = "Gasolina")
    private String fuel;

    @Schema(description = "Número de pasajeros.", example = "5")
    @Column(name = "NUMBER_PASSENGERS")
    private int numPassengers;

    @Schema(description = "Número de maletas.", example = "2")
    @Column(name = "NUMBER_BAGS")
    private int numBags;

    @Schema(description = "Número de puertas.", example = "4")
    @Column(name = "NUMBER_DOORS")
    private int numDoors;

    @Schema(description = "Es automatico?", example = "Si.")
    private Boolean isAutomatic;

    @OneToMany (mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnore
    @Schema(description = "Lista de imagenes relacionadas al producto.")
    private List<Image> images = new ArrayList<>();

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "producto_caracteristica",
            joinColumns = @JoinColumn(name = "producto_id"),
            inverseJoinColumns = @JoinColumn(name = "caracteristica_id")
    )
    private List<Characteristic> caracteristicas = new ArrayList<>();

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnore
    private List<Reservation> reservations = new ArrayList<>();

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnore
    private List<Favorite> favorites = new ArrayList<>();

    public Product(Long id, String name, Category category, String brand, String model, String description, String fuel, int numPassengers, int numBags, int numDoors, Boolean isAutomatic, List<Image> images, List<Characteristic> caracteristicas, List<Reservation> reservations) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.brand = brand;
        this.model = model;
        this.description = description;
        this.fuel = fuel;
        this.numPassengers = numPassengers;
        this.numBags = numBags;
        this.numDoors = numDoors;
        this.isAutomatic = isAutomatic;
        this.images = images;
        this.caracteristicas = caracteristicas;
        this.reservations = reservations;
    }
}
