package com.CarRental.CarRental.service;

import com.CarRental.CarRental.dto.EmailEventDto;
import com.CarRental.CarRental.dto.product.response.Response;
import com.CarRental.CarRental.dto.user.LoginUserRequest;
import com.CarRental.CarRental.dto.user.LoginUserResponse;
import com.CarRental.CarRental.dto.user.RegisterRequest;
import com.CarRental.CarRental.dto.user.RegisterResponse;
import com.CarRental.CarRental.exception.ApiException;
import com.CarRental.CarRental.mapper.UserMapper;
import com.CarRental.CarRental.model.User;
import com.CarRental.CarRental.model.UserRol;
import com.CarRental.CarRental.repository.IUserRepository;
import com.CarRental.CarRental.security.JwtTokenUtil;
import com.CarRental.CarRental.service.interfaces.IUserService;
import com.CarRental.CarRental.util.ValidatorUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.CarRental.CarRental.exception.factory.UserFactoryException.*;
import static com.CarRental.CarRental.util.ValidatorUtil.validateEmail;

@Slf4j
@Service
@RequiredArgsConstructor

public class UserService implements IUserService {

    @Value("${url.login}")
    private String urlLogin;
    private static final Logger LOGGER = Logger.getLogger(UserService.class);
    private final IUserRepository userRepository;
    private final ApplicationEventPublisher applicationEventPublisher;
    private final JwtTokenUtil jwtTokenUtil;
    private final PasswordEncoder bCryptPasswordEncoder;

    @Override
    public LoginUserResponse login(LoginUserRequest loginUserRequest) throws ApiException {
        User user = userRepository.findByEmail(loginUserRequest.email())
                .orElseThrow(CREDENTIAL_INVALID::toException);

        if(!bCryptPasswordEncoder.matches(loginUserRequest.password(), user.getPassword())){
            throw CREDENTIAL_INVALID.toException();
        }

        LOGGER.info("User [" + user.getName() + " " + user.getSurname() + "] Ha iniciado sesión correctamente.");

        return  LoginUserResponse.builder()
                .token(jwtTokenUtil.generateToken(user))
                .message("Login exitoso")
                .rol(user.getRole())
                .userName(user.getName())
                .lastName(user.getSurname())
                .email(user.getEmail())
                .id(user.getId())
                .success(true)
                .build();
    }

    @Override
    public RegisterResponse registerUser(final RegisterRequest request) throws ApiException {
        ValidatorUtil.validateRegister(request);
        final Optional<User> user = userRepository.findByEmail(request.getEmail());
        if(user.isPresent()){
            throw userAlreadyRegister(request.getEmail()).toException();
        }

        request.setPassword(this.bCryptPasswordEncoder.encode(request.getPassword()));
        User userRegisted = userRepository.save(UserMapper.toEntity(request));

        this.sendEmail(request.getEmail(), request.getName());
        return RegisterResponse.builder().success(true)
                .message("Su registro fue exitoso")
                .warning("A su email, llegara un correo electronico de confirmación, si no llega puede reintentar o envio un correo que no existe")
                .token(jwtTokenUtil.generateToken(userRegisted))
                .name(userRegisted.getName())
                .lastName(userRegisted.getSurname())
                .email(userRegisted.getEmail())
                .userRol(userRegisted.getRole())
                .id(userRegisted.getId())
                .build();
    }

    @Override
    public RegisterResponse resendEmail(final RegisterRequest request) throws ApiException {
        validateEmail(request);
        User user = userRepository.findByEmail(request.getEmail())
                .orElseThrow(()->notFoundEmail(request.getEmail()).toException());
        this.sendEmail(request.getEmail(), user.getSurname());
        return RegisterResponse.builder().success(true)
                .message("Se reenvio exitosamente el email")
                .warning("A su email, llegara un correo electronico de confirmación, si no llega puede reintentar o envio un correo que no existe")
                .build();
    }

    @Override
    public Response logout(String jwt) throws ApiException {
        jwtTokenUtil.removeToken(jwt.substring(7));
        return Response.builder().success(true).message("Logout exitoso").build();
    }

    @Override
    public List<RegisterRequest> getAllUsers() {
        return this.userRepository.findAll().stream()
                .filter(x->x.getRole()!= UserRol.SUPER_ADMINISTRADOR)
                .map(UserMapper::toDto).toList();
    }

    private void sendEmail(final String email, final String name){
        String body = "<html>" +
                "<head>" +
                "<title>Confirmacion registro exitoso</title>" +
                "</head>" +
                "<body>" +
                "<h1>Saludos! "+name+"</h1>" +
                "<p>Este es un mensaje de confirmacion que tu registro en Car Rental fue exitoso</p>" +
                "<p>Para poderte loguear en nuestra pagina web debes usar el siguiente email "+email+" con el password que decidiste al registrarte</p>" +
                "<p>El siguiente enlace te redirecciona al login:</p>" +
                "<p><a href=\"" + urlLogin + "\">Visitar el enlace</a></p>" +

                "</body>" +
                "</html>";

        EmailEventDto emailEventDto = new EmailEventDto(this, email, "Registro exitoso",body);
        applicationEventPublisher.publishEvent(emailEventDto);
    }

    public Response changeRole(List<String> emails, UserRol role) {
        final List<User> users = this.userRepository.findByEmailIn(emails)
                .stream()
                .map(user -> {
                    user.setRole(role);
                    return user;
                })
                .toList();
        this.userRepository.saveAll(users);
        emails.forEach(email->{
            Optional<String> token= this.jwtTokenUtil.getTokenByEmail(email);
            token.ifPresent(this.jwtTokenUtil::removeToken);
        });
        return Response.builder().message("El cambio de rol fue exitoso").success(true).build();
    }
}
