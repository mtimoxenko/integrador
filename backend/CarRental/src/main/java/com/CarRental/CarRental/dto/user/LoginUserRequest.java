package com.CarRental.CarRental.dto.user;


public record LoginUserRequest(
        String email,
        String password
) { }
