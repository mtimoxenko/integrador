package com.CarRental.CarRental.controller;

import com.CarRental.CarRental.service.OpenAIService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import reactor.core.publisher.Mono;

@RestController
@Tag(name = "Recomendaciones IA", description = "Endpoints para la gestión de recomendaciones con IA")
@RequestMapping("/openai/recommendation")
public class RecommendationController {

    private final OpenAIService openAIService;

    public RecommendationController(OpenAIService openAIService) {
        this.openAIService = openAIService;
    }

    @Operation(summary = "Obtener recomendación", description = "Método para optener recomendación por IA")
    @PostMapping
    public Mono<ResponseEntity<String>> getRecommendation(@RequestBody UserInput userInput) {
        // Ensure to match the method name with your service method.
        return openAIService.generateText(userInput.getText())
                .map(recommendation -> ResponseEntity.ok().body(recommendation))
                .onErrorResume(e -> Mono.just(ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error fetching recommendation")));
    }

    // Changed to public visibility
    @Setter
    @Getter
    public static class UserInput {
        private String text;
    }
}
