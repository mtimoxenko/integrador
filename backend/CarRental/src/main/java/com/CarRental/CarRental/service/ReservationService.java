package com.CarRental.CarRental.service;

import com.CarRental.CarRental.dto.EmailEventDto;
import com.CarRental.CarRental.dto.reservation.ReservationDto;
import com.CarRental.CarRental.dto.reservation.ReservationResponse;
import com.CarRental.CarRental.exception.ApiException;
import com.CarRental.CarRental.model.Product;
import com.CarRental.CarRental.model.Reservation;
import com.CarRental.CarRental.model.User;
import com.CarRental.CarRental.repository.IProductRepository;
import com.CarRental.CarRental.repository.IReservationRepository;
import com.CarRental.CarRental.repository.IUserRepository;
import com.CarRental.CarRental.service.interfaces.IReservationService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.CarRental.CarRental.exception.factory.ReservationFactoryException.*;

@Service
public class ReservationService implements IReservationService {

    private final IReservationRepository reservationRepository;
    private final IProductRepository productRepository;
    private final IUserRepository userRepository;
    private final ObjectMapper mapper;
    private final ApplicationEventPublisher applicationEventPublisher;

    public ReservationService(IReservationRepository reservationRepository, IProductRepository productRepository, IUserRepository userRepository, ObjectMapper mapper, ApplicationEventPublisher applicationEventPublisher) {
        this.reservationRepository = reservationRepository;
        this.productRepository = productRepository;
        this.userRepository = userRepository;
        this.mapper = mapper;
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @Override
    public ReservationResponse createReservation(ReservationDto request) throws ApiException {
        Reservation reservation = new Reservation();
        Optional<Product> product = productRepository.findById(request.getProduct_id());
        Optional<User> user = userRepository.findById(request.getUser_id());

        //Validando que existan el usuario y el producto
        if(!product.isPresent()) throw PRODUCT_DOES_NOT_EXIST.toException();
        if(!user.isPresent()) throw USER_DOES_NOT_EXIST.toException();

        //Validando que sea mínimo de dos días la reserva
        Long daysBetweenDates = ChronoUnit.DAYS.between(request.getStartDate(), request.getEndDate());
        if(daysBetweenDates < 0) throw RESERVATION_DATES_INVALID.toException();
        if(daysBetweenDates < 2) throw RESERVATION_TOO_SHORT.toException();

        //Validando disponibilidad de producto
        if(reservationRepository.productReservation(request.getProduct_id(), request.getStartDate(), request.getEndDate())>0){
            throw PRODUCT_NOT_AVILABLE.toException();
        }

        //Para no crear un Mapper, aprovechando que esta entidad es corta
        reservation.setProduct(mapper.convertValue(product, Product.class));
        reservation.setUser(mapper.convertValue(user, User.class));
        reservation.setStartDate(request.getStartDate());
        reservation.setEndDate(request.getEndDate());

        reservationRepository.save(reservation);

        //Envío de confirmación
        this.sendEmail(user.get().getEmail(),user.get().getName(),product.get().getBrand(),
                product.get().getModel(),request.getStartDate(), request.getEndDate());

        return ReservationResponse.builder()
                .success(true)
                .message("Reserva creada con éxito!, a su correo le llegará un correo de confirmación")
                .reservation(mapper.convertValue(reservation, ReservationDto.class))
                .build();
    }

    @Override
    public List<ReservationDto> getReservations() {
        List<Reservation> reservationsBBDD = reservationRepository.findAll();
        List<ReservationDto> reservationsDTO = new ArrayList<>();

        for (Reservation booking: reservationsBBDD){
            ReservationDto bookingDto = new ReservationDto(booking.getProduct().getId(),
                    booking.getUser().getId(), booking.getStartDate(), booking.getEndDate());

            reservationsDTO.add(bookingDto);
        }

        return reservationsDTO;
    }

    @Override
    public ReservationResponse getReservationsByUserId(Long userId) throws ApiException {
        Optional<User> user = userRepository.findById(userId);

        //Validando que exista el usuario
        if(!user.isPresent()) throw USER_DOES_NOT_EXIST.toException();

        List<Reservation> reservations = reservationRepository.findByUserId(userId);
        List<ReservationDto> reservationDtos = mapper.convertValue(reservations, new TypeReference<List<ReservationDto>>() {});
        for (int i = 0; i < reservationDtos.size(); i++) {
            reservationDtos.get(i).setProduct_id(reservations.get(i).getProduct().getId());
        }

        return ReservationResponse.builder()
                .success(true)
                .message("Reservas asociadas al usuario "+user.get().getName())
                .reservations(reservationDtos)
                .build();
    }

    @Override
    public ReservationResponse deleteReservation(Long reservationId) throws ApiException {
        Reservation reservation = reservationRepository.findById(reservationId)
                .orElseThrow(DELETE_RESERVATION_NOT_FOUND::toException);

        reservationRepository.deleteById(reservationId);

        return ReservationResponse.builder()
                .success(true)
                .message("Reserva con id "+reservationId+" eliminada exitosamente")
                .build();
    }

    private void sendEmail(String email, String userName, String productBrand, String productModel, LocalDate startDate, LocalDate endDate){
        String body = "<html>" +
                "<head>" +
                "<title>Confirmacion de reserva exitosa</title>" +
                "</head>" +
                "<body>" +
                "<h1>Saludos! "+userName+"</h1>" +
                "<p>Este es un mensaje de confirmacion de que tu reserva del vehículo"+ productBrand + " " + productModel +" fue exitosa</p>" +
                "<p>Podrás disfrutar de nuestro producto del día "+startDate+" al día "+ endDate +"</p>" +
                "<br><br><br>"+
                "<p>Gracias por utilizar CarRental</p>" +

                "</body>" +
                "</html>";

        EmailEventDto emailEventDto = new EmailEventDto(this, email, "Reserva exitosa - CarRental",body);
        applicationEventPublisher.publishEvent(emailEventDto);
    }
}
