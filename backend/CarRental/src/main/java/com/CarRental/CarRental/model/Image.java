package com.CarRental.CarRental.model;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "images")
public class Image {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Schema(description = "Identificador unico de la imagen.", example = "10")
    private Long id;

    @Schema(description = "Blob imagen.", example = "iVBORw0KGgoAAAANSUhEUgAAAAUA\n" +
            "    AAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO\n" +
            "    9TXL0Y4OHwAAAABJRU5ErkJggg==")
    //@Lob
    //@Column(columnDefinition = "LONGBLOB")
    private String imageUrl;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PRODUCT_ID", referencedColumnName = "id", nullable = false)
    @Schema(description = "Product al que esta relacionada la imagen.")
    private Product product;


}
