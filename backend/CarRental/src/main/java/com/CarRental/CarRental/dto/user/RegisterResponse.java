package com.CarRental.CarRental.dto.user;


import com.CarRental.CarRental.model.UserRol;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class RegisterResponse {
    private boolean success;
    private String message;
    private String warning;
    private String token;
    private String email;
    private String name;
    private String lastName;
    private UserRol userRol;
    private Long id;
}
