package com.CarRental.CarRental.service;

import com.CarRental.CarRental.dto.characteristic.CharacteristicDto;
import com.CarRental.CarRental.dto.product.response.Response;
import com.CarRental.CarRental.exception.ApiException;
import com.CarRental.CarRental.mapper.CharacteristicMapper;
import com.CarRental.CarRental.model.Characteristic;
import com.CarRental.CarRental.repository.ICharacteristicRepository;
import com.CarRental.CarRental.service.interfaces.ICharacteristicService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.CarRental.CarRental.exception.factory.CategoryFactoryException.DELETE_CATEGORY_ERROR_NOT_FOUND;
import static com.CarRental.CarRental.exception.factory.CharacteristicFactoryException.*;

@Service
@Slf4j
public class CharacteristicService implements ICharacteristicService {
    private final ICharacteristicRepository characteristicRepository;
    public CharacteristicService(ICharacteristicRepository characteristicRepository) {
        this.characteristicRepository = characteristicRepository;
    }

    @Override
    public Response createCharacteristic(CharacteristicDto request) throws ApiException {
        //Todo validar no nullo
        Optional<Characteristic> characteristic = characteristicRepository.findByNameOrIconName(request.getName(), request.getIcon());
        if(characteristic.isPresent()) {
            throw  ALREADY_EXIST_CHARACTERISTIC.toException();
        }

         characteristicRepository.save(CharacteristicMapper.toEntity(request));

        return Response.builder()
                .success(true)
                .message("Caracteristica Agragada exitosamente")
                .build();
    }

    @Override
    public Response editCharacteristic(CharacteristicDto request) throws ApiException {
        //Todo validar caracterisitca no null
        boolean existOtherCharacteristy = characteristicRepository.existsByNameOrIconNameAndIdNot(request.getName(), request.getIcon(), request.getId());
        if(existOtherCharacteristy){
            throw ALREADY_EXIST_CHARACTERISTIC.toException();
        }

        characteristicRepository.save(CharacteristicMapper.toEntity(request));

        return Response.builder()
                .success(true)
                .message("Caracteristica Actualizada exitosamente")
                .build();
    }

    @Override
    public List<CharacteristicDto> getCharacteristic() {
        return this.characteristicRepository.findAll().stream().map(CharacteristicMapper::toDto).toList();
    }

    @Override
    public Response deleteCharacteristic(Long id) throws ApiException {
        Characteristic characteristic = characteristicRepository.findById(id)
                .orElseThrow(DELETE_CATEGORY_ERROR_NOT_FOUND::toException);

        //Validando si tiene productos asociados
        if(!characteristic.getProducts().isEmpty()){
            throw CHARACTERISTIC_IN_USE.toException();
        }
        characteristicRepository.deleteById(id);

        return Response.builder()
                .success(true)
                .message("Caracteristica eliminada exitosamente")
                .build();
    }

    @Override
    public CharacteristicDto findById(Long id) throws ApiException {
        return this.characteristicRepository.findById(id).map(CharacteristicMapper::toDto).orElseThrow(CHARACTERISTIC_NOT_FOUND::toException);
    }

    @Override
    public List<Characteristic> findByInId(List<Long> ids) {
        return this.characteristicRepository.findByIdIn(ids);
    }
}
