package com.CarRental.CarRental.config;


import com.CarRental.CarRental.model.*;
import com.CarRental.CarRental.repository.ICategoryRepository;
import com.CarRental.CarRental.repository.IProductRepository;
import com.CarRental.CarRental.repository.IReservationRepository;
import com.CarRental.CarRental.repository.IUserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.annotation.PostConstruct;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Component
public class AdminUserInitialization {
    private final IUserRepository adminRepository;
    private final ICategoryRepository categoryRepository;
    private final IProductRepository productRepository;
    private final PasswordEncoder bCryptPasswordEncoder;
    private final IReservationRepository reservationRepository;
    private final IUserRepository userRepository;
    private final ObjectMapper mapper;

    public AdminUserInitialization (final IUserRepository adminRepository, ICategoryRepository categoryRepository, IProductRepository productRepository, final PasswordEncoder bCryptPasswordEncoder, IReservationRepository reservationRepository, IUserRepository userRepository, ObjectMapper mapper) {
        this.adminRepository = adminRepository;
        this.categoryRepository = categoryRepository;
        this.productRepository = productRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.reservationRepository = reservationRepository;
        this.userRepository = userRepository;
        this.mapper = mapper;
    }
    @PostConstruct
    public void initialization(){
        try {
            usersInit();
            //categoriesInit();
            //productsInit();
            //reservationsInit();
        } catch (Exception e) {
            // Manejo de la excepción
            e.printStackTrace();
        }
    }

    public void initial() throws IOException {
        categoriesInit();
        productsInit();
    }
    private void usersInit(){
        saveUser(null,"carrentainfo@gmail.com", "Super", "Administrador", UserRol.SUPER_ADMINISTRADOR, "Prueba123$");
        saveUser(null,"fardila@rentalcar.com", "Fernanda", "Ardila", UserRol.CLIENTE, "Cliente123$");
        saveUser(null,"aalvarado@rentalcar.com", "Amy", "Alvarado", UserRol.CLIENTE, "Cliente123$");
        saveUser(null,"mlopez@rentalcar.com", "Marcos", "Lopez", UserRol.ADMINISTRADOR, "Prueba123$");
    }

    private void saveUser (Long id, String email, String name, String lastName, UserRol rol , String password){

        if(!adminRepository.findByEmail(email).isPresent()){

            User administrator = new User();
            administrator.setId(id);
            administrator.setEmail(email);
            administrator.setPassword(bCryptPasswordEncoder.encode(password));
            administrator.setName(name);
            administrator.setSurname(lastName);
            administrator.setRole(rol);
            adminRepository.save(administrator);
        }
    }

    private void categoriesInit() throws IOException {
        Category category1 = new Category(
                1L,
                "SUV",
                "Vehículo espacioso con capacidad para transportar pasajeros y carga, ideal para aventuras en carretera y terrenos irregulares.",
                "https://gitlab.com/mtimoxenko/integrador/-/raw/d922ce467b83c035e9877518148899a0e0159531/backend/CarRental/src/images/Categorias/SUV.jpg"
        );
        categoryRepository.save(category1);

        Category category2 = new Category(
                2L,
                "Compacto",
                "Automóvil pequeño y ágil, perfecto para la ciudad y estacionamiento en espacios reducidos.",
                "https://img.remediosdigitales.com/caa8d4/2016_chevrolet_spark_5/1366_2000.jpg"
                );
        categoryRepository.save(category2);

        Category category3 = new Category(
                3L,
                "Sedán",
                "Automóvil de tamaño mediano a grande, con un diseño elegante y cómodo, ideal para viajes en carretera y uso diario.",
                "https://gitlab.com/mtimoxenko/integrador/-/raw/d922ce467b83c035e9877518148899a0e0159531/backend/CarRental/src/images/Categorias/sedan.jpg"                );
                categoryRepository.save(category3);

        Category category4 = new Category(
                4L,
                "Todo Terreno (4x4)",
                "Vehículo robusto diseñado para enfrentar terrenos difíciles y condiciones adversas con tracción en las cuatro ruedas.",
                "https://gitlab.com/mtimoxenko/integrador/-/raw/d922ce467b83c035e9877518148899a0e0159531/backend/CarRental/src/images/Categorias/todoTerreno.jpg"
        );
        categoryRepository.save(category4);

        Category category5 = new Category(
                5L,
                "Pickup",
                "Camioneta con una cama de carga trasera, versátil para trabajos pesados y transporte de mercancías.",
                "https://gitlab.com/mtimoxenko/integrador/-/raw/d922ce467b83c035e9877518148899a0e0159531/backend/CarRental/src/images/Categorias/pickup.jpg"
        );
        categoryRepository.save(category5);

        Category category6 = new Category(
                6L,
                "Monovolumen",
                "Vehículo familiar con gran espacio interior y asientos modulares, perfecto para viajes en grupo y carga voluminosa.",
                "https://gitlab.com/mtimoxenko/integrador/-/raw/d922ce467b83c035e9877518148899a0e0159531/backend/CarRental/src/images/Categorias/monovolumen.jpg"
        );
        categoryRepository.save(category6);

        Category category7 = new Category(
                7L,
                "Deportivo",
                "Automóvil de alto rendimiento con un diseño aerodinámico y potente motor, enfocado en la velocidad y la emoción al conducir.",
                "https://gitlab.com/mtimoxenko/integrador/-/raw/d922ce467b83c035e9877518148899a0e0159531/backend/CarRental/src/images/Categorias/deportivo.jpg"
        );
        categoryRepository.save(category7);

        Category category8 = new Category(
                8L,
                "Convertible",
                "Automóvil con techo retráctil o desmontable, ideal para disfrutar de paseos al aire libre y días soleados",
                "https://gitlab.com/mtimoxenko/integrador/-/raw/d922ce467b83c035e9877518148899a0e0159531/backend/CarRental/src/images/Categorias/Convertible.jpg"
        );
        categoryRepository.save(category8);

        Category category9 = new Category(
                9L,
                "Eléctrico",
                "Automóvil impulsado por un motor eléctrico en lugar de un motor de combustión interna, ofreciendo una conducción eficiente y respetuosa con el medio ambiente, con cero emisiones de escape y menor dependencia de combustibles fósiles.",
                "https://gitlab.com/mtimoxenko/integrador/-/raw/d922ce467b83c035e9877518148899a0e0159531/backend/CarRental/src/images/Categorias/omicrono_electrico.jpg"
        );
        categoryRepository.save(category9);
    }

    private byte[] imageLoad(String fileName) throws IOException {
        Path path = Paths.get(fileName);
        byte[] imageBytes = Files.readAllBytes(path);
        String base64Image = "data:image/jpeg;base64," + Base64.getEncoder().encodeToString(imageBytes);
        return Base64.getUrlEncoder().encode(base64Image.getBytes(StandardCharsets.UTF_8));
    }

    private void productsInit() throws IOException {
        for (int j = 1; j <= 36; j += 5) {
            productCreation("Sedán", (long) j, "Ferrari" + j, "Ferrari", "f40", "Auto italiano deportivo",
                    "Gasolina", 2, 2, 0, false, "Ferrari_458");

            productCreation("SUV", (long) (j + 1), "Nissan Leaf" + j, "Nissan", "Leaf", "Pequeño automóvil ágil, sutil y elegante",
                    "Gasolina", 4, 4, 1, false, "Nissan_Leaf");

            productCreation("Compacto", (long) (j + 2), "Ford_Mondeo" + j, "Ford", "Mondeo", "Automóvil familiar P508",
                    "Gasolina", 4, 4, 2, false, "Ford_Mondeo");

            productCreation("Todo Terreno (4x4)", (long) (j + 3), "Jaguar_F-Type" + j, "Jaguar", "F-Type", "Auto italiano deportivo",
                    "Gasolina", 2, 2, 0, false, "Jaguar_F-Type");

            productCreation("Pickup", (long) (j + 4), "Acura_NSX" + j, "Acura", "NSX", "Pequeño automóvil ágil, sutil y elegante",
                    "Gasolina", 4, 4, 1, false, "Acura_NSX");
        }

    }


    public void productCreation(String categoryName, Long productId, String name, String brand, String model, String description,
                                String fuel, int numPassengers, int numBags, int numDoors, boolean isAutomatic,
                                String imagesFolder) throws IOException {

        Category category1 = categoryRepository.findByName(categoryName).get();
        Product product1 = new Product(
                productId,
                name,
                category1,
                brand,
                model,
                description,
                fuel,
                numPassengers,
                numBags,
                numDoors,
                isAutomatic,
                new ArrayList<>(),
                new ArrayList<>(),
                null
    );

    File folder = new File("./src/images/" + imagesFolder);
    File[] files = folder.listFiles();
    if (files != null) {
        List<Image> imagesP1 = new ArrayList<>();
        for (File file : files) {
            String path = file.getPath();
            path = path.replace("\\", "/");
            path = path.substring(1);

            Image image = new Image(null, "https://gitlab.com/mtimoxenko/integrador/-/raw/d922ce467b83c035e9877518148899a0e0159531/backend/CarRental"+path, product1);
            imagesP1.add(image);
        }
        product1.setImages(imagesP1);
        productRepository.save(product1);
    } else {
        throw new FileNotFoundException("No se encontraron archivos en la carpeta especificada");
    }
}


    private void reservationsInit(){
        createReservation(1L, 1L, LocalDate.of(2024,03,10), LocalDate.of(2024,03,16));
        createReservation(3L, 2L, LocalDate.of(2024,03,16), LocalDate.of(2024,03,21));
        createReservation(4L, 3L, LocalDate.of(2024,04,01), LocalDate.of(2024,04,8));
        createReservation(4L, 2L, LocalDate.of(2024,04,13), LocalDate.of(2024,04,15));
        createReservation(4L, 1L, LocalDate.of(2024,04,18), LocalDate.of(2024,04,22));
        createReservation(10L, 4L, LocalDate.of(2024,04,15), LocalDate.of(2024,04,18));
    }
    private void createReservation(Long productId, Long userId, LocalDate startDate, LocalDate endDate){
        Product product = mapper.convertValue(productRepository.findById(productId), Product.class);
        User user = mapper.convertValue(userRepository.findById(userId), User.class);

        Reservation reservation = new Reservation(null, product, user, startDate, endDate);
        reservationRepository.save(reservation);
    }
}