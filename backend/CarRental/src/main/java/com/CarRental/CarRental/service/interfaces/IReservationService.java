package com.CarRental.CarRental.service.interfaces;


import com.CarRental.CarRental.dto.reservation.ReservationDto;
import com.CarRental.CarRental.dto.reservation.ReservationResponse;
import com.CarRental.CarRental.exception.ApiException;

import java.util.List;

public interface IReservationService {
    ReservationResponse createReservation(ReservationDto request) throws ApiException;
    List<ReservationDto> getReservations();
    ReservationResponse getReservationsByUserId(Long userId) throws ApiException;
    ReservationResponse deleteReservation(Long reservationId) throws ApiException;

}
