package com.CarRental.CarRental.dto.reservation;

import com.CarRental.CarRental.dto.product.response.Response;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@SuperBuilder
@ToString(callSuper = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@EqualsAndHashCode(callSuper = true)
public class ReservationResponse extends Response {
    private ReservationDto reservation;
    private List<ReservationDto> reservations;
}
