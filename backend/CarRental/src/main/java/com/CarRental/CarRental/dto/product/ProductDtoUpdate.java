package com.CarRental.CarRental.dto.product;

import com.CarRental.CarRental.dto.image.ImageRequest;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductDtoUpdate {
    private Long id;
    private Long category_id;
}
