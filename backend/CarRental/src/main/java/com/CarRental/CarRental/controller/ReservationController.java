package com.CarRental.CarRental.controller;

import com.CarRental.CarRental.dto.reservation.ReservationDto;
import com.CarRental.CarRental.dto.reservation.ReservationResponse;
import com.CarRental.CarRental.exception.ApiException;
import com.CarRental.CarRental.exception.dto.ApiErrorDto;
import com.CarRental.CarRental.service.ReservationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.log4j.Logger;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.CarRental.CarRental.util.GeneralConstansSwagger.*;
import static com.CarRental.CarRental.util.GeneralConstansSwagger.RESPONSE_ERROR_404_DELETE_RESERVATION_NOT_FOUND;

@RestController
@Tag(name = "Reserva", description = "Endpoints para la gestión de Reservas")
@RequestMapping(value = "/reservation")
public class ReservationController {
    private final ReservationService reservationService;
    private final Logger LOGGER = Logger.getLogger(ReservationController.class);

    public ReservationController(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @Operation(summary = "Listar reservas", description = "Método encargado de listar reservas")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "success", content = @Content(examples = {
                    @ExampleObject(name = "Reservas registradas:", value =
                            "[\n" +
                                    "    {\n" +
                                    "        \"product_id\": 1,\n" +
                                    "        \"user_id\": 1,\n" +
                                    "        \"startDate\": [\n" +
                                    "            2024,\n" +
                                    "            3,\n" +
                                    "            10\n" +
                                    "        ],\n" +
                                    "        \"endDate\": [\n" +
                                    "            2024,\n" +
                                    "            3,\n" +
                                    "            16\n" +
                                    "        ]\n" +
                                    "    },\n" +
                                    "    {\n" +
                                    "        \"product_id\": 3,\n" +
                                    "        \"user_id\": 2,\n" +
                                    "        \"startDate\": [\n" +
                                    "            2024,\n" +
                                    "            3,\n" +
                                    "            16\n" +
                                    "        ],\n" +
                                    "        \"endDate\": [\n" +
                                    "            2024,\n" +
                                    "            3,\n" +
                                    "            21\n" +
                                    "        ]\n" +
                                    "    },\n" +
                                    "    {\n" +
                                    "        \"product_id\": 4,\n" +
                                    "        \"user_id\": 3,\n" +
                                    "        \"startDate\": [\n" +
                                    "            2024,\n" +
                                    "            4,\n" +
                                    "            1\n" +
                                    "        ],\n" +
                                    "        \"endDate\": [\n" +
                                    "            2024,\n" +
                                    "            4,\n" +
                                    "            8\n" +
                                    "        ]\n" +
                                    "    },\n" +
                                    "]"
                    )
            }))
    })
    @GetMapping("/public")
    public List<ReservationDto> getReservations(){
        return reservationService.getReservations();
    }

    @Operation(summary = "Crear reserva", description = "Método encargado de crear una reserva")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "success", content = @Content(examples = {
                    @ExampleObject(name = "Reserva creada exitosamente", value =
                            "{\n" +
                                    "    \"success\": true,\n" +
                                    "    \"message\": \"Reserva creada con éxito!, a su correo le llegará un correo de confirmación\",\n" +
                                    "    \"reservation\": {\n" +
                                    "        \"startDate\": [\n" +
                                    "            2024,\n" +
                                    "            2,\n" +
                                    "            5\n" +
                                    "        ],\n" +
                                    "        \"endDate\": [\n" +
                                    "            2024,\n" +
                                    "            2,\n" +
                                    "            8\n" +
                                    "        ]\n" +
                                    "    }\n" +
                                    "}"
                    )
            })),
            @ApiResponse(responseCode = "400", description = "Bad request", content = @Content(schema = @Schema(implementation = ApiErrorDto.class), examples = {
                    @ExampleObject(name = "Product_not_avilable", value = RESPONSE_ERROR_400_PRODUCT_NOT_AVILABLE)
            })),
            @ApiResponse(responseCode = "400", description = "Bad request", content = @Content(schema = @Schema(implementation = ApiErrorDto.class), examples = {
                    @ExampleObject(name = "Product_does_not_exist", value = RESPONSE_ERROR_400_PRODUCT_DOES_NOT_EXIST)
            })),
            @ApiResponse(responseCode = "400", description = "Bad request", content = @Content(schema = @Schema(implementation = ApiErrorDto.class), examples = {
                    @ExampleObject(name = "User_does_not_exist", value = RESPONSE_ERROR_400_USER_DOES_NOT_EXIST)
            })),
            @ApiResponse(responseCode = "400", description = "Bad request", content = @Content(schema = @Schema(implementation = ApiErrorDto.class), examples = {
                    @ExampleObject(name = "Reservation_too_short", value = RESPONSE_ERROR_400_RESERVATION_TOO_SHORT)
            })),
            @ApiResponse(responseCode = "400", description = "Bad request", content = @Content(schema = @Schema(implementation = ApiErrorDto.class), examples = {
                    @ExampleObject(name = "Reservation_dates_invalid", value = RESPONSE_ERROR_400_RESERVATION_DATES_INVALID)
            })),
            @ApiResponse(responseCode = "404", description = "Not found", content = @Content(schema = @Schema(implementation = ApiErrorDto.class), examples = {
                    @ExampleObject(name = "Delete_reservation_not_found", value = RESPONSE_ERROR_404_DELETE_RESERVATION_NOT_FOUND)
            }))
    })
    @PostMapping()
    @PreAuthorize("hasRole('SUPER_ADMINISTRADOR') || hasRole('ADMINISTRADOR') || hasRole('CLIENTE')")
    public ReservationResponse createBooking(@RequestBody ReservationDto reservationDto) throws ApiException {
        return reservationService.createReservation(reservationDto);
    }


    @Operation(summary = "Listar reservas de un usuario determinado", description = "Método encargado de listar reservas de un usuario por su ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "success", content = @Content(examples = {
                    @ExampleObject(name = "Reservas registradas para el usuario en cuestión:", value =
                            "{\n" +
                                    "    \"success\": true,\n" +
                                    "    \"message\": \"Reservas asociadas al usuario\",\n" +
                                    "    \"reservations\": [\n" +
                                    "        {\n" +
                                    "            \"product_id\": 3,\n" +
                                    "            \"startDate\": [\n" +
                                    "                2024,\n" +
                                    "                3,\n" +
                                    "                16\n" +
                                    "            ],\n" +
                                    "            \"endDate\": [\n" +
                                    "                2024,\n" +
                                    "                3,\n" +
                                    "                21\n" +
                                    "            ]\n" +
                                    "        },\n" +
                                    "        {\n" +
                                    "            \"product_id\": 4,\n" +
                                    "            \"startDate\": [\n" +
                                    "                2024,\n" +
                                    "                4,\n" +
                                    "                13\n" +
                                    "            ],\n" +
                                    "            \"endDate\": [\n" +
                                    "                2024,\n" +
                                    "                4,\n" +
                                    "                15\n" +
                                    "            ]\n" +
                                    "        },\n" +
                                    "        {\n" +
                                    "            \"product_id\": 9,\n" +
                                    "            \"startDate\": [\n" +
                                    "                2024,\n" +
                                    "                2,\n" +
                                    "                5\n" +
                                    "            ],\n" +
                                    "            \"endDate\": [\n" +
                                    "                2024,\n" +
                                    "                2,\n" +
                                    "                8\n" +
                                    "            ]\n" +
                                    "        }\n" +
                                    "    ]\n" +
                                    "}"
                    )
            }))
    })
    @PreAuthorize("hasRole('SUPER_ADMINISTRADOR') || hasRole('ADMINISTRADOR') || hasRole('CLIENTE')")
    @GetMapping("/byUser/{id}")
    public  ReservationResponse getReservationsByUsedId(@PathVariable Long id) throws ApiException {
        return reservationService.getReservationsByUserId(id);
    }

    @Operation(summary = "Eliminar reserva", description = "Método encargado de elimimnar una reserva")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "success", content = @Content(examples = {
                    @ExampleObject(name = "Eliminar reserva exitosamente", value =
                            "{\n" +
                                    "    \"success\": true,\n" +
                                    "    \"message\": \"Reserva eliminada exitosamente\"\n" +
                                    "}"
                    )
            }))
    })
    @DeleteMapping(value = "/{id}")
    @PreAuthorize("hasRole('ADMINISTRADOR') || hasRole('SUPER_ADMINISTRADOR')")
    public ReservationResponse deleteReservation(@PathVariable Long id) throws ApiException{
        return reservationService.deleteReservation(id);
    }


}