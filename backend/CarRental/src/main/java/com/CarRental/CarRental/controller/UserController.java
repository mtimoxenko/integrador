package com.CarRental.CarRental.controller;

import com.CarRental.CarRental.dto.product.response.Response;
import com.CarRental.CarRental.dto.user.LoginUserRequest;
import com.CarRental.CarRental.dto.user.LoginUserResponse;
import com.CarRental.CarRental.dto.user.RegisterRequest;
import com.CarRental.CarRental.dto.user.RegisterResponse;
import com.CarRental.CarRental.exception.ApiException;
import com.CarRental.CarRental.exception.dto.ApiErrorDto;
import com.CarRental.CarRental.service.interfaces.IUserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import static com.CarRental.CarRental.util.GeneralConstansSwagger.*;

@Slf4j
@Tag(name = "User", description = "Endpoints para la gestión de usuarios")
@RestController
@RequestMapping("/user")
public class UserController {
    final IUserService userService;

    public UserController(IUserService userService) {
        this.userService = userService;
    }

    @Operation(summary = "Inicio de sesión del usuario", description = "Autentica a un usuario y devuelve información de inicio de sesión")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Inicio de sesión exitoso",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = LoginUserResponse.class))),
            @ApiResponse(responseCode = "401", description = "Bad request", content = @Content(schema = @Schema(implementation = ApiErrorDto.class), examples = {
                    @ExampleObject(name = "bad_credential", value = RESPONSE_ERROR_401_BAD_CREDENTIAL)
            }))
    })
    @PostMapping("/public/login")
    public ResponseEntity<LoginUserResponse> loginUser(
            @Parameter(description = "Credenciales de login del User", required = true) @RequestBody LoginUserRequest loginUserRequest) throws ApiException {
        LoginUserResponse loginUserResponse = userService.login(loginUserRequest);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("user_login", "true");
        return ResponseEntity.ok()
                .headers(httpHeaders)
                .body(loginUserResponse);
    }

    @Operation(summary = "Registro de un usuario", description = "Método encargado para registriar un usuario")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Registro exitoso",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = RegisterResponse.class))),
            @ApiResponse(responseCode = "400", description = "Bad request", content = @Content(schema = @Schema(implementation = ApiErrorDto.class), examples = {
                    @ExampleObject(name = "request_null", value = RESPONSE_ERROR_400_REQUEST_NULL),
                    @ExampleObject(name = "email_invalid", value = RESPONSE_ERROR_400_EMAIL_INVALID),
                    @ExampleObject(name = "password_invalid", value = RESPONSE_ERROR_400_PASSWORD_INVALID),
                    @ExampleObject(name = "name_invalid", value = RESPONSE_ERROR_400_NAME_INVALID),
                    @ExampleObject(name = "already_user", value = RESPONSE_ERROR_400_ALREADY_USER)
            }))
    })
    @PostMapping("/public/register")
    public RegisterResponse register(@RequestBody final RegisterRequest request) throws ApiException {
        return  this.userService.registerUser(request);
    }

    @Operation(summary = "Reenviar email", description = "Método encargado de reenviar un email")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Reenvío exitoso de email",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = RegisterResponse.class))),
            @ApiResponse(responseCode = "400", description = "Bad request", content = @Content(schema = @Schema(implementation = ApiErrorDto.class), examples = {
                    @ExampleObject(name = "request_null", value = RESPONSE_ERROR_400_REQUEST_NULL),
                    @ExampleObject(name = "email_invalid", value = RESPONSE_ERROR_400_EMAIL_INVALID),
                    @ExampleObject(name = "notFoundEmail", value = RESPONSE_ERROR_400_NOT_FOUND_USER)
            }))
    })
    @PostMapping("/public/resendEmail")
    public RegisterResponse resendEmail(@RequestBody final RegisterRequest request) throws ApiException {
        return  this.userService.resendEmail(request);
    }

    @Operation(summary = "Cierre de sesión del usuario", description = "Cierre de sesión de un usuario autenticado")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Cierre de sesión exitoso",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Response.class)))
    })
    @PostMapping("/logout")
    @PreAuthorize("hasRole('ADMINISTRADOR') || hasRole('CLIENTE')")
    public Response login(@RequestHeader(name = HttpHeaders.AUTHORIZATION) String jwt) throws ApiException {

        return this.userService.logout(jwt);
    }
}