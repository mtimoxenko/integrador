package com.CarRental.CarRental.dto.characteristic;

import jakarta.validation.constraints.NotBlank;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CharacteristicDto {
    private long id;
    @NotBlank(message = "El icono no puede estar en blanco")
    private String icon;
    @NotBlank(message = "El nombre no puede estar en blanco")
    private String name;
}
