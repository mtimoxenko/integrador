package com.CarRental.CarRental.repository;

import com.CarRental.CarRental.model.Characteristic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ICharacteristicRepository extends JpaRepository<Characteristic, Long> {
    Optional<Characteristic> findByNameOrIconName(String name, String iconName);
    boolean existsByNameOrIconNameAndIdNot(String nombre, String icono, Long id);
    List<Characteristic> findByIdIn(List<Long> ids);
}
