package com.CarRental.CarRental.controller;

import com.CarRental.CarRental.dto.product.FavoriteDto;
import com.CarRental.CarRental.dto.product.request.FavoriteRequest;
import com.CarRental.CarRental.dto.product.response.Response;
import com.CarRental.CarRental.exception.ApiException;
import com.CarRental.CarRental.exception.dto.ApiErrorDto;
import com.CarRental.CarRental.service.interfaces.IFavoriteService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.CarRental.CarRental.util.GeneralConstansSwagger.RESPONSE_ERROR_400_USER_OR_PRODUCT_NOT_FOUND;

@Slf4j
@Tag(name = "Favoritos", description = "Endpoints para el manejo de favoritos")
@RestController
@RequestMapping("/favorito")
public class FavoriteController {


    private final IFavoriteService favoriteService;

    public FavoriteController(IFavoriteService favoriteService) {
        this.favoriteService = favoriteService;
    }

    @Operation(summary = "Obtener favoritos de un usuario", description = "Enpoint encargado de listar los favoritos de un usuario determinado")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "success", content = @Content(examples = {
                    @ExampleObject(name = "Productos favoritos del usuario en cuestión: ", value =
                            "[\n" +
                                    "    {\n" +
                                    "        \"idFav\": 1,\n" +
                                    "        \"productDto\": {\n" +
                                    "            \"id\": 3,\n" +
                                    "            \"name\": \"Vehículo 3\",\n" +
                                    "            \"category\": {\n" +
                                    "                \"id\": 2,\n" +
                                    "                \"name\": \"Categoría 2\",\n" +
                                    "                \"description\": \"Descripción de la categoría 2\",\n" +
                                    "                \"imageUrl\": \"https://imagenCategoría2.jpg\",\n" +
                                    "                \"products\": []\n" +
                                    "            },\n" +
                                    "            \"brand\": \"Marca Vehículo 3\",\n" +
                                    "            \"model\": \"Modelo Vehículo 3\",\n" +
                                    "            \"description\": \"Descripción Vehículo 3\",\n" +
                                    "            \"fuel\": \"Combustible Vehículo 3\",\n" +
                                    "            \"numPassengers\": 4,\n" +
                                    "            \"numBags\": 4,\n" +
                                    "            \"numDoors\": 2,\n" +
                                    "            \"isAutomatic\": false,\n" +
                                    "            \"images\": [\n" +
                                    "                {\n" +
                                    "                    \"id\": 11,\n" +
                                    "                    \"imageUrl\": \"https://imagen1.jpg\"\n" +
                                    "                },\n" +
                                    "                {\n" +
                                    "                    \"id\": 12,\n" +
                                    "                    \"imageUrl\": \"https://imagen1.jpg\"\n" +
                                    "                },\n" +
                                    "                {\n" +
                                    "                    \"id\": 13,\n" +
                                    "                    \"imageUrl\": \"https://imagen1.jpg\"\n" +
                                    "                },\n" +
                                    "                {\n" +
                                    "                    \"id\": 14,\n" +
                                    "                    \"imageUrl\": \"https://imagen1.jpg\"\n" +
                                    "                },\n" +
                                    "                {\n" +
                                    "                    \"id\": 15,\n" +
                                    "                    \"imageUrl\": \"https://imagen1.jpg\"\n" +
                                    "                }\n" +
                                    "            ],\n" +
                                    "        }\n" +
                                    "    }\n" +
                                    "]"
                    )
            }))
    })
    @PreAuthorize("hasRole('SUPER_ADMINISTRADOR') || hasRole('ADMINISTRADOR') || hasRole('CLIENTE')")
    @GetMapping("/{id}")
    public List<FavoriteDto> getFavoriteByUserId(@PathVariable Long id) {
        return favoriteService.getFavoritesByUser(id);
    }

    @Operation(summary = "Agregar favorito", description = "Enpoint encargado de agregar un producto favorito a un usuario asociado")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "success", content = @Content(examples = {
                    @ExampleObject(name = "Producto agregado exitosamente", value =
                            "{\n" +
                                    "    \"success\": true,\n" +
                                    "    \"message\": \"El producto fue Agregado a la lista de favoritos\"\n" +
                                    "}"
                    )
            })),
            @ApiResponse(responseCode = "400", description = "Not found", content = @Content(schema = @Schema(implementation = ApiErrorDto.class), examples = {
                    @ExampleObject(name = "product_or_user_Error_not_found", value = RESPONSE_ERROR_400_USER_OR_PRODUCT_NOT_FOUND)
            }))
    })
    @PreAuthorize("hasRole('SUPER_ADMINISTRADOR') || hasRole('ADMINISTRADOR') || hasRole('CLIENTE')")
    @PostMapping("/agregar")
    public Response createFavorite(@RequestBody FavoriteRequest request) throws ApiException {
        return favoriteService.createFavorite(request.getUserId(), request.getProductId());

    }

    @Operation(summary = "Eliminar favorito", description = "Enpoint encargado de elimimnar un producto favorito de un usuario determinado")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "success", content = @Content(examples = {
                    @ExampleObject(name = "Eliminación exitosa de favorito", value =
                            "{\n" +
                                    "    \"success\": true,\n" +
                                    "    \"message\": \"El producto fue eiliminado de la lista de favoritos\"\n" +
                                    "}"
                    )
            }))
    })
    @PreAuthorize("hasRole('SUPER_ADMINISTRADOR') || hasRole('ADMINISTRADOR') || hasRole('CLIENTE')")
    @DeleteMapping
    public Response deleteFavorite(@RequestBody FavoriteRequest request) {
        return favoriteService.deleteFavorite(request.getUserId(), request.getProductId());
    }
}