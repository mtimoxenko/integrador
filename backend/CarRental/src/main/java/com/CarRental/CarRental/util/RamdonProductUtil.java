package com.CarRental.CarRental.util;

import com.CarRental.CarRental.dto.product.ProductDto;
import com.CarRental.CarRental.mapper.ProductMapper;
import com.CarRental.CarRental.model.Product;

import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

public class RamdonProductUtil {
    private static Map<String,Map<Long,Instant >> recordsByUser = new HashMap<String,Map<Long,Instant >>();
    private Set<RecordWithTimestamp> selectedRecords;
    private static Instant lastCleanupTime= Instant.now();
    private static final int MAX_RECORD = 10;
    private RamdonProductUtil(){
        this.lastCleanupTime = Instant.now();
    }

    public static final List<ProductDto>getRamdomProduct(final List<Product> products, final String username) {
       return getRamdomProduct1(products, username)
                .stream()
                .map(ProductMapper::toDto)
                .collect(Collectors.toList());

    }

    private static  List<Product>  getRamdomProduct1(final List<Product> products, final String username) {
        // Si hay menos de 10 products, se baraja la lista y se retorna.
        if(products.size()<=MAX_RECORD) {
             Collections.shuffle(products);
            return products;
        }

        // Si hay mas de 10 products, se verifica si han pasado mas de 2 horas para ejecutar el limpiadoor y se actualiza la hora.
        final Map<Long,Instant > selectedRecords = recordsByUser.getOrDefault(username, new HashMap<>());
        //Verfiicar si tiene mas de 2 horas la ultima consulta para limpiar los seleccionados
        if(Duration.between(lastCleanupTime, Instant.now()).toHours()>=2) {
            cleanUpRecords(selectedRecords);
            lastCleanupTime = Instant.now();
        }
        // si hay registros previamente seleccionados y que no tengan mas 2 horas, se eliminan de la lista ded products actual a retornar
        if(!selectedRecords.isEmpty() &&(products.size()-selectedRecords.size())>=MAX_RECORD){
            products.removeIf(record-> selectedRecords.containsKey(record.getId()));
        }
        if((products.size()-selectedRecords.size())<MAX_RECORD){
            selectedRecords.clear();
        }
        //Si luego de eliminar los registros seleccionados, quedan menos de 10 registros. se borra la seleccioon y se
        // agregan los nuevos registros a la seleccion.
        if(products.size()<=MAX_RECORD) {
            selectedRecords.clear();
            products
                    .stream()
                    .parallel()
                    .forEach(producto -> selectedRecords.put(producto.getId(), Instant.now()));

            recordsByUser.put(username, selectedRecords);
            Collections.shuffle(products);
            return products;
        }

        List<Product> randomRedords = new ArrayList<>();

        while (randomRedords.size() < MAX_RECORD) {
            int randomIndex = (int) (Math.random() * products.size());
            Product product = products.remove(randomIndex);
            randomRedords.add(product);
            selectedRecords.put(product.getId(), Instant.now());
        }
        recordsByUser.put(username, selectedRecords);
    return randomRedords;
    }

    // se recorren los registros anteriormente seleccionados aleatoreamente y se elimina de la seleccion
    // aquellos que tienen mas de 2 horas registrados.
    //duda probar
    private static void cleanUpRecords(Map<Long,Instant > selectedRecords ){
        if(selectedRecords.isEmpty()){
            return;
        }
        selectedRecords.entrySet().stream().forEach(x->{
            if (Duration.between(x.getValue(), Instant.now()).toHours() >= 2) {
                selectedRecords.remove(x.getKey());
            }
        });
    }
}
