package com.CarRental.CarRental.model;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "caracteristica")
public class Characteristic {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Schema(description = "Identificador único", example = "1L")
    private Long id;

    @Schema(description = "Nombre de la caracteristica", example = "Aire acondicionado")
    private String name;

    @Schema(description = "Nombre del icono", example = "icon")
    private String iconName;

    @ManyToMany(mappedBy = "caracteristicas")
    private List<Product> products = new ArrayList<>();
}
