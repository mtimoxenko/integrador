package com.CarRental.CarRental.dto.product;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class FavoriteDto {
    private long idFav;
    private ProductDto productDto;
}
