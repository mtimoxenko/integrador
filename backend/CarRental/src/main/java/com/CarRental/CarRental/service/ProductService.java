package com.CarRental.CarRental.service;

import com.CarRental.CarRental.dto.PageableDto;
import com.CarRental.CarRental.dto.product.ProductDto;
import com.CarRental.CarRental.dto.product.ProductDtoCreate;
import com.CarRental.CarRental.dto.product.ProductDtoCategory;
import com.CarRental.CarRental.dto.product.request.ProductCriteria;
import com.CarRental.CarRental.dto.product.request.ProductRequest;
import com.CarRental.CarRental.dto.product.response.ProductBrandsResponse;
import com.CarRental.CarRental.dto.product.response.ProductCategoryResponse;
import com.CarRental.CarRental.dto.product.response.ProductResponse;
import com.CarRental.CarRental.exception.ApiException;
import com.CarRental.CarRental.mapper.ProductMapper;
import com.CarRental.CarRental.model.Category;
import com.CarRental.CarRental.model.Image;
import com.CarRental.CarRental.model.Product;
import com.CarRental.CarRental.model.Reservation;
import com.CarRental.CarRental.repository.IImageRepository;
import com.CarRental.CarRental.repository.IProductRepository;
import com.CarRental.CarRental.service.interfaces.ICharacteristicService;
import com.CarRental.CarRental.service.interfaces.IProductService;
import com.CarRental.CarRental.util.RamdonProductUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.persistence.criteria.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.CarRental.CarRental.exception.factory.ProductFactoryException.*;

@Service
@Slf4j
public class ProductService implements IProductService {
    
    private final IProductRepository productRepository;
    private final ICharacteristicService characteristicService;
    private final IImageRepository imageRepository;
    private final CategoryService categoryService;

    private final ObjectMapper mapper;

    public ProductService(final IProductRepository productRepository, ICharacteristicService characteristicService, IImageRepository imageRepository, CategoryService categoryService, ObjectMapper mapper) {
        this.productRepository = productRepository;
        this.characteristicService = characteristicService;
        this.imageRepository = imageRepository;
        this.categoryService = categoryService;
        this.mapper = mapper;
    }

    public ProductResponse createProduct(final ProductDtoCreate request) throws ApiException {
        String name = request.getName().toLowerCase().replace(" ","").trim().replace("-","");
        final Optional<Product> productDto = productRepository
                .findByName(name);
        if(productDto.isPresent()) {
            throw ALREADY_EXIST_PRODUCT.toException();
        }

        Product product = ProductMapper.toEntityCreate(request);
        product.setName(name);
        product.setCategory(categoryService.findById(request.getCategory_id()));
        product.setCaracteristicas(characteristicService.findByInId(request.getCharacteristicsId()));
        final Product productSaved = productRepository.save(product);

        List<Image> images = request.getImagesUrls().stream()
                .map(url ->{
                    Image image = new Image();
                    image.setImageUrl(url);
                    image.setProduct(productSaved);
                    return image;
                }).collect(Collectors.toList());

        imageRepository.saveAll(images);

        return ProductResponse.builder()
                .success(true)
                .message("Producto creado con exito")
                .currentProduct(ProductMapper.toDto(productSaved))
                .build();
    }

    public ProductResponse deleteProduct(final long id) throws ApiException {

        productRepository
                .findById(id)
                .orElseThrow(DELETE_PRODUCT_ERROR_NOT_FOUND::toException);
        productRepository.deleteById(id);

        return ProductResponse.builder()
                .success(true)
                .message("Producto Eliminado con exito")
                .build();
    }

    public ProductResponse updateProduct(long product_id, long category_id) throws ApiException {

        Optional<Product> productBBDD = productRepository.findById(product_id);
        if(!productBBDD.isPresent()) throw UPDATE_PRODUCT_ERROR_NOT_FOUND.toException();

        Product product = mapper.convertValue(productBBDD, Product.class);
        product.setCategory(categoryService.findById(category_id));
        List<Image> images = productBBDD.get().getImages();
        product.setImages(images);

        final Product productSaved = productRepository.save(product);

        return ProductResponse.builder()
                .success(true)
                .message("Producto actualizado con exito")
                .currentProduct(ProductMapper.toDto(productSaved))
                .build();

    }

    public ProductResponse getProducts(final  ProductRequest request) {

        final Pageable pageable = request.getCriteria().getPageableDto().toPageable();
        final Page<Product> pageProduct = productRepository
               .findAll(getSpecification(request.getCriteria()), pageable);
        final List<ProductDto> all = pageProduct.getContent().stream()
                .map(producto -> ProductMapper.toDto(producto))
                .collect(Collectors.toList());

        return ProductResponse.builder()
                .success(true)
                .products(all)
                .pageable(PageableDto.fromPageable(pageProduct))
                .build();
    }

    private Specification<Product> getSpecification(final ProductCriteria criteria) {
        return (Root<Product> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();

            if (criteria.getName() != null) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("name")), "%" + criteria.getName() + "%"));
            }
            if (criteria.getCategory() != null) {
                Join<Product, Category> categoriaJoin = root.join("category", JoinType.INNER);
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(categoriaJoin.get("name")), "%" + criteria.getCategory().toLowerCase() + "%"));
            }
            if (criteria.getBrand() != null) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("brand")), "%" + criteria.getBrand().toLowerCase() + "%"));
            }

            if (criteria.getStartDate() != null && criteria.getEndDate() != null) {
                Subquery<Long> overlappingReservationsSubquery = query.subquery(Long.class);
                Root<Reservation> reservationRoot = overlappingReservationsSubquery.from(Reservation.class);
                overlappingReservationsSubquery.select(reservationRoot.get("product").get("id"));
                overlappingReservationsSubquery.where(
                        criteriaBuilder.equal(reservationRoot.get("product").get("id"), root.get("id")),
                        criteriaBuilder.or(
                                criteriaBuilder.and(
                                        criteriaBuilder.lessThanOrEqualTo(reservationRoot.get("startDate"), criteria.getEndDate()),
                                        criteriaBuilder.greaterThanOrEqualTo(reservationRoot.get("endDate"), criteria.getStartDate())
                                ),
                                criteriaBuilder.and(
                                        criteriaBuilder.lessThanOrEqualTo(reservationRoot.get("endDate"), criteria.getEndDate()),
                                        criteriaBuilder.greaterThanOrEqualTo(reservationRoot.get("endDate"), criteria.getStartDate())
                                )
                        )
                );

                predicates.add(criteriaBuilder.not(root.get("id").in(overlappingReservationsSubquery)));
            }

            if (criteria.getModel() != null) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("model")), "%" + criteria.getModel().toLowerCase() + "%"));
            }
            if (criteria.getDescription() != null) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("description")), "%" + criteria.getDescription().toLowerCase() + "%"));
            }
            if (criteria.getFuel() != null) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("fuel")), "%" + criteria.getFuel().toLowerCase() + "%"));
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }

    public ProductResponse getRandomProducts(final String username) {
        final String id = Optional.ofNullable(username).orElse(UUID.randomUUID().toString());
        final List<Product> product = productRepository.findAll();
        return ProductResponse.builder()
                .products(RamdonProductUtil.getRamdomProduct(product, id))
                .id(id)
                .success(true)
                .build();
    }

    @Override
    public ProductResponse getProductById(long id) throws ApiException {
        return ProductResponse.builder()
                .currentProduct(productRepository.findById(id).map(ProductMapper::toDto).orElseThrow(PRODUCT_ERROR_NOT_FOUND::toException))
                .id( String.valueOf(id))
                .success(true)
                .build();
    }

    @Override
    public ProductCategoryResponse getProductsByCategory(Long id) throws ApiException {
        List<Product> productsByCategory = productRepository.findByCategory(id);
        if(productsByCategory.isEmpty()) throw PRODUCT_FOR_CATEGORY_NOT_FOUND.toException();

        List<ProductDtoCategory> productDtos = IntStream.range(0, productsByCategory.size())
                .boxed()
                .map(i -> {
                    Product product = productsByCategory.get(i);
                    ProductDtoCategory productoDto = ProductMapper.toDtoCategory(product);
                    productoDto.setCategory_name(product.getCategory().getName());
                    return productoDto;
                })
                .collect(Collectors.toList());


        return ProductCategoryResponse.builder()
                .success(true)
                .products(productDtos)
                .build();
    }

    @Override
    public ProductCategoryResponse getProductsByCategories(List<Long> categoryIds) throws ApiException {
        List<Product> productsByCategory = productRepository.findByCategories(categoryIds);
        if(productsByCategory.isEmpty()) throw PRODUCT_FOR_CATEGORY_NOT_FOUND.toException();

        List<ProductDtoCategory> productDtos = IntStream.range(0, productsByCategory.size())
                .boxed()
                .map(i -> {
                    Product product = productsByCategory.get(i);
                    ProductDtoCategory productDto = ProductMapper.toDtoCategory(product);
                    productDto.setCategory_name(product.getCategory().getName());
                    return productDto;
                })
                .collect(Collectors.toList());

        return ProductCategoryResponse.builder()
                .success(true)
                .products(productDtos)
                .build();
    }

    @Override
    public ProductBrandsResponse getProductBrands() throws ApiException {
        List<String> brands = productRepository.getProductBrands();
        if(brands.isEmpty()) throw PRODUCT_BRANDS_NOT_FOUND.toException();

        return ProductBrandsResponse.builder()
                .success(true)
                .brands(brands)
                .build();
    }


}
