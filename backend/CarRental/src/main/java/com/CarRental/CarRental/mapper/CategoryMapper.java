package com.CarRental.CarRental.mapper;

import com.CarRental.CarRental.dto.category.CategoryRequest;
import com.CarRental.CarRental.model.Category;
import lombok.experimental.UtilityClass;

@UtilityClass
public class CategoryMapper {

    public static Category toEntity(CategoryRequest request){
        Category category = new Category();
        category.setId(request.getId());
        category.setName(request.getName());
        category.setDescription(request.getDescription());
        category.setImageUrl(request.getImageUrl());
        //categoria.setImageEncode(Base64.getUrlEncoder().encode(request.getImageEncode().getBytes(StandardCharsets.UTF_8)));


        return category;
    }

    public static CategoryRequest toDto(Category entity){
        return CategoryRequest.builder()
                .id(entity.getId())
                .name(entity.getName())
                .description(entity.getDescription())
                .imageUrl(entity.getImageUrl())
                //.imageEncode(new String( Base64.getUrlDecoder().decode(entity.getImageEncode()), StandardCharsets.UTF_8))
                //.products(entity.getProductos().stream().map(ProductMapper::toDto).toList())
                .build();
    }


}
