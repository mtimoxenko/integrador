package com.CarRental.CarRental.service.interfaces;

import com.CarRental.CarRental.dto.product.ProductDtoCreate;
import com.CarRental.CarRental.dto.product.request.ProductRequest;
import com.CarRental.CarRental.dto.product.response.ProductBrandsResponse;
import com.CarRental.CarRental.dto.product.response.ProductCategoryResponse;
import com.CarRental.CarRental.dto.product.response.ProductResponse;
import com.CarRental.CarRental.exception.ApiException;

import java.util.List;

public interface IProductService {
    ProductResponse createProduct(final ProductDtoCreate request) throws ApiException;
    ProductResponse deleteProduct(long id) throws ApiException;
    ProductResponse updateProduct(long product_id, long category_id) throws ApiException;
    ProductResponse getProducts(final  ProductRequest request);
    ProductResponse getRandomProducts(String username);
    ProductResponse getProductById(long id) throws ApiException;
    ProductCategoryResponse getProductsByCategory (Long category_id) throws ApiException;
    ProductCategoryResponse getProductsByCategories (List<Long> categoryIds) throws ApiException;
    ProductBrandsResponse getProductBrands ()throws ApiException;
}
