package com.CarRental.CarRental.util;

import lombok.experimental.UtilityClass;


@UtilityClass
public class GeneralConstansSwagger {
    //PRODUCTOS -------------------------------------------------------------------------------------
    public static final String RESPONSE_ERROR_400_ALREADY_EXIST_PRODUCT= "{\n" +
            "    \"error\": \"already_exist\",\n" +
            "    \"message\": \"Product ya se encuentra regitrado en la base de datos\",\n" +
            "    \"status\": 400,\n" +
            "    \"causes\": []\n" +
            "}";

    public static final String RESPONSE_ERROR_404_DELETE_PRODUCT_ERROR_NOT_FOUND= "{\n" +
            "    \"error\": \"delete_Error_not_found\",\n" +
            "    \"message\": \"No se puede eliminar un producto que no existe\",\n" +
            "    \"status\": 404,\n" +
            "    \"causes\": []\n" +
            "}";

    public static final String RESPONSE_ERROR_404_UPDATE_PRODUCT_ERROR_NOT_FOUND= "{\n" +
            "    \"error\": \"Update_error_not_found\",\n" +
            "    \"message\": \"No se puede actualizar un producto que no existe\",\n" +
            "    \"status\": 404,\n" +
            "    \"causes\": []\n" +
            "}";

    public static final String RESPONSE_ERROR_404_PRODUCT_ERROR_NOT_FOUND= "{\n" +
            "    \"error\": \"Error_not_found\",\n" +
            "    \"message\": \"No se encontro el producto\",\n" +
            "    \"status\": 404,\n" +
            "    \"causes\": []\n" +
            "}";

    public static final String RESPONSE_ERROR_400_PRODUCT_FOR_CATEGORY_NOT_FOUND= "{\n" +
            "    \"error\": \"products_for_category_not_found\",\n" +
            "    \"message\": \"No se encontraron productos por esta categoría\",\n" +
            "    \"status\": 400,\n" +
            "    \"causes\": []\n" +
            "}";



    //USUARIOS -------------------------------------------------------------------------------------
    public static final String RESPONSE_ERROR_401_BAD_CREDENTIAL = "{\n" +
            "    \"error\": \"bad_credential\",\n" +
            "    \"message\": \"Credenciales invalidas: User y/o Password invalid\",\n" +
            "    \"status\": 401,\n" +
            "    \"causes\": []\n" +
            "}";

    public static final String RESPONSE_ERROR_400_REQUEST_NULL = "{\n" +
            "    \"error\": \"request_null\",\n" +
            "    \"message\": \"El request no puede ser nulo\",\n" +
            "    \"status\": 400,\n" +
            "    \"causes\": []\n" +
            "}";

    public static final String RESPONSE_ERROR_400_EMAIL_INVALID= "{\n" +
            "    \"error\": \"email_invalid\",\n" +
            "    \"message\": \"El email no puede ser nulo o no es valido\",\n" +
            "    \"status\": 400,\n" +
            "    \"causes\": []\n" +
            "}";

    public static final String RESPONSE_ERROR_400_PASSWORD_INVALID= "{\n" +
            "    \"error\": \"password_invalid\",\n" +
            "    \"message\": \"El password es invalido. \nMínimo 8 caracteres de longitud, \nal menos una letra mayúscula, \nuna letra minúscula, \nun número y un carácter especial entre @#$%^&+=!\",\n" +
            "    \"status\": 400,\n" +
            "    \"causes\": []\n" +
            "}";

    public static final String RESPONSE_ERROR_400_NAME_INVALID= "{\n" +
            "    \"error\": \"name_invalid\",\n" +
            "    \"message\": \"El nombre no puede ser nulo, o vacio\",\n" +
            "    \"status\": 400,\n" +
            "    \"causes\": []\n" +
            "}";

    public static final String RESPONSE_ERROR_400_ALREADY_USER= "{\n" +
            "    \"error\": \"already_user\",\n" +
            "    \"message\": \"El email ****@***.com ya se encuentra registrado a otro usuario\",\n" +
            "    \"status\": 400,\n" +
            "    \"causes\": []\n" +
            "}";

    public static final String RESPONSE_ERROR_400_NOT_FOUND_USER= "{\n" +
            "    \"error\": \"notFoundEmail\",\n" +
            "    \"message\": \"El email ****@***.com no se encuentra registrado\",\n" +
            "    \"status\": 400,\n" +
            "    \"causes\": []\n" +
            "}";

    //CATEGORÍAS -----------------------------------------------------------------
    public static final String RESPONSE_ERROR_400_ALREADY_EXIST_CATEGORY= "{\n" +
            "    \"error\": \"already_exist\",\n" +
            "    \"message\": \"La categoría ya se encuentra registrada en la base de datos\",\n" +
            "    \"status\": 400,\n" +
            "    \"causes\": []\n" +
            "}";

    public static final String RESPONSE_ERROR_404_DELETE_CATEGORY_ERROR_NOT_FOUND= "{\n" +
            "    \"error\": \"delete_Error_not_found\",\n" +
            "    \"message\": \"No se puede eliminar una categoría que no existe\",\n" +
            "    \"status\": 404,\n" +
            "    \"causes\": []\n" +
            "}";

    //FAVORITOS -------------------------------------------------------------------
    public static final String RESPONSE_ERROR_400_USER_OR_PRODUCT_NOT_FOUND= "{\n" +
            "    \"error\": \"product_or_user_Error_not_found\",\n" +
            "    \"message\": \"Usuario o producto no encontrado\",\n" +
            "    \"status\": 400,\n" +
            "    \"causes\": []\n" +
            "}";

    //RESERVA--------------------------------------------------------------------------
    public static final String RESPONSE_ERROR_400_PRODUCT_NOT_AVILABLE= "{\n" +
            "    \"error\": \"Product_not_avilable\",\n" +
            "    \"message\": \"El producto no está disponible para está fecha\",\n" +
            "    \"status\": 400,\n" +
            "    \"causes\": []\n" +
            "}";

    public static final String RESPONSE_ERROR_400_PRODUCT_DOES_NOT_EXIST= "{\n" +
            "    \"error\": \"Product_does_not_exist\",\n" +
            "    \"message\": \"El producto que desea reserva no existe\",\n" +
            "    \"status\": 400,\n" +
            "    \"causes\": []\n" +
            "}";

    public static final String RESPONSE_ERROR_400_USER_DOES_NOT_EXIST= "{\n" +
            "    \"error\": \"User_does_not_exist\",\n" +
            "    \"message\": \"El User que desea hacer la reserva no se encuentra registrado\",\n" +
            "    \"status\": 400,\n" +
            "    \"causes\": []\n" +
            "}";

    public static final String RESPONSE_ERROR_400_RESERVATION_TOO_SHORT= "{\n" +
            "    \"error\": \"Reservation_too_short\",\n" +
            "    \"message\": \"La reserva no cumple con el mínimo de días\",\n" +
            "    \"status\": 400,\n" +
            "    \"causes\": []\n" +
            "}";

    public static final String RESPONSE_ERROR_400_RESERVATION_DATES_INVALID= "{\n" +
            "    \"error\": \"Reservation_dates_invalid\",\n" +
            "    \"message\": \"La fecha de inicio debe ser menor a la fecha de fin\",\n" +
            "    \"status\": 400,\n" +
            "    \"causes\": []\n" +
            "}";

    public static final String RESPONSE_ERROR_404_DELETE_RESERVATION_NOT_FOUND= "{\n" +
            "    \"error\": \"Delete_reservation_not_found\",\n" +
            "    \"message\": \"La reserva que desea eliminar no existe\",\n" +
            "    \"status\": 400,\n" +
            "    \"causes\": []\n" +
            "}";
}