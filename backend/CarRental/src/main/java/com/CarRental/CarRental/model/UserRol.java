package com.CarRental.CarRental.model;

public enum UserRol {
    CLIENTE,
    AGENTE,
    ADMINISTRADOR,
    SUPER_ADMINISTRADOR
}
