package com.CarRental.CarRental.repository;


import com.CarRental.CarRental.model.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;


@Repository
public interface IReservationRepository extends JpaRepository<Reservation, Long> {
    @Query(value ="SELECT COUNT(*) FROM reservations WHERE product_id = :id AND :startDate <= end_date AND :endDate >= start_date;", nativeQuery = true)
    Long productReservation(@Param("id") Long id, @Param("startDate") LocalDate startDate, @Param("endDate") LocalDate endDate);

    List<Reservation> findByUserId(Long userId);

}
