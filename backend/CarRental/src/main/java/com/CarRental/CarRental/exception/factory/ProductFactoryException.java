package com.CarRental.CarRental.exception.factory;

import com.CarRental.CarRental.exception.dto.ApiErrorDto;
import lombok.experimental.UtilityClass;

import static jakarta.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static jakarta.servlet.http.HttpServletResponse.SC_NOT_FOUND;

@UtilityClass
public class ProductFactoryException {
    public static final ApiErrorDto ALREADY_EXIST_PRODUCT =
            new ApiErrorDto("already_exist", "Product ya se encuentra registrado en la base de datos", SC_BAD_REQUEST);
    public static final ApiErrorDto DELETE_PRODUCT_ERROR_NOT_FOUND =
            new ApiErrorDto("delete_Error_not_found", "No se puede eliminar un producto que no existe", SC_NOT_FOUND);

    public static final ApiErrorDto PRODUCT_ERROR_NOT_FOUND =
            new ApiErrorDto("Error_not_found", "No se encontro el producto", SC_NOT_FOUND);
    public static final ApiErrorDto UPDATE_PRODUCT_ERROR_NOT_FOUND =
            new ApiErrorDto("Update_error_not_found", "No se puede actualizar un producto que no existe", SC_NOT_FOUND);

    public static final ApiErrorDto PRODUCT_FOR_CATEGORY_NOT_FOUND =
            new ApiErrorDto("products_for_category_not_found", "No se encontraron productos por esta categoría", SC_BAD_REQUEST);

    public static final ApiErrorDto PRODUCT_BRANDS_NOT_FOUND =
            new ApiErrorDto("Product_brands_not_found", "No se encontraron marcas asociadas a los productos", SC_BAD_REQUEST);
}
