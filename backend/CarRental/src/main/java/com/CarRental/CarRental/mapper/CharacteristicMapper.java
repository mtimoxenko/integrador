package com.CarRental.CarRental.mapper;

import com.CarRental.CarRental.dto.characteristic.CharacteristicDto;
import com.CarRental.CarRental.model.Characteristic;
import lombok.experimental.UtilityClass;

@UtilityClass
public class CharacteristicMapper {

    public static CharacteristicDto toDto(final Characteristic characteristic){
        return CharacteristicDto.builder()
                .id(characteristic.getId())
                .icon(characteristic.getIconName())
                .name(characteristic.getName())
                .build();
    }

    public static Characteristic toEntity(final CharacteristicDto request){
        Characteristic characteristic = new Characteristic();
        characteristic.setId(request.getId());
        characteristic.setName(request.getName());
        characteristic.setIconName(request.getIcon());
        return characteristic;
    }
}
