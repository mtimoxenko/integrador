package com.CarRental.CarRental.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "users") // Especifica el nombre de la tabla en la base de datos
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Schema(description = "Identificador único", example = "1L")
    private Long id;

    @Column(nullable = false)
    @Schema(description = "Dirección de correo electrónico del User.", example = "john.doe@example.com")
    private String email;

    @Schema(description = "Nombre del User.", example = "John")
    private String name;

    @Schema(description = "Apellido del User.", example = "Doe")
    private String surname;

    @Column(nullable = false)
    @Schema(description = "Contraseña de la cuenta de usuario.", example = "password123", accessMode = Schema.AccessMode.WRITE_ONLY)
    private String password;

    @Enumerated(EnumType.STRING)
    @Column(nullable = true)
    @Schema(description = "Rol del usuario", example = "CLIENTE")
    private UserRol role;

    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
    @JsonIgnore
    private List<Reservation> reservations = new ArrayList<>();
}
