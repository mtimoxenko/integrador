package com.CarRental.CarRental.service.interfaces;

import com.CarRental.CarRental.dto.product.response.Response;
import com.CarRental.CarRental.dto.user.LoginUserRequest;
import com.CarRental.CarRental.dto.user.LoginUserResponse;
import com.CarRental.CarRental.dto.user.RegisterRequest;
import com.CarRental.CarRental.dto.user.RegisterResponse;
import com.CarRental.CarRental.exception.ApiException;
import com.CarRental.CarRental.model.UserRol;

import java.util.List;

public interface IUserService {
    LoginUserResponse login(LoginUserRequest loginUserRequest) throws ApiException;
    RegisterResponse registerUser(RegisterRequest request)throws ApiException;
    RegisterResponse resendEmail(RegisterRequest request) throws ApiException;
    Response logout(String jwt)throws ApiException;
    List<RegisterRequest> getAllUsers();
    Response changeRole(List<String> emails, UserRol role);
}
