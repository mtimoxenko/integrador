package com.CarRental.CarRental.dto.product.request;

import com.CarRental.CarRental.dto.PageableDto;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.time.LocalDate;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductCriteria {
    //Todo deben definir los campos de busqueda que enviaran desde el forntend
    private String name;
    private String category;
    private String brand;
    private String model;
    private String description;
    private String fuel;
    private int numPassengers;
    private int numBags;
    private int numDoors;
    private Boolean isAutomatic;

    //este es para mandar los campos de paginacioon y ordenamiento
    private PageableDto pageableDto;

    private LocalDate startDate;
    private LocalDate endDate;
}
