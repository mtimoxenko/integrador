package com.CarRental.CarRental.controller;

import com.CarRental.CarRental.dto.product.response.Response;
import com.CarRental.CarRental.dto.user.RegisterRequest;
import com.CarRental.CarRental.model.UserRol;
import com.CarRental.CarRental.service.interfaces.IUserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@Slf4j
@Tag(name = "Usuario admin", description = "Endpoints para la Administración del sistema")
@RestController
@RequestMapping("/admin")

public class AdminController {
    private final IUserService userService;

    public AdminController(IUserService userService) {
        this.userService = userService;
    }

    @Operation(summary = "Cambio de rol de un usuario", description = "Método encargado de cambiar el rol de un usuario determinado")
    @PreAuthorize("hasRole('SUPER_ADMINISTRADOR')")
    @PutMapping("/changeTo/role/{role}")
    public void changeRole(@RequestBody String email, @PathVariable UserRol role){
        userService.changeRole(Collections.singletonList(email), role);
    }

    @Operation(summary = "Cambio de rol de una lista de usuarios", description = "Método encargado de cambiar el rol de un grupo de usuarios determinado")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Cambio de rol exitoso",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Response.class)))
    })
    @PreAuthorize("hasRole('SUPER_ADMINISTRADOR')")
    @PutMapping("/changeTo/roles/{role}")
    public Response changeRoles(@RequestBody List<String> emails, @PathVariable UserRol role){
        return userService.changeRole(emails, role);
    }

    @Operation(summary = "Obtener el listado de usuarios", description = "Método encargado de obtener los usuarios registrados")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Usuarios registrados:",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Response.class)))
    })
    @PreAuthorize("hasRole('SUPER_ADMINISTRADOR') || hasRole('ADMINISTRADOR')")
    @GetMapping("/getAllUsers")
    public List<RegisterRequest>  getAllUsers(){
        return userService.getAllUsers();
    }
}