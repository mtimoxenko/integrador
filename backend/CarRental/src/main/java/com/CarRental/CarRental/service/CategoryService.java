package com.CarRental.CarRental.service;

import com.CarRental.CarRental.dto.category.CategoryRequest;
import com.CarRental.CarRental.dto.category.CategoryResponse;
import com.CarRental.CarRental.dto.product.ProductDto;
import com.CarRental.CarRental.exception.ApiException;
import com.CarRental.CarRental.mapper.CategoryMapper;
import com.CarRental.CarRental.model.Category;
import com.CarRental.CarRental.repository.ICategoryRepository;
import com.CarRental.CarRental.service.interfaces.ICategoryService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.CarRental.CarRental.exception.factory.CategoryFactoryException.*;

@Service
public class CategoryService implements ICategoryService {

    private final ICategoryRepository categoriaRepository;
    private final ObjectMapper mapper;


    public CategoryService(ICategoryRepository categoriaRepository, ObjectMapper mapper) {
        this.categoriaRepository = categoriaRepository;
        this.mapper = mapper;
    }


    @Override
    public CategoryResponse createCategoty(CategoryRequest request) throws ApiException {

        //Validación por nombre, si existe se genera una excepcion
        String name = request.getName().toLowerCase().trim().replace("-","");
        name = name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
        Optional<Category> categoryDto = categoriaRepository.findByName(name);
        if(categoryDto.isPresent()) throw ALREADY_EXIST_CATEGORY.toException();

        Category categoryReceived = CategoryMapper.toEntity(request);
        categoryReceived.setName(name);
        Category categorySaved = categoriaRepository.save(categoryReceived);
        request.setId(categorySaved.getId());

        return CategoryResponse.builder()
                .success(true)
                .message("Categoría creada exitosamente")
                .build();
    }

    @Override
    public List<CategoryRequest> getCategories() {
        List<CategoryRequest> categories = new ArrayList<>();
        List<Category> categoriesBBDD = categoriaRepository.findAll();

        for (Category category : categoriesBBDD){
            categories.add(CategoryMapper.toDto(category));
        }

        categories.forEach(category -> category.setProducts(mapper.convertValue(categoriesBBDD.
                get(categories.indexOf(category)).getProducts(), new TypeReference<List<ProductDto>>() {})));

        return categories;
    }

    @Override
    public CategoryResponse deleteCategory(Long id) throws ApiException {
        //Validando si existe
        Category category = categoriaRepository.findById(id)
                .orElseThrow(DELETE_CATEGORY_ERROR_NOT_FOUND::toException);

        categoriaRepository.deleteById(id);

        return CategoryResponse.builder()
                .success(true)
                .message("Categoría " + category.getName() + " eliminada exitosamente")
                .build();
    }

    @Override
    public Category findById(Long id) throws ApiException {
        return categoriaRepository.findById(id)
                .orElseThrow(CATEGORY_NOT_FOUND::toException);
    }
}
