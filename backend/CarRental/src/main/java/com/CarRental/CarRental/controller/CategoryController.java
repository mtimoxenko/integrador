package com.CarRental.CarRental.controller;

import com.CarRental.CarRental.dto.category.CategoryRequest;
import com.CarRental.CarRental.dto.category.CategoryResponse;
import com.CarRental.CarRental.dto.image.ImageRequest;
import com.CarRental.CarRental.exception.ApiException;
import com.CarRental.CarRental.exception.dto.ApiErrorDto;
import com.CarRental.CarRental.model.Image;
import com.CarRental.CarRental.model.Product;
import com.CarRental.CarRental.service.CategoryService;
import com.CarRental.CarRental.service.FileStorageService;
import com.CarRental.CarRental.service.S3FileStorage;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.log4j.Logger;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import com.CarRental.CarRental.exception.ApiException;
import com.CarRental.CarRental.model.Category;

import java.util.List;
import java.util.stream.Collectors;

import static com.CarRental.CarRental.util.GeneralConstansSwagger.RESPONSE_ERROR_400_ALREADY_EXIST_CATEGORY;
import static com.CarRental.CarRental.util.GeneralConstansSwagger.RESPONSE_ERROR_404_DELETE_CATEGORY_ERROR_NOT_FOUND;

@RestController
@Tag(name = "Categoria", description = "Endpoints para la gestión de Categorias")
@RequestMapping(value = "/category")

public class CategoryController {
    private final CategoryService categoryService;
    private final S3FileStorage s3FileStorage;
    private final Logger LOGGER = Logger.getLogger(CategoryController.class);

    public CategoryController(CategoryService categoryService, S3FileStorage s3FileStorage) {
        this.categoryService = categoryService;
        this.s3FileStorage = s3FileStorage;
    }

    @Operation(summary = "Listar categorías", description = "Método encargado de listar todas las categorías registradas")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "success", content = @Content(examples = {
                    @ExampleObject(name = "creacion exitosa de producto", value =
                            "[\n" +
                                    "    {\n" +
                                    "        \"id\": 1,\n" +
                                    "        \"name\": \"Categoría 1\",\n" +
                                    "        \"description\": \"Descripción categoría 1\",\n" +
                                    "        \"imageUrl\": \"https://ImagenCategoría1.jpg\",\n" +
                                    "        \"products\": [...]\n" +
                                    "    {\n" +
                                    "        [categorias...]\n" +
                                    "]" )
            }))
    })
    @GetMapping("/public")
    public List<CategoryRequest> getCategories(){
        return categoryService.getCategories();
    }
    @Operation(summary = "Crear categoría", description = "Método encargado de crear una nueva categoría")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "success", content = @Content(examples = {
                    @ExampleObject(name = "creacion exitosa de producto", value =
                            "{\n" +
                                    "    \"success\": true,\n" +
                                    "    \"message\": \"Categoría creada exitosamente\"\n" +
                                    "}"
                    )
            })),
            @ApiResponse(responseCode = "400", description = "Bad request", content = @Content(schema = @Schema(implementation = ApiErrorDto.class), examples = {
                    @ExampleObject(name = "already_exist", value = RESPONSE_ERROR_400_ALREADY_EXIST_CATEGORY)
            }))
    })
    @PostMapping
    @PreAuthorize("hasRole('ADMINISTRADOR') || hasRole('SUPER_ADMINISTRADOR')")
    public CategoryResponse createCategory(
            @RequestParam String name,
            @RequestParam String description,
            @RequestParam MultipartFile image
    ) throws ApiException {
        String fileName = s3FileStorage.storeFile(image); // store files
        String imageUrl = s3FileStorage.getSignedUrl(fileName);

        CategoryRequest categoryRequest = new CategoryRequest(null, name, description, imageUrl,null);

        return categoryService.createCategoty(categoryRequest);
    }


    @Operation(summary = "Borrar categoría", description = "Método encargado de borrar una categoría determinada")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "success", content = @Content(examples = {
                    @ExampleObject(name = "Eliminación exitosa de producto", value =
                            "{\n" +
                                    "    \"success\": true,\n" +
                                    "    \"message\": \"Categoría eliminada exitosamente\"\n" +
                                    "}"
                    )
            })),
            @ApiResponse(responseCode = "400", description = "Not found", content = @Content(schema = @Schema(implementation = ApiErrorDto.class), examples = {
                    @ExampleObject(name = "delete_Error_not_found", value = RESPONSE_ERROR_404_DELETE_CATEGORY_ERROR_NOT_FOUND)
            }))
    })
    @DeleteMapping(value = "/{id}")
    @PreAuthorize("hasRole('ADMINISTRADOR') || hasRole('SUPER_ADMINISTRADOR')")
    public CategoryResponse deleteCategory(@PathVariable Long id) throws ApiException{
        Category category = categoryService.findById(id);
        if (category != null && category.getImageUrl() != null && !category.getImageUrl().isEmpty()) {
            try {
                String fileName = category.getImageUrl();
                s3FileStorage.deleteFile(fileName);

                //Images from products
                List<Product> products = category.getProducts();
                List<String> imageUrls = products.stream()
                        .flatMap(product -> product.getImages().stream())
                        .map(Image::getImageUrl)
                        .collect(Collectors.toList());

                for (String imageUrl : imageUrls) {
                    try {
                        s3FileStorage.deleteFile(imageUrl);
                    } catch (Exception e) {
                        LOGGER.error("Failed to delete image from S3 for URL:" + imageUrl, e);
                    }
                }
            } catch (Exception e) {
                LOGGER.error("Failed to delete image from S3 for category ID: " + id, e);
            }
        }
        return categoryService.deleteCategory(id);
    }


}
