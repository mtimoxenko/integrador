package com.CarRental.CarRental.dto.reservation;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class ReservationDto {
    private Long product_id;
    private Long user_id;
    private LocalDate startDate;
    private LocalDate endDate;
}
