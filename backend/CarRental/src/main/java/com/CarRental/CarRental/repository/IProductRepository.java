package com.CarRental.CarRental.repository;

import com.CarRental.CarRental.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IProductRepository extends JpaRepository<Product, Long> {
    Optional<Product> findByName(String name);
    Optional<Product> findById(Long id);
    Page<Product> findAll(Specification<Product> specification, Pageable pageable);

    @Query(value = "SELECT * FROM `products` WHERE `category_id` =:id", nativeQuery = true) //Para MySQL

    //@Query(value = "select * from products where category_id =:id", nativeQuery = true)
    List<Product> findByCategory(@Param("id") Long id);
    @Query(value = "SELECT * FROM products WHERE category_id IN :categoryIds", nativeQuery = true)
    List<Product> findByCategories(@Param("categoryIds") List<Long> categoryIds);

    @Query(value = "select distinct brand from products", nativeQuery = true)
    List<String> getProductBrands();

}
