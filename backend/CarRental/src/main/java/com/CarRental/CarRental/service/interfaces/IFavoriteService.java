package com.CarRental.CarRental.service.interfaces;

import com.CarRental.CarRental.dto.product.FavoriteDto;
import com.CarRental.CarRental.dto.product.response.Response;
import com.CarRental.CarRental.exception.ApiException;

import java.util.List;

public interface IFavoriteService {
    List<FavoriteDto> getFavoritesByUser(Long id);
    Response createFavorite(Long userId, Long productId) throws ApiException;
    Response deleteFavorite(Long userId, Long productId);
}
