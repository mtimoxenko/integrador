package com.CarRental.CarRental.dto.product.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Response {
    private boolean success;
    private String message;

}
