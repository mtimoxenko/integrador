package com.CarRental.CarRental.listener;

import com.CarRental.CarRental.dto.EmailEventDto;
import com.CarRental.CarRental.service.interfaces.IEmailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class EmailEventListener implements ApplicationListener<EmailEventDto> {
    private final IEmailService iEmailService;

    public EmailEventListener(IEmailService iEmailService) {
        this.iEmailService = iEmailService;
    }

    @Override
    public void onApplicationEvent(final EmailEventDto event) {
        log.debug("recibiendo mensaje {}" ,event);
        this.iEmailService.sendEmail(event.getTo(), event.getSubject(), event.getBody());
    }
}
