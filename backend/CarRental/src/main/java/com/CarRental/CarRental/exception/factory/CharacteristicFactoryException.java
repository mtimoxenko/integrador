package com.CarRental.CarRental.exception.factory;

import com.CarRental.CarRental.exception.dto.ApiErrorDto;
import lombok.experimental.UtilityClass;

import static jakarta.servlet.http.HttpServletResponse.SC_BAD_REQUEST;

@UtilityClass
public class CharacteristicFactoryException {

    public static final ApiErrorDto CHARACTERISTIC_NOT_FOUND =
            new ApiErrorDto("characteristic_not_found", "La Caracteristica solicitada no se encuentra registrada", SC_BAD_REQUEST);
    public static final ApiErrorDto CHARACTERISTIC_IN_USE =
            new ApiErrorDto("characteristic_in_use", "La Caracteristica contiene productos asociados", SC_BAD_REQUEST);

    public static final ApiErrorDto ALREADY_EXIST_CHARACTERISTIC =
            new ApiErrorDto("already_exist", "Ya se encuentra registrada una caracteristica con el mismo nombre o icono", SC_BAD_REQUEST);
}
