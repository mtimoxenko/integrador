package com.CarRental.CarRental.dto.user;

import com.CarRental.CarRental.model.UserRol;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class LoginUserResponse {
    private Long id;
    private boolean success;
    private String userName;
    private String lastName;
    private String email;
    private UserRol rol;
    private String token;
    private String message;
}
