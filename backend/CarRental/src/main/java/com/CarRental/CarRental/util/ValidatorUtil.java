package com.CarRental.CarRental.util;

import com.CarRental.CarRental.dto.user.RegisterRequest;
import com.CarRental.CarRental.exception.ApiException;
import lombok.experimental.UtilityClass;

import java.util.Optional;
import java.util.regex.Pattern;

import static com.CarRental.CarRental.exception.factory.UserFactoryException.*;

@UtilityClass
public class ValidatorUtil {
    private static final Pattern EMAIL_VALID = Pattern.compile("^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}(\\.[A-Za-z]{2,})?$");
    private static final Pattern PASSWORD_VALID = Pattern.compile("^(?=.*\\d).{8,}$");
    public static void validateRegister(final RegisterRequest request) throws ApiException {
        validateEmail(request);
        Optional.ofNullable(request).orElseThrow(REQUEST_NULL::toException);
        Optional.ofNullable(request.getEmail())
                .filter(email->EMAIL_VALID.matcher(email).matches()).orElseThrow(EMAIL_INVALID::toException);
        Optional.ofNullable(request.getPassword())
                .filter(password->PASSWORD_VALID.matcher(password).matches()).orElseThrow(PASSWORD_INVALID::toException);
        Optional.ofNullable(request.getName()).map(String::trim)
                .filter(name-> !name.isEmpty()&& !name.isBlank())
                .orElseThrow(NAME_INVALID::toException);
        Optional.ofNullable(request.getLastName()).map(String::trim)
                .filter(lastname-> !lastname.isEmpty()&& !lastname.isBlank())
                .orElseThrow(LASTNAME_INVALID::toException);
    }
    public static void validateEmail(final RegisterRequest request) throws ApiException {

        Optional.ofNullable(request).orElseThrow(REQUEST_NULL::toException);
        Optional.ofNullable(request.getEmail())
                .filter(email -> EMAIL_VALID.matcher(email).matches()).orElseThrow(EMAIL_INVALID::toException);
    }

}
