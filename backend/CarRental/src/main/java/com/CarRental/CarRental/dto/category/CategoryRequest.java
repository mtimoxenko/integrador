package com.CarRental.CarRental.dto.category;

import com.CarRental.CarRental.dto.product.ProductDto;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class CategoryRequest {
    private Long id;
    private String name;
    private String description;
    private String imageUrl;
    private List<ProductDto> products;
}
