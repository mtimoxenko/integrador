package com.CarRental.CarRental.mapper;

import com.CarRental.CarRental.dto.image.ImageRequest;
import com.CarRental.CarRental.dto.product.ProductDto;
import com.CarRental.CarRental.dto.product.ProductDtoCreate;
import com.CarRental.CarRental.dto.product.ProductDtoCategory;
import com.CarRental.CarRental.model.Image;
import com.CarRental.CarRental.model.Product;
import lombok.experimental.UtilityClass;

@UtilityClass
public class ProductMapper {

    public static Product toEntity(ProductDto dto) {

        Product product = new Product();
        product.setId(dto.getId());
        product.setName(dto.getName());
        product.setCategory(CategoryMapper.toEntity(dto.getCategory()));
        product.setBrand(dto.getBrand());
        product.setModel(dto.getModel());
        product.setDescription(dto.getDescription());
        product.setFuel(dto.getFuel());
        product.setNumPassengers(dto.getNumPassengers());
        product.setNumBags(dto.getNumBags());
        product.setNumDoors(dto.getNumDoors());
        product.setIsAutomatic(dto.getIsAutomatic());
        product.setImages(dto.getImages().stream().map(imageRequest -> toImagen(imageRequest, product)).toList());

        return product;
    }

    public static Product toEntityCreate(ProductDtoCreate dto) {

        Product product = new Product();
        product.setId(dto.getId());
        product.setName(dto.getName());
        product.setBrand(dto.getBrand());
        product.setModel(dto.getModel());
        product.setDescription(dto.getDescription());
        product.setFuel(dto.getFuel());
        product.setNumPassengers(dto.getNumPassengers());
        product.setNumBags(dto.getNumBags());
        product.setNumDoors(dto.getNumDoors());
        product.setIsAutomatic(dto.getIsAutomatic());
        //product.setImages(dto.getImages().stream().map(imageRequest -> toImagen(imageRequest, product)).toList());

        return product;
    }


    private static Image toImagen(final ImageRequest imageRequest, final Product product){
       final Image image = new Image();
       //imagen.setEncodeImage(Base64.getUrlEncoder().encode(imageRequest.getEncodeImage().getBytes(StandardCharsets.UTF_8)));
        image.setImageUrl(imageRequest.getImageUrl());
        image.setId(imageRequest.getId());
        image.setProduct(product);
        return image;
    }

    public static ProductDto toDto(Product entity) {

        return ProductDto.builder().id(entity.getId())
                .name(entity.getName())
                .category(CategoryMapper.toDto(entity.getCategory()))
                .brand(entity.getBrand())
                .model(entity.getModel())
                .description(entity.getDescription())
                .fuel(entity.getFuel())
                .numPassengers(entity.getNumPassengers())
                .numBags(entity.getNumBags())
                .numDoors(entity.getNumDoors())
                .isAutomatic(entity.getIsAutomatic())
                .images(entity.getImages().stream().map(ProductMapper::toDtoImagen).toList())
                .characteristics(entity.getCaracteristicas().stream().map(CharacteristicMapper::toDto).toList())
                .build();
    }

    public static ProductDtoCategory toDtoCategory(Product entity) {

        return ProductDtoCategory.builder().id(entity.getId())
                .name(entity.getName())
                .category_id(entity.getCategory().getId())
                .brand(entity.getBrand())
                .model(entity.getModel())
                .description(entity.getDescription())
                .fuel(entity.getFuel())
                .numPassengers(entity.getNumPassengers())
                .numBags(entity.getNumBags())
                .numDoors(entity.getNumDoors())
                .isAutomatic(entity.getIsAutomatic())
                .images(entity.getImages().stream().map(ProductMapper::toDtoImagen).toList())
                .characteristics(entity.getCaracteristicas().stream().map(CharacteristicMapper::toDto).toList())
                .build();
    }

    private static ImageRequest toDtoImagen(final Image image){

        return ImageRequest.builder()
                .id(image.getId())
                //.encodeImage(new String( Base64.getUrlDecoder().decode(imagen.getEncodeImage()), StandardCharsets.UTF_8))
                .imageUrl(image.getImageUrl())
                .build();
    }
}
