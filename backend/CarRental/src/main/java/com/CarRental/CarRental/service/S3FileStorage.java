package com.CarRental.CarRental.service;

import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.GetUrlRequest;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import software.amazon.awssdk.services.s3.model.S3Exception;
import software.amazon.awssdk.services.s3.model.DeleteObjectRequest;


import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.UUID;



@Service
public class S3FileStorage {
    @Autowired
    private S3Client s3Client;

    @Value("${cloud.aws.s3.bucket}")
    private String bucketName;

    public String storeFile(MultipartFile file) {
        String originalFileName = file.getOriginalFilename();
        String fileExtension = "";
        if (originalFileName != null && !originalFileName.isEmpty()) {
            int extensionIndex = originalFileName.lastIndexOf('.');
            if (extensionIndex > 0) {
                fileExtension = originalFileName.substring(extensionIndex);
            }
            String fileName = UUID.randomUUID().toString() + fileExtension;

            try {
                // Create a temporary file
                Path tempFilePath = Files.createTempFile("upload-", fileExtension);
                file.transferTo(tempFilePath);

                // Upload the file to S3
                s3Client.putObject(PutObjectRequest.builder()
                                .bucket(bucketName)
                                .key(fileName)
                                .build(),
                        RequestBody.fromFile(tempFilePath));

                // Clean up the temporary file
                Files.delete(tempFilePath);

                // Return the S3 object key
                return fileName;
            } catch (IOException e) {
                throw new RuntimeException("Failed to store file in S3", e);
            }
        } else {
            throw new RuntimeException("Failed to store empty file.");
        }
    }

    public String getSignedUrl(String fileName) {
        try {
            // Construir la URL pública para el objeto en S3
            URL publicUrl = s3Client.utilities().getUrl(
                    GetUrlRequest.builder()
                            .bucket(bucketName)
                            .key(fileName)
                            .build());

            // Devolver la URL pública como String
            return publicUrl.toString();
        } catch (S3Exception e) {
            throw new RuntimeException("Error al obtener la URL pública del objeto desde S3", e);
        } catch (Exception e) {
            throw new RuntimeException("Error desconocido al obtener la URL pública del objeto desde S3", e);
        }
    }


    /**
     * Delete a file from the S3 bucket.
     *
     * @param fileName the name of the file to be deleted from S3
     * @throws RuntimeException if the file cannot be deleted
     */
    public void deleteFile(String fileURL) {
        try {
            // Parsear la URL para obtener el nombre del archivo
            URL url = new URL(fileURL);
            String fileName = url.getPath().substring(1); // Se omite el primer carácter "/", ya que el nombre del archivo sigue
            s3Client.deleteObject(DeleteObjectRequest.builder()
                    .bucket(bucketName)
                    .key(fileName)
                    .build());
        } catch (S3Exception e) {
            throw new RuntimeException("Error deleting the file from S3", e);
        } catch (Exception e){
            throw new RuntimeException("Error desconocido al procesar la URL del archivo", e);
        }
    }

}
