package com.CarRental.CarRental.repository;

import com.CarRental.CarRental.model.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IImageRepository extends JpaRepository<Image, Long> {
    Optional<Image> findById(Long id);
    List<Image> findByProductId(Long id);

}
