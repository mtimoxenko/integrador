package com.CarRental.CarRental.service;
import com.CarRental.CarRental.dto.category.CategoryRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.stream.Collectors;


import java.util.List;

@Service
public class OpenAIService {

    private final ObjectMapper objectMapper = new ObjectMapper();
    private static final Logger logger = LoggerFactory.getLogger(OpenAIService.class);

    private final WebClient webClient;
    private final String openAiApiKey;
    private final CategoryService categoryService;


    public OpenAIService(WebClient.Builder webClientBuilder,
                         @Value("${openai.api.key}") String openAiApiKey, CategoryService categoryService) {
        this.webClient = webClientBuilder.baseUrl("https://api.openai.com/v1").build();
        this.openAiApiKey = openAiApiKey;
        this.categoryService = categoryService;
    }

    public Mono<String> generateText(String userInput) {
        // Wrap the synchronous getCategories call in a reactive type
        return Mono.fromCallable(categoryService::getCategories)
                .flatMap(categories -> {
                    String detailedPrompt = buildRecommendationPrompt(userInput, categories);
                    return this.webClient.post()
                            .uri("/chat/completions")
                            .header("Content-Type", "application/json")
                            .header("Authorization", "Bearer " + this.openAiApiKey)
                            .bodyValue(buildRequestBody(detailedPrompt))
                            .retrieve()
                            .bodyToMono(String.class)
                            .map(this::parseCommitMessage);
                });
    }


    private String buildRecommendationPrompt(String userInput, List<CategoryRequest> categories) {
        // Convert the List<CategoryRequest> to a stream, map it to get the names,
        // and then collect the names into a single string separated by commas
        String categoryNames = categories.stream()
                .map(CategoryRequest::getName)
                .collect(Collectors.joining(", "));

        // Construct the prompt with the dynamic list of category names
        return "Actuando como un experto vendedor de autos, dado que un cliente está planeando un viaje que requiere un vehículo con las siguientes características: \"" + userInput +
                "\", proporciona una recomendación concisa de un vehículo en no más de cuatro frases. Incluye detalles necesarios como la categoría del vehículo (" + categoryNames +
                "), marca, modelo, número de puertas, capacidad para pasajeros y maletas, si es automático o manual y el tipo de combustible recomendado.";
    }





    private String buildRequestBody(String detailedPrompt) {
        ObjectNode rootNode = objectMapper.createObjectNode();
        ArrayNode messagesNode = rootNode.putArray("messages");

        ObjectNode userMessageNode = messagesNode.addObject();
        userMessageNode.put("role", "user");
        userMessageNode.put("content", detailedPrompt);

        rootNode.put("model", "gpt-3.5-turbo");

        try {
            return objectMapper.writeValueAsString(rootNode);
        } catch (JsonProcessingException e) {
            logger.error("Error building the request body: {}", e.getMessage(), e);
            return "{}"; // return empty JSON object in case of error
        }
    }




    // Parse the response to extract the commit message
    private String parseCommitMessage(String jsonResponse) {
        try {
            JsonNode rootNode = objectMapper.readTree(jsonResponse);
            JsonNode choicesNode = rootNode.path("choices");
            if (choicesNode.isArray() && choicesNode.has(0)) {
                JsonNode messageNode = choicesNode.get(0).path("message").path("content");
                if (!messageNode.isMissingNode()) {
                    return messageNode.asText();
                }
            }
            logger.error("No recommendation found in the response");
            return "No recommendation found."; // Default message if the parsing fails
        } catch (Exception e) {
            logger.error("Error processing the recommendation: {}", e.getMessage(), e);
            return "Error processing the recommendation."; // Error message
        }
    }

}
