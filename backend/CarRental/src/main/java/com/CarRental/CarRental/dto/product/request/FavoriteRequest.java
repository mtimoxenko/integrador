package com.CarRental.CarRental.dto.product.request;

import lombok.Data;

@Data
public class FavoriteRequest {
    private Long userId;
    private Long productId;
}
