package com.CarRental.CarRental.mapper;

import com.CarRental.CarRental.dto.image.ImageRequest;
import com.CarRental.CarRental.model.Image;
import com.CarRental.CarRental.model.Product;
import lombok.experimental.UtilityClass;

@UtilityClass
public class ImageMapper {

    public static Image toEntity(ImageRequest dto, Product product) {

        Image image = new Image();
        image.setId(dto.getId());
        //imagen.setEncodeImage( Base64.getUrlEncoder().encode(dto.getEncodeImage().getBytes(StandardCharsets.UTF_8)));
        image.setImageUrl(dto.getImageUrl());
        image.setProduct(product);

        return image;
    }

}
