package com.CarRental.CarRental.mapper;

import com.CarRental.CarRental.dto.user.RegisterRequest;
import com.CarRental.CarRental.model.User;
import com.CarRental.CarRental.model.UserRol;
import lombok.experimental.UtilityClass;

import java.util.Optional;

@UtilityClass
public class UserMapper {

    public static User toEntity(final RegisterRequest request){
        final User user = new User();
        user.setName(request.getName());
        user.setEmail(request.getEmail());
        user.setSurname(request.getLastName());
        user.setPassword(request.getPassword());
        user.setRole(Optional.ofNullable(request.getUserRol()).orElse(UserRol.CLIENTE));
        return user;
    }

    public static RegisterRequest toDto(final User request){
       return RegisterRequest.builder()
               .email(request.getEmail())
               .userRol(request.getRole())
               .name(request.getName())
               .lastName(request.getSurname())
               .password(request.getPassword())
               .build();
    }
}
