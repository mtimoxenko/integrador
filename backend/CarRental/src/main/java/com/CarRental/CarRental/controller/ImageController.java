package com.CarRental.CarRental.controller;


import com.CarRental.CarRental.dto.image.ImageRequest;
import com.CarRental.CarRental.dto.image.ImageResponse;
import com.CarRental.CarRental.dto.product.request.ProductRequest;
import com.CarRental.CarRental.exception.ApiException;
import com.CarRental.CarRental.service.ImageService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "Imagen", description = "Endpoints para la gestión de Imagenes")
@RequestMapping(value = "/images")
@RestController
public class ImageController {
    private final ImageService imageService;

    public ImageController(final ImageService imageService) {
        this.imageService = imageService;
    }

    @Operation(summary = "Crear imagen", description = "Método encargado de crear una imagen")
    @PostMapping
    public ImageResponse createImage(@RequestBody ImageRequest request, ProductRequest productRequest) throws ApiException {
        return imageService.createImage(request, productRequest);
    }

    @Operation(summary = "Listar todas la imagenes", description = "Método encargado de listar imagenes")
    @GetMapping
    public List<ImageRequest> getImages(){
        return imageService.getImages();
    }

    @Operation(summary = "Borrar imagen", description = "Método encargado de borrar una imagen")
    @DeleteMapping(value="/{id}")
    public ImageResponse deleteImage(final @PathVariable  Long id) throws ApiException {
        return imageService.deleteImage(id);
    }



}
