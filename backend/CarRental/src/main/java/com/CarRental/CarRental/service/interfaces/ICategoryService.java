package com.CarRental.CarRental.service.interfaces;

import com.CarRental.CarRental.dto.category.CategoryRequest;
import com.CarRental.CarRental.dto.category.CategoryResponse;
import com.CarRental.CarRental.exception.ApiException;
import com.CarRental.CarRental.model.Category;

import java.util.List;

public interface ICategoryService {
    CategoryResponse createCategoty(CategoryRequest request) throws ApiException;
    List<CategoryRequest> getCategories();
    CategoryResponse deleteCategory(Long id) throws ApiException;
    Category findById(Long id) throws ApiException;

}
