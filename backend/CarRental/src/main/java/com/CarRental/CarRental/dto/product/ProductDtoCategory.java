package com.CarRental.CarRental.dto.product;

import com.CarRental.CarRental.dto.characteristic.CharacteristicDto;
import com.CarRental.CarRental.dto.image.ImageRequest;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductDtoCategory {
    private Long id;
    private String name;
    private Long category_id;
    private String category_name;
    private String brand;
    private String model;
    private String description;
    private String fuel;
    private int numPassengers;
    private int numBags;
    private int numDoors;
    private Boolean isAutomatic;
    private List<ImageRequest> images;
    private List<CharacteristicDto> characteristics;

}
