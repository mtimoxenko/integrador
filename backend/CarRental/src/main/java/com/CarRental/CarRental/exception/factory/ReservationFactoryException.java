package com.CarRental.CarRental.exception.factory;

import com.CarRental.CarRental.exception.dto.ApiErrorDto;
import lombok.experimental.UtilityClass;

import static jakarta.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static jakarta.servlet.http.HttpServletResponse.SC_NOT_FOUND;

@UtilityClass
public class ReservationFactoryException {

    public static final ApiErrorDto PRODUCT_NOT_AVILABLE =
            new ApiErrorDto("Product_not_avilable", "El producto no está disponible para está fecha", SC_BAD_REQUEST);

    public static final ApiErrorDto PRODUCT_DOES_NOT_EXIST =
            new ApiErrorDto("Product_does_not_exist", "El producto que desea reserva no existe", SC_BAD_REQUEST);

    public static final ApiErrorDto USER_DOES_NOT_EXIST =
            new ApiErrorDto("User_does_not_exist", "El User que desea hacer la reserva no se encuentra registrado", SC_BAD_REQUEST);

    public static final ApiErrorDto RESERVATION_TOO_SHORT =
            new ApiErrorDto("Reservation_too_short", "La reserva no cumple con el mínimo de días", SC_BAD_REQUEST);

    public static final ApiErrorDto RESERVATION_DATES_INVALID =
            new ApiErrorDto("Reservation_dates_invalid", "La fecha de inicio debe ser menor a la fecha de fin", SC_BAD_REQUEST);

    public static final ApiErrorDto DELETE_RESERVATION_NOT_FOUND =
            new ApiErrorDto("Delete_reservation_not_found", "La reserva que desea eliminar no existe", SC_NOT_FOUND);
}
