package com.CarRental.CarRental.dto.user;

import com.CarRental.CarRental.model.UserRol;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RegisterRequest {
    private String email;
    private String name;
    private String lastName;
    private String password;
    private UserRol userRol;
}
