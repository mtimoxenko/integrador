package com.CarRental.CarRental.controller;

import com.CarRental.CarRental.dto.characteristic.CharacteristicDto;
import com.CarRental.CarRental.dto.product.response.Response;
import com.CarRental.CarRental.exception.ApiException;
import com.CarRental.CarRental.service.interfaces.ICharacteristicService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Tag(name = "Caracteristicas", description = "Endpoints para la gestión de Caracteristicas")
@RequestMapping(value = "/characteristic")
@Validated
public class CharacteristicController {
    private final ICharacteristicService characteristicService;

    public CharacteristicController(ICharacteristicService characteristicService) {
        this.characteristicService = characteristicService;
    }
    @Operation(summary = "Crear características", description = "Método encargado de crear una características")
    @PostMapping
    @PreAuthorize("hasRole('ADMINISTRADOR')")
    public Response create(@Valid @RequestBody CharacteristicDto request) throws ApiException {
        return characteristicService.createCharacteristic(request);
    }

    @Operation(summary = "Editar característica", description = "Método encargado de editar característica")
    @PutMapping
    @PreAuthorize("hasRole('ADMINISTRADOR')")
    public Response edit(@Valid @RequestBody CharacteristicDto request) throws ApiException {
        return characteristicService.editCharacteristic(request);
    }

    @Operation(summary = "Borrar característica", description = "Método encargado de borrar característica")
    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMINISTRADOR')")
    public Response delete(@PathVariable Long id) throws ApiException {
        return characteristicService.deleteCharacteristic(id);
    }

    @Operation(summary = "Listar características", description = "Método encargado de listar características")
    @GetMapping
    @PreAuthorize("hasRole('ADMINISTRADOR')")
    public List<CharacteristicDto> getAll(){
        return this.characteristicService.getCharacteristic();
    }

    @Operation(summary = "Listar característica por ID", description = "Método encargado de listar característica por su ID")
    @GetMapping("/{id}")
    @PreAuthorize("hasRole('ADMINISTRADOR')")
    public CharacteristicDto getById(@PathVariable Long id) throws ApiException {
        return this.characteristicService.findById(id);
    }
}
