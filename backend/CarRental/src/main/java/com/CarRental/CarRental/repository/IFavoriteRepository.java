package com.CarRental.CarRental.repository;

import com.CarRental.CarRental.model.Favorite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IFavoriteRepository extends JpaRepository<Favorite, Long> {
    List<Favorite> findByUserEmail(String userId);
    List<Favorite> findByUserId(Long id);
    @Query(value = "SELECT * FROM favorites f WHERE f.user_id = :userId AND f.product_id = :productId", nativeQuery = true)
    Optional<Favorite> findByUserIdAndProductId(@Param("userId")Long userId, @Param("productId") Long productId);
}
