package com.CarRental.CarRental.security;

import com.CarRental.CarRental.dto.product.response.Response;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.stream.Stream;

import static com.CarRental.CarRental.exception.factory.UserFactoryException.INVALID_AUTHORIZATION_HEADER;

@Slf4j
@Component
public class JwtAuthorizationFilter extends OncePerRequestFilter {
    @Value("${security.secret}")
    private  String secret;
    private final JwtTokenUtil jwtTokenUtil;

    public JwtAuthorizationFilter(final JwtTokenUtil jwtTokenUtil) {
        this.jwtTokenUtil = jwtTokenUtil;
    }
    @Override
    protected  boolean shouldNotFilter(final HttpServletRequest request){
        return Stream.of(
                "/products/public",
                "/h2-console",
                "/user/public",
                "category/public",
                "/uploads",
                "/actuator/prometheus",
                "reservation/public",
                "/openai/recommendation",
                "/swagger-ui/",
                "/v3/api-docs" // Add this line
        ).anyMatch(url -> request.getRequestURI().contains(url));
    }

    @Override
    protected void doFilterInternal(final HttpServletRequest request,
                                    final HttpServletResponse response,
                                    final FilterChain filterChain)
            throws JWTVerificationException, ServletException,IOException {
        try {
            final String authorizationHeader = request.getHeader("Authorization");
            if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer ")) {
                throw INVALID_AUTHORIZATION_HEADER.toException();
            }

            final String token = authorizationHeader.substring(7);
            final Authentication auth = jwtTokenUtil.validateToken(token);
            SecurityContextHolder.getContext().setAuthentication(auth);
            filterChain.doFilter(request, response);
        }catch (Exception e){

            // Convierte el objeto ResponseDto a JSON
            String jsonResponse = new ObjectMapper().writeValueAsString(Response.builder().success(false).message(e.getMessage()).build());
            SecurityContextHolder.clearContext();
            // Establece el código de estado y el contenido de la respuesta
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            response.setContentType("application/json");
            response.getWriter().write(jsonResponse);
            response.getWriter().flush();
        }
    }
}
