package com.CarRental.CarRental.service;

import com.CarRental.CarRental.dto.image.ImageRequest;
import com.CarRental.CarRental.dto.image.ImageResponse;
import com.CarRental.CarRental.dto.product.request.ProductRequest;
import com.CarRental.CarRental.exception.ApiException;
import com.CarRental.CarRental.mapper.ImageMapper;
import com.CarRental.CarRental.mapper.ProductMapper;
import com.CarRental.CarRental.model.Image;
import com.CarRental.CarRental.repository.IImageRepository;
import com.CarRental.CarRental.service.interfaces.IImageService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static com.CarRental.CarRental.exception.factory.ImageFactoryException.DELETE_IMAGE_ERROR_NOT_FOUND;

@Service
public class ImageService implements IImageService {
    private final IImageRepository imageRepository;
    private final ObjectMapper mapper;

    public ImageService(final IImageRepository imageRepository, ObjectMapper mapper) {
        this.imageRepository = imageRepository;
        this.mapper = mapper;
    }

    public ImageResponse createImage(ImageRequest request, ProductRequest productRequest) throws ApiException {

       Image saveImage =  imageRepository.save(ImageMapper.toEntity(request, ProductMapper.toEntity(productRequest.getCurrentProduct())));
        return ImageResponse
                .builder()
                .success(true)
                .message("Imagen creada exitosamente")
                .build();
    }

    public Boolean existById(Long id){
        return imageRepository.existsById(id);
    }

    public ImageResponse deleteImage(Long id) throws ApiException {

        imageRepository
                .findById(id)
                .orElseThrow(DELETE_IMAGE_ERROR_NOT_FOUND::toException);

        imageRepository.deleteById(id);

        return ImageResponse.builder()
                .success(true)
                .message("Imagen Eliminada con exito")
                .build();
    }

    public List<ImageRequest> getImages(){
        List<ImageRequest> images = new ArrayList<>();
        List<Image> imagesModel = imageRepository.findAll();

        for(Image image: imagesModel){
            images.add(mapper.convertValue(image, ImageRequest.class));
        }
        for (int i = 0; i < images.size(); i++) {
            images.get(i).setProduct_id(imagesModel.get(i).getProduct().getId());
        }

        return images;
    }



}
