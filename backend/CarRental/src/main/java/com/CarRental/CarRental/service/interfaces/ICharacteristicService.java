package com.CarRental.CarRental.service.interfaces;

import com.CarRental.CarRental.dto.characteristic.CharacteristicDto;
import com.CarRental.CarRental.dto.product.response.Response;
import com.CarRental.CarRental.exception.ApiException;
import com.CarRental.CarRental.model.Characteristic;

import java.util.List;

public interface ICharacteristicService {

    Response createCharacteristic(CharacteristicDto request) throws ApiException;
    Response editCharacteristic(CharacteristicDto request) throws ApiException;
    List<CharacteristicDto> getCharacteristic();
    Response deleteCharacteristic(Long id) throws ApiException;
    CharacteristicDto findById(Long id) throws ApiException;
    List<Characteristic> findByInId(List<Long> ids);
}
