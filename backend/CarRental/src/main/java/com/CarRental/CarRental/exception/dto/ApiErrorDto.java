package com.CarRental.CarRental.exception.dto;

import com.CarRental.CarRental.exception.ApiException;
import com.google.common.collect.ImmutableSet;
import lombok.Getter;
import lombok.ToString;

import java.util.Collections;
import java.util.Objects;
import java.util.Set;

@Getter
@ToString
public class ApiErrorDto {
  private String error;
  private String message;
  private int status;

  private final Set<CauseDto> causes;

  /* default */ ApiErrorDto() {
    this.causes = Collections.emptySet();
  }
  public ApiErrorDto(String error, String message, int status) {
    this();
    this.error = error;
    this.message = message;
    this.status = status;
  }

  public ApiErrorDto(String error, String message, int status, Set<CauseDto> causes) {
    this.error = error;
    this.message = message;
    this.status = status;
    this.causes = causes == null ? Collections.emptySet() : causes;
  }

  public ApiErrorDto withCause(final String causeCode, final String causeDescription) {
    final CauseDto causeDto = new CauseDto(causeCode, causeDescription);
    return new ApiErrorDto(error, message, status,  ImmutableSet.<CauseDto>builder().addAll(getCauses()).add(causeDto).build());
  }

  public ApiErrorDto withCause(final Set<CauseDto> newCauseDtos) {
    return new ApiErrorDto(error, message, status, ImmutableSet.<CauseDto>builder().addAll(getCauses()).addAll(newCauseDtos).build());
  }

  public ApiErrorDto withCause(final ApiErrorDto otherApiErrorDto) {
    return withCause(otherApiErrorDto.getCauses());
  }

  public ApiException toException() {
    return new ApiException(error, message, status,  causes);
  }

  public static ApiErrorDto fromException(final ApiException e) {
    return new ApiErrorDto(e.getError(), e.getMessage(), e.getStatus(), e.getCauseDtos());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    ApiErrorDto apiErrorDto = (ApiErrorDto) o;
    return status == apiErrorDto.status &&
        Objects.equals(error, apiErrorDto.error) &&
        Objects.equals(message, apiErrorDto.message) &&
        unorderedListEquals(causes, apiErrorDto.causes);
  }

  @Override
  public int hashCode() {
    return Objects.hash(error, message, status, causes);
  }

  private boolean unorderedListEquals(Set<CauseDto> list1, Set<CauseDto> list2) {
    return list1.containsAll(list2) && list2.containsAll(list1);
  }
}
