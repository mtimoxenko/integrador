package com.CarRental.CarRental.mapper;

import com.CarRental.CarRental.dto.product.FavoriteDto;
import com.CarRental.CarRental.model.Favorite;
import lombok.experimental.UtilityClass;

@UtilityClass
public class FavoriteMapper {

    public static FavoriteDto toDto(Favorite entity){
        return FavoriteDto.builder()
                .idFav(entity.getId())
                .productDto(ProductMapper.toDto(entity.getProduct()))
                .build();
    }
}
