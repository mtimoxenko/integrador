package com.CarRental.CarRental.dto.product;

import com.CarRental.CarRental.dto.characteristic.CharacteristicDto;
import com.CarRental.CarRental.dto.category.CategoryRequest;
import com.CarRental.CarRental.dto.image.ImageRequest;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductDto {
    private Long id;
    private String name;
    private CategoryRequest category;
    private String brand;
    private String model;
    private String description;
    private String fuel;
    private int numPassengers;
    private int numBags;
    private int numDoors;
    private Boolean isAutomatic;
    private List<ImageRequest> images;
    private List<CharacteristicDto> characteristics;

}
